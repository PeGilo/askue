﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AskueCtp.Entities
{
    public class ReadingType
    {
        private int m_Code = default(int);
        private string m_Description = String.Empty;

        public virtual int Code
        {
            get { return m_Code; }
        }

        /// <summary>
        /// Can be null
        /// </summary>
        public virtual string Description
        {
            get { return m_Description; }
        }

        public override bool Equals(object obj)
        {
            if (!base.Equals(obj)) return false;

            if (obj == null) return false;

            if (this.GetType() != obj.GetType()) return false;

            ReadingType other = (ReadingType)obj;

            return (
                m_Code.Equals(other.m_Code)
                );
        }

        public override int GetHashCode()
        {
            return m_Code;
        }
    }
}
