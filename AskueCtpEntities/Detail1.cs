﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AskueCtp.Entities
{
    /// <summary>
    /// Детализация 1
    /// </summary>
    public class Detail1
    {
        private Detail1Cid m_Cid = null;
        private String m_Description = String.Empty;

        public virtual Detail1Cid Cid
        {
            get { return m_Cid; }
        }

        /// <summary>
        /// Can be null
        /// </summary>
        public virtual String Description
        {
            get { return m_Description; }
        }

        public override bool Equals(object obj)
        {
            if (!base.Equals(obj)) return false;

            if (obj == null) return false;

            if (this.GetType() != obj.GetType()) return false;

            Detail1 other = (Detail1)obj;

            return (
                m_Cid.Equals(other.m_Cid)
                );
        }

        public override int GetHashCode()
        {
            return m_Cid.GetHashCode();
        }
    }
}
