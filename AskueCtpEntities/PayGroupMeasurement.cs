﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AskueCtp.Entities
{
    public class PayGroupMeasurement
    {
        private Feeder m_Feeder = null;
        private Sign m_Sign = Sign.Plus;
        private int? m_Interval = default(int?);
        private PayGroupMeasurementCid m_Cid = null;
        private FirstLevelPayGroup m_ParentGroup = null;
        private ReadingType m_ReadingType = null;
        private Counter m_Counter = null;
        private int m_CounterNumber = default(int);
        private string m_SV = String.Empty;

        public virtual PayGroupMeasurementCid Cid
        {
            get { return m_Cid; }
        }

        public virtual Feeder Feeder
        {
            get { return m_Feeder; }
        }

        public virtual Sign Sign
        {
            get { return m_Sign; }
        }

        public virtual int? Interval
        {
            get { return m_Interval; }
        }

        public virtual int CounterNumber
        {
            get { return m_CounterNumber; }
        }

        public virtual Counter Counter
        {
            get { return m_Counter; }
        }

        /// <summary>
        /// Can be null
        /// </summary>
        public virtual string SV
        {
            get { return m_SV; }
        }

        public virtual ReadingType ReadingType
        {
            get { return m_ReadingType; }
        }

        public virtual FirstLevelPayGroup ParentGroup
        {
            get { return m_ParentGroup; }
        }

        public override bool Equals(object obj)
        {
            if (!base.Equals(obj)) return false;

            if (obj == null) return false;

            if (this.GetType() != obj.GetType()) return false;

            PayGroupMeasurement other = (PayGroupMeasurement)obj;

            return (
                m_Cid.Equals(other.m_Cid)
                );
        }

        public override int GetHashCode()
        {
            return m_Cid.GetHashCode();
        }

    }
}
