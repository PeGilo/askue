﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AskueCtp.Entities
{
    public class FirstLevelPayGroup : IPayGroup
    {
        private PayGroupCid m_Cid = null;
        private AskueCtp.Entities.Object m_ParentObject = null;
        private ICollection<PayGroupMeasurement> m_PayGroupMeasurements = new List<PayGroupMeasurement>();

        private string m_Description = String.Empty;
        private string m_Creator = String.Empty;
        private int? m_Interval = default(int?);
        private DateTime? m_BeginDate = default(DateTime?);
        private DateTime? m_EndDate = default(DateTime?);
        private DateTime? m_CreationDate = default(DateTime?);
        private GroupType m_Type = GroupType.Calculated;
        private ReadingType m_ReadingType = null;
        private TimeZone m_TimeZone = null;
        private Detail1 m_Detail1 = null;
        private Detail2 m_Detail2 = null;
        private BalanceElement m_BalanceElement = null;

        public virtual PayGroupCid Cid
        {
            get { return m_Cid; }
        }

        public virtual AskueCtp.Entities.Object ParentObject
        {
            get { return m_ParentObject; }
        }

        public virtual ICollection<PayGroupMeasurement> PayGroupMeasurements
        {
            get { return m_PayGroupMeasurements; }
        }

        public virtual string Description
        {
            get { return m_Description; }
        }

        /// <summary>
        /// Тип расчетной группы (расчетная, техническая)
        /// </summary>
        public virtual GroupType Type
        {
            get { return m_Type; }
        }

        /// <summary>
        /// Тип показаний (АЭпр, АЭотд, РЭпр, ...)
        /// </summary>
        public virtual ReadingType ReadingType
        {
            get { return m_ReadingType; }
        }

        /// <summary>
        /// Вариант временной зоны
        /// </summary>
        public virtual TimeZone TimeZone
        {
            get { return m_TimeZone; }
        }

        /// <summary>
        /// Детализация 1
        /// </summary>
        public virtual Detail1 Detail1
        {
            get { return m_Detail1; }
        }

        /// <summary>
        /// Детализация 2
        /// </summary>
        public virtual Detail2 Detail2
        {
            get { return m_Detail2; }
        }

        public virtual BalanceElement BalanceElement
        {
            get { return m_BalanceElement; }
        }

        public virtual int? Interval
        {
            get { return m_Interval; }
        }

        public virtual DateTime? BeginDate
        {
            get { return m_BeginDate; }
        }

        public virtual DateTime? EndDate
        {
            get { return m_EndDate; }
        }

        public virtual DateTime? CreationDate
        {
            get { return m_CreationDate; }
        }

        /// <summary>
        /// Can be null
        /// </summary>
        public virtual string Creator
        {
            get { return m_Creator; }
        }

        public override bool Equals(object obj)
        {
            if (!base.Equals(obj)) return false;

            if (obj == null) return false;

            if (this.GetType() != obj.GetType()) return false;

            FirstLevelPayGroup other = (FirstLevelPayGroup)obj;

            return (
                m_Cid.Equals(other.m_Cid)
                );
        }

        public override int GetHashCode()
        {
            return m_Cid.GetHashCode();
        }
    }
}
