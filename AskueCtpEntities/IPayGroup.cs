﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AskueCtp.Entities
{
    public interface IPayGroup
    {
        PayGroupCid Cid
        {
            get;
        }

        string Description
        {
            get;
        }

        AskueCtp.Entities.Object ParentObject
        {
            get;
        }
    }
}
