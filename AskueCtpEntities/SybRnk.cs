﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AskueCtp.Entities
{
    /// <summary>
    /// Summary description for SybRnk
    /// </summary>
    public class SybRnk
    {
        //SYB_RNK	NUMBER(3,0)	        No		1			
        //TXT	    VARCHAR2(25 BYTE)	Yes		2			
        //YN	    NUMBER(1,0)	        Yes		3			Видимость
        //N	        NUMBER(2,0)	        Yes		4			Порядок вывода
        //P1	    VARCHAR2(1 BYTE)	Yes		5			

        private int m_Code = -1;
        private string m_Description = String.Empty;
        private int? m_Visibility = default(int?);
        private int? m_Sequence = default(int?);
        private string m_P1 = String.Empty;
        private ICollection<AskueCtp.Entities.Object> m_Objects =
            new List<AskueCtp.Entities.Object>();

        public virtual int Code
        {
            get
            {
                return m_Code;
            }
        }

        /// <summary>
        /// Can be null
        /// </summary>
        public virtual string Description
        {
            get
            {
                return m_Description;
            }
        }

        public virtual ICollection<AskueCtp.Entities.Object> Objects
        {
            get
            {
                return m_Objects;
            }
        }

        public virtual bool Visibility
        {
            get
            {
                return (m_Visibility.HasValue && m_Visibility.Value == 1);
            }
        }

        /// <summary>
        /// Порядок вывода
        /// </summary>
        public virtual int? Sequence
        {
            get
            {
                return m_Sequence;
            }
        }

        /// <summary>
        /// Can be null
        /// </summary>
        private string P1
        {
            get
            {
                return m_P1;
            }
        }

        public override bool Equals(object obj)
        {
            if (!base.Equals(obj)) return false;

            if (obj == null) return false;

            if (this.GetType() != obj.GetType()) return false;

            SybRnk other = (SybRnk)obj;

            return (this.m_Code == other.m_Code
                // Enough of code comparison. Otherwise should compare m_Objects also.
                //&& this.m_Description == other.m_Description
                //&& this.m_P1 == other.m_P1
                //&& this.m_Yn == other.m_Yn
                //&& this.m_N == other.m_N
                );
        }

        public override int GetHashCode()
        {
            return m_Code;
        }
    }
}
