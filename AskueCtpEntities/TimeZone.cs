﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AskueCtp.Entities
{
    /// <summary>
    /// Вариант временной зоны
    /// </summary>
    public class TimeZone
    {
        private int m_Code = default(int);
        private string m_Description = String.Empty;
        private DateTime m_BeginDate = default(DateTime);
        private DateTime m_EndDate = default(DateTime);
        private DateTime? m_ChangeDate = default(DateTime?);
        private string m_Creator = String.Empty;

        public virtual int Code
        {
            get { return m_Code; }
        }

        /// <summary>
        /// Can be null
        /// </summary>
        public virtual string Description
        {
            get { return m_Description; }
        }

        /// <summary>
        /// Can be null
        /// </summary>
        public virtual string Creator
        {
            get { return m_Creator; }
        }

        public virtual DateTime BeginDate
        {
            get { return m_BeginDate; }
        }

        public virtual DateTime EndDate
        {
            get { return m_EndDate; }
        }

        public virtual DateTime? ChangeDate
        {
            get { return m_ChangeDate; }
        }

        public override bool Equals(object obj)
        {
            if (!base.Equals(obj)) return false;

            if (obj == null) return false;

            if (this.GetType() != obj.GetType()) return false;

            TimeZone other = (TimeZone)obj;

            return (
                m_Code.Equals(other.m_Code)
                );
        }

        public override int GetHashCode()
        {
            return m_Code;
        }
    }
}
