﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AskueCtp.Entities
{
    public class FeederMeasurement
    {
        private FeederMeasurementCid m_Cid = new FeederMeasurementCid();
        private int? m_CounterNumber = default(int?);
        private int? m_CountShouldBe = default(int?);
        private float? m_Value = default(float?);
        private float? m_RashPoln = default(float?);
        private float? m_PokStart = default(float?);
        private int? m_Min0 = default(int?);
        private int? m_Min1 = default(int?);
        private float? m_AkSum = default(float?);
        private long? m_Impulses = default(long?);

        public virtual FeederMeasurementCid Cid
        {
            get { return m_Cid; }
        }

        public virtual int? CounterNumber
        {
            get { return m_CounterNumber; }
        }

        public virtual int? CountShouldBe
        {
            get { return m_CountShouldBe; }
        }

        public virtual float? Value
        {
            get { return m_Value; }
        }

        public virtual float? RashPoln
        {
            get { return m_RashPoln; }
        }

        public virtual float? PokStart
        {
            get { return m_PokStart; }
        }

        public virtual int? Min0
        {
            get { return m_Min0; }
        }

        public virtual int? Min1
        {
            get { return m_Min1; }
        }

        public virtual float? AkSum
        {
            get { return m_AkSum; }
        }

        public virtual long? Impulses
        {
            get { return m_Impulses; }
        }

        public override bool Equals(object obj)
        {
            if (!base.Equals(obj)) return false;

            if (obj == null) return false;

            if (this.GetType() != obj.GetType()) return false;

            FeederMeasurement other = (FeederMeasurement)obj;

            return (Object.Equals(this.m_Cid, other.m_Cid));
        }

        public override int GetHashCode()
        {
            return m_Cid.GetHashCode();
        }
    }
}
