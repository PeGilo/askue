﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AskueCtp.Entities
{
    public class Feeder
    {
        private FeederCid m_Cid = null;
        private AskueCtp.Entities.Object m_ParentObject;
        private ICollection<FeederProfile> m_Profiles = new List<FeederProfile>();
        private BusBar m_BusBar = null;

        private string m_Description = String.Empty;
        private float? m_LossPercentLinear = default(float?);
        private float? m_LossPercentTr = default(float?);
        private int? m_KT1 = default(int?);
        private int? m_KT2 = default(int?);
        private int? m_KN1 = default(int?);
        private int? m_KN2 = default(int?);
        private string m_Creator = String.Empty;
        private DateTime? m_CreationDate = default(DateTime?);

        public virtual FeederCid Cid
        {
            get { return m_Cid; }
        }

        public virtual BusBar BusBar
        {
            get { return m_BusBar; }
        }

        /// <summary>
        /// Can be null
        /// </summary>
        public virtual string Description
        {
            get { return m_Description; }
        }

        public virtual float? LossPercentLinear
        {
            get { return m_LossPercentLinear; }
        }

        public virtual float? LossPercentTr
        {
            get { return m_LossPercentTr; }
        }

        /// <summary>
        /// Can be null
        /// </summary>
        public virtual string Creator
        {
            get { return m_Creator; }
        }

        public virtual DateTime? CreationDate
        {
            get { return m_CreationDate; }
        }

        public virtual int? KT1
        {
            get { return m_KT1; }
        }

        public virtual int? KT2
        {
            get { return m_KT2; }
        }

        public virtual int? KN1
        {
            get { return m_KN1; }
        }

        public virtual int? KN2
        {
            get { return m_KN2; }
        }

        public virtual AskueCtp.Entities.Object ParentObject
        {
            get { return m_ParentObject; }
            set { m_ParentObject = value; }
        }

        public virtual ICollection<FeederProfile> Profiles
        {
            get { return m_Profiles; }
        }

        public override bool Equals(object obj)
        {
            if (!base.Equals(obj)) return false;

            if (obj == null) return false;

            if (this.GetType() != obj.GetType()) return false;

            Feeder other = (Feeder)obj;

            return (
                m_Cid.Equals(other.m_Cid)
                );
        }

        public override int GetHashCode()
        {
            return m_Cid.GetHashCode();
        }
    }
}
