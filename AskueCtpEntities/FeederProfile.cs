﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AskueCtp.Entities
{
    public class FeederProfile
    {
        private ProfileType m_ProfileType = ProfileType.ActiveOut;
        private Counter m_Counter = null;
        private Feeder m_ParentFeeder = null;
        private FeederProfileCid m_Cid = null;
        private ICollection<FeederMeasurement> m_Measurements = new List<FeederMeasurement>();

        public virtual ProfileType ProfileType
        {
            get { return m_ProfileType; }
        }

        public virtual FeederProfileCid Cid
        {
            get { return m_Cid; }
        }

        public virtual Counter Counter
        {
            get { return m_Counter; }
        }

        public virtual Feeder ParentFeeder
        {
            get { return m_ParentFeeder; }
        }

        public virtual ICollection<FeederMeasurement> Measurements
        {
            get { return m_Measurements; }
        }
    }
}
