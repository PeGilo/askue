﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AskueCtp.Entities
{
    public enum GroupType : int
    {
        Calculated = 0,
        /// <summary>
        /// Monitoring
        /// </summary>
        Technical = 1
    }
}
