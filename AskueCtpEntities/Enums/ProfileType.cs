﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AskueCtp.Entities
{
    /// <summary>
    /// Тип измеряемых показаний
    /// </summary>
    public enum ProfileType : int
    {
        /// <summary>
        /// Активный Прием
        /// </summary>
        ActiveIn = 1, 

        /// <summary>
        /// Активная Отдача
        /// </summary>
        ActiveOut = 2,

        /// <summary>
        /// Реактивный Прием
        /// </summary>
        ReactiveIn = 3,

        /// <summary>
        /// Реактивная Отдача
        /// </summary>
        ReactiveOut = 4
    }
}
