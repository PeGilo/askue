﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AskueCtp.Entities
{
    public enum Sign : int
    {
        Minus = 0,
        Plus = 1
    }
}
