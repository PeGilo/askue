﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AskueCtp.Entities
{
    public enum BalanceGroup : int
    {
        Electricity = 0,
        Gas = 1,
        Heat = 2,
        Water = 3
    }
}
