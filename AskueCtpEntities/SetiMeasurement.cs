﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AskueCtp.Entities
{
    public class SetiMeasurement
    {
        private SetiMeasurementCid m_Cid = null;

        private Single? m_Ia = default(Single?);
        private Single? m_Ib = default(Single?);
        private Single? m_Ic = default(Single?);
        private Single? m_Ua = default(Single?);
        private Single? m_Ub = default(Single?);
        private Single? m_Uc = default(Single?);
        private Single? m_PfAngA = default(Single?);
        private Single? m_PfAngB = default(Single?);
        private Single? m_PfAngC = default(Single?);
        private Counter m_Counter = null;

        public virtual Single? Ia { get { return m_Ia; } set { m_Ia = value; } }
        public virtual Single? Ib { get { return m_Ib; } set { m_Ib = value; } }
        public virtual Single? Ic { get { return m_Ic; } set { m_Ic = value; } }
        public virtual Single? Ua { get { return m_Ua; } set { m_Ua = value; } }
        public virtual Single? Ub { get { return m_Ub; } set { m_Ub = value; } }
        public virtual Single? Uc { get { return m_Uc; } set { m_Uc = value; } }
        public virtual Single? PfAngA { get { return m_PfAngA; } set { m_PfAngA = value; } }
        public virtual Single? PfAngB { get { return m_PfAngB; } set { m_PfAngB = value; } }
        public virtual Single? PfAngC { get { return m_PfAngC; } set { m_PfAngC = value; } }
        public virtual Counter Counter
        {
            get { return m_Counter; }
        }
        
        public virtual SetiMeasurementCid Cid
        {
            get { return m_Cid; }
        }

        public override bool Equals(object obj)
        {
            if (!base.Equals(obj)) return false;

            if (obj == null) return false;

            if (this.GetType() != obj.GetType()) return false;

            SetiMeasurement other = (SetiMeasurement)obj;

            return (this.m_Cid == other.m_Cid);
        }

        public override int GetHashCode()
        {
            return m_Cid.GetHashCode();
        }

        public override string ToString()
        {
            return "Cid=" + m_Cid.ToString();
        }

        public virtual double FullEnergy
        {
            get {
                if (this == null || !this.Ia.HasValue || !this.Ib.HasValue || !this.Ic.HasValue
                    || !this.Ua.HasValue || !this.Ub.HasValue || !this.Uc.HasValue
                    || !this.PfAngA.HasValue || !this.PfAngB.HasValue || !this.PfAngC.HasValue)
                {
                    throw new InvalidOperationException("Measurement contains null value");
                }

                if (this.Counter == null || !this.Counter.KN.HasValue || !this.Counter.KT.HasValue)
                {
                    throw new InvalidOperationException("Measurement does not contain information about counter");
                }

                double Pa, Pb, Pc, P;
                double Qa, Qb, Qc, Q;
                double S;
                double KT = this.Counter.KT.Value; // this values can be also calculated from Feeder object
                double KN = this.Counter.KN.Value;

                double ia = this.Ia.Value * KT;
                double ib = this.Ib.Value * KT;
                double ic = this.Ic.Value * KT;

                double ua = this.Ua.Value * KN; 
                double ub = this.Ub.Value * KN;
                double uc = this.Uc.Value * KN;

                double angARad = this.PfAngA.Value * Math.PI / 180;
                double angBRad = this.PfAngB.Value * Math.PI / 180;
                double angCRad = this.PfAngC.Value * Math.PI / 180;

                // Активная мощность
                Pa = ia * ua * Math.Abs(Math.Cos(angARad)); // Берем по модулю, чтобы не было минусов
                Pb = ib * ub * Math.Abs(Math.Cos(angBRad));
                Pc = ic * uc * Math.Abs(Math.Cos(angCRad));

                // Реактивная мощность
                //Qa = ia * ua * Math.Sin(angARad);
                //Qb = ib * ub * Math.Sin(angBRad);
                //Qc = ic * uc * Math.Sin(angCRad);

                // Полноая мощность
                P = Pa + Pb + Pc;
                //Q = Qa + Qb + Qc;

                //S = Math.Sqrt(P * P + Q * Q);
                
                return P;
            }
        }
    }
}
