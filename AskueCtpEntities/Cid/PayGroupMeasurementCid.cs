﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AskueCtp.Common;

namespace AskueCtp.Entities
{
    [Serializable]
    public class PayGroupMeasurementCid
    {
        private int m_SybRnk;
        private int m_ObjectNumber;
        private int m_PayGroupNumber;

        private int m_FeederOwnerSybRnk;
        private int m_FeederOwnerObjectNumber;
        private int m_FeederNumber;

        public PayGroupMeasurementCid()
        {
        }

        public PayGroupMeasurementCid(int sybRnk, int objectNumber, int payGroupNumber,
            int feederOwnerSybRnk, int feederOwnerObjectNumber, int feederNumber)
        {
            m_SybRnk = sybRnk;
            m_ObjectNumber = objectNumber;
            m_PayGroupNumber = payGroupNumber;
            m_FeederOwnerSybRnk = feederOwnerSybRnk;
            m_FeederOwnerObjectNumber = feederOwnerObjectNumber;
            m_FeederNumber = feederNumber;
        }

        public virtual int SybRnk
        {
            get { return m_SybRnk; }
            set { m_SybRnk = value; }
        }

        public virtual int ObjectNumber
        {
            get { return m_ObjectNumber; }
            set { m_ObjectNumber = value; }
        }        

        public virtual int PayGroupNumber
        {
            get { return m_PayGroupNumber; }
            set { m_PayGroupNumber = value; }
        }

        public virtual int FeederOwnerSybRnk
        {
            get { return m_FeederOwnerSybRnk; }
            set { m_FeederOwnerSybRnk = value; }
        }
        
        public virtual int FeederOwnerObjectNumber
        {
            get { return m_FeederOwnerObjectNumber; }
            set { m_FeederOwnerObjectNumber = value; }
        }

        public virtual int FeederNumber
        {
            get { return m_FeederNumber; }
            set { m_FeederNumber = value; }
        }

        public override bool Equals(object obj)
        {
            if (!base.Equals(obj)) return false;

            if (obj == null) return false;

            if (this.GetType() != obj.GetType()) return false;

            PayGroupMeasurementCid other = (PayGroupMeasurementCid)obj;

            return (
                m_SybRnk.Equals(other.m_SybRnk)
                && m_ObjectNumber.Equals(other.m_ObjectNumber)
                && m_PayGroupNumber.Equals(other.m_PayGroupNumber)
                && m_FeederOwnerSybRnk.Equals(other.m_FeederOwnerSybRnk)
                && m_FeederOwnerObjectNumber.Equals(other.m_FeederOwnerObjectNumber)
                && m_FeederNumber.Equals(other.m_FeederNumber)
                );
        }

        public override int GetHashCode()
        {
            return Hash.Generate(m_SybRnk, m_ObjectNumber, m_PayGroupNumber,
                m_FeederOwnerSybRnk, m_FeederOwnerObjectNumber, m_FeederNumber);
        }
    }
}
