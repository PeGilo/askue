﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AskueCtp.Common;

namespace AskueCtp.Entities
{
    public class FeederProfileCid
    {
        private int m_SybRnk;
        private int m_ObjectNumber;
        private int m_FeederNumber;
        private ProfileType m_ProfileType;

        public virtual int FeederNumber
        {
            get { return m_FeederNumber; }
            set { m_FeederNumber = value; }
        }

        public virtual int ObjectNumber
        {
            get { return m_ObjectNumber; }
            set { m_ObjectNumber = value; }
        }

        public virtual int SybRnk
        {
            get { return m_SybRnk; }
            set { m_SybRnk = value; }
        }

        public virtual ProfileType ProfileType
        {
            get { return m_ProfileType; }
            set { m_ProfileType = value; }
        }

        public override bool Equals(object obj)
        {
            if (!(obj is FeederProfileCid)) return false;

            return this.Equals((FeederProfileCid)obj);
        }

        public virtual bool Equals(FeederProfileCid obj)
        {
            return (m_SybRnk.Equals(obj.m_SybRnk)
                && m_ObjectNumber.Equals(obj.m_ObjectNumber)
                && m_FeederNumber.Equals(obj.m_FeederNumber)
                && m_ProfileType.Equals(obj.m_ProfileType));
        }

        public override int GetHashCode()
        {
            return Hash.Generate(m_SybRnk, m_ObjectNumber, m_FeederNumber, (int)m_ProfileType);
        }
    }
}
