﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AskueCtp.Common;

namespace AskueCtp.Entities
{

    /// <summary>
    /// Composite ID for pay groups
    /// </summary>
    [Serializable]
    public class PayGroupCid
    {
        private int m_SybRnk;
        private int m_ObjectNumber;
        private int m_PayGroupNumber;

        public PayGroupCid()
        { }

        public PayGroupCid(int sybRnk, int objectNumber, int payGroupNumber)
        {
            m_SybRnk = sybRnk;
            m_ObjectNumber = objectNumber;
            m_PayGroupNumber = payGroupNumber;
        }

        public virtual int SybRnk
        {
            get { return m_SybRnk; }
            set { m_SybRnk = value; }
        }

        public virtual int ObjectNumber
        {
            get { return m_ObjectNumber; }
            set { m_ObjectNumber = value; }
        }

        public virtual int PayGroupNumber
        {
            get { return m_PayGroupNumber; }
            set { m_PayGroupNumber = value; }
        }

        public override bool Equals(object obj)
        {
            if (!base.Equals(obj)) return false;

            if (obj == null) return false;

            if (this.GetType() != obj.GetType()) return false;

            PayGroupCid other = (PayGroupCid)obj;

            return (
                m_SybRnk.Equals(other.m_SybRnk)
                && m_ObjectNumber.Equals(other.m_ObjectNumber)
                && m_PayGroupNumber.Equals(other.m_PayGroupNumber)
                );
        }

        public override int GetHashCode()
        {
            return Hash.Generate(m_SybRnk, m_ObjectNumber, m_PayGroupNumber);
        }
    }
}
