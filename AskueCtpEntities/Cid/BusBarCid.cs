﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AskueCtp.Common;

namespace AskueCtp.Entities
{
    public class BusBarCid
    {
        private int m_SybRnk;
        private int m_ObjectNumber;
        private int m_BusBarNumber;

        public BusBarCid()
        {
        }

        public BusBarCid(int sybRnk, int objectNumber, int busBarNumber)
        {
            m_SybRnk = sybRnk;
            m_ObjectNumber = objectNumber;
            m_BusBarNumber = busBarNumber;
        }

        public virtual int FeederNumber
        {
            get { return m_BusBarNumber; }
        }

        public virtual int ObjectNumber
        {
            get { return m_ObjectNumber; }
        }

        public virtual int SybRnk
        {
            get { return m_SybRnk; }
        }

        public override bool Equals(object obj)
        {
            if (!base.Equals(obj)) return false;

            if (obj == null) return false;

            if (this.GetType() != obj.GetType()) return false;

            BusBarCid other = (BusBarCid)obj;

            return (
                m_SybRnk.Equals(other.m_SybRnk)
                && m_ObjectNumber.Equals(other.m_ObjectNumber)
                && m_BusBarNumber.Equals(other.m_BusBarNumber)
                );
        }

        public override int GetHashCode()
        {
            return Hash.Generate(m_SybRnk, m_ObjectNumber, m_BusBarNumber);
        }
    }
}
