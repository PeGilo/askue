﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AskueCtp.Common;

namespace AskueCtp.Entities
{
    [Serializable]
    public class ObjectCid
    {
        private int m_SybRnk;
        private int m_ObjectNumber;

        public ObjectCid()
        {
        }

        public ObjectCid(int sybRnk, int objNumber)
        {
            m_SybRnk = sybRnk;
            m_ObjectNumber = objNumber;
        }

        public virtual int SybRnk
        {
            get { return m_SybRnk; }
            //set { m_SybRnk = value; }
        }

        public virtual int ObjectNumber
        {
            get { return m_ObjectNumber; }
            //set { m_ObjectNumber = value; }
        }

        public override bool Equals(object obj)
        {
            if (!base.Equals(obj)) return false;

            if (obj == null) return false;

            if (this.GetType() != obj.GetType()) return false;

            ObjectCid other = (ObjectCid)obj;

            return (
                m_SybRnk.Equals(other.m_SybRnk)
                && m_ObjectNumber.Equals(other.m_ObjectNumber)
                );
        }

        public override int GetHashCode()
        {
            return Hash.Generate(m_SybRnk, m_ObjectNumber);
        }

        public override string ToString()
        {
            return "{" + "SybRnk=" + m_SybRnk.ToString() + " ObjectNumber=" + m_ObjectNumber.ToString() + "}";
        }
    }
}
