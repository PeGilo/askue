﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AskueCtp.Common;

namespace AskueCtp.Entities
{
    public class FeederMeasurementCid
    {
        private int? m_SybRnk;
        private int? m_ObjectNumber;
        private int? m_FeederNumber;
        private ProfileType? m_ProfileType;
        private DateTime? m_Date;
        private int? m_IntervalNumber;
        private int? m_Count;
        private string m_State;
        private int? m_Interval;

        public FeederMeasurementCid()
        {
            m_SybRnk = null;
            m_ObjectNumber = null;
            m_FeederNumber = null;
            m_ProfileType = null;
            m_Date = null;
            m_IntervalNumber = null;
            m_Count = null;
            m_State = null;
            m_Interval = null;
        }

        public FeederMeasurementCid(int? sybRnk, int? objNumber, int? feederNumber,
            ProfileType? profileType, DateTime? date, int? intervalNumber, int? count,
            string state, int? interval)
        {
            m_SybRnk = sybRnk;
            m_ObjectNumber = objNumber;
            m_FeederNumber = feederNumber;
            m_ProfileType = profileType;
            m_Date = date;
            m_IntervalNumber = intervalNumber;
            m_Count = count;
            m_State = state;
            m_Interval = interval;
        }

        public virtual int? SybRnk
        {
            get { return m_SybRnk; }
            //set { m_SybRnk = value; }
        }
        public virtual int? ObjectNumber
        {
            get { return m_ObjectNumber; }
            //set { m_ObjectNumber = value; }
        }
        public virtual int? FeederNumber
        {
            get { return m_FeederNumber; }
            //set { m_FeederNumber = value; }
        }
        public virtual ProfileType? ProfileType
        {
            get { return m_ProfileType; }
            //set { m_ProfileType = value; }
        }
        public virtual DateTime? Date
        {
            get { return m_Date; }
            //set { m_Date = value; }
        }
        public virtual int? IntervalNumber
        {
            get { return m_IntervalNumber; }
            //set { m_IntervalNumber = value; }
        }
        public virtual int? Count
        {
            get { return m_Count; }
            //set { m_Count = value; }
        }
        /// <summary>
        /// Can be null
        /// </summary>
        public virtual string State
        {
            get { return m_State; }
            //set { m_State = value; }
        }
        public virtual int? Interval
        {
            get { return m_Interval; }
            //set { m_Interval = value; }
        }

        public override bool Equals(object obj)
        {
            if (!base.Equals(obj)) return false;

            if (obj == null) return false;

            if (this.GetType() != obj.GetType()) return false;

            FeederMeasurementCid other = (FeederMeasurementCid)obj;

            return (
                m_SybRnk.Equals(other.m_SybRnk)
                && m_ObjectNumber.Equals(other.m_ObjectNumber)
                && m_FeederNumber.Equals(other.m_FeederNumber)
                && m_FeederNumber.Equals(other.m_ProfileType)
                && m_FeederNumber.Equals(other.m_Date)
                && m_FeederNumber.Equals(other.m_IntervalNumber)
                && m_FeederNumber.Equals(other.m_Count)
                && Object.Equals(m_FeederNumber, other.m_State)
                && m_FeederNumber.Equals(other.m_Interval)
                );
        }

        public override int GetHashCode()
        {
            int state = 0;
            Int32.TryParse(m_State, out state);
            return Hash.Generate(
                m_SybRnk.HasValue ? m_SybRnk.Value : 0, 
                m_ObjectNumber.HasValue ? m_ObjectNumber.Value : 0, 
                m_FeederNumber.HasValue ? m_FeederNumber.Value : 0,
                m_ProfileType.HasValue ? (int)m_ProfileType : 0,
                m_Date.HasValue ? m_Date.Value.Day : 0,
                m_IntervalNumber.HasValue ? m_IntervalNumber.Value : 0, 
                m_Count.HasValue ? m_Count.Value : 0, 
                m_Interval.HasValue ? m_Interval.Value : 0, 
                state);
        }
    }
}
