﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AskueCtp.Common;

namespace AskueCtp.Entities
{
    [Serializable]
    public class SetiMeasurementCid
    {
        private Int32 m_CounterNumber = default(Int32);
        private DateTime m_Date = default(DateTime);

        public virtual Int32 CounterNumber
        {
            get { return m_CounterNumber; }
        }

        public virtual DateTime Date
        {
            get { return m_Date; }
        }

        public SetiMeasurementCid()
        {
        }

        public SetiMeasurementCid(Int32 counterNumber, DateTime date)
        {
            m_CounterNumber = counterNumber;
            m_Date = date;
        }

        public override bool Equals(object obj)
        {
            if (!base.Equals(obj)) return false;

            if (obj == null) return false;

            if (this.GetType() != obj.GetType()) return false;

            SetiMeasurementCid other = (SetiMeasurementCid)obj;

            return (
                m_CounterNumber.Equals(other.m_CounterNumber)
                && m_Date.Equals(other.m_Date)
                );
        }

        public override int GetHashCode()
        {
            return Hash.Generate(m_CounterNumber, (int)m_Date.Ticks);
        }

        public override string ToString()
        {
            return "{" + "CounterNumber=" + m_CounterNumber.ToString() + " Date=" + m_Date.ToString() + "}";
        }
    }
}
