﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AskueCtp.Common;

namespace AskueCtp.Entities
{
    [Serializable]
    public class FirstLevelMeasurementCid
    {
        private int m_SecondLevelSybRnk;
        private int m_SecondLevelObjectNumber;
        private int m_SecondLevelPayGroupNumber;

        private int m_FirstLevelSybRnk;
        private int m_FirstLevelObjectNumber;
        private int m_FirstLevelPayGroupNumber;

        private PayGroupCid m_FirstLevelPayGroupCid = null;
        private PayGroupCid m_SecondLevelPayGroupCid = null;

        public virtual int SecondLevelSybRnk
        {
            get { return m_SecondLevelSybRnk; }
            set { m_SecondLevelSybRnk = value; }
        }

        public virtual int SecondLevelObjectNumber
        {
            get { return m_SecondLevelObjectNumber; }
            set { m_SecondLevelObjectNumber = value; }
        }

        public virtual int SecondLevelPayGroupNumber
        {
            get { return m_SecondLevelPayGroupNumber; }
            set { m_SecondLevelPayGroupNumber = value; }
        }

        public virtual int FirstLevelSybRnk
        {
            get { return m_FirstLevelSybRnk; }
            set { m_FirstLevelSybRnk = value; }
        }

        public virtual int FirstLevelObjectNumber
        {
            get { return m_FirstLevelObjectNumber; }
            set { m_FirstLevelObjectNumber = value; }
        }

        public virtual int FirstLevelPayGroupNumber
        {
            get { return m_FirstLevelPayGroupNumber; }
            set { m_FirstLevelPayGroupNumber = value; }
        }

        public virtual PayGroupCid FirstLevelPayGroupCid
        {
            get { return m_FirstLevelPayGroupCid; }
        }

        public virtual PayGroupCid SecondLevelPayGroupCid
        {
            get { return m_SecondLevelPayGroupCid; }
        }

        public override bool Equals(object obj)
        {
            if (!(obj is FirstLevelMeasurementCid)) return false;

            return this.Equals((FirstLevelMeasurementCid)obj);
        }

        public virtual bool Equals(FirstLevelMeasurementCid obj)
        {
            return (m_FirstLevelSybRnk.Equals(obj.m_FirstLevelSybRnk)
                && m_FirstLevelObjectNumber.Equals(obj.m_FirstLevelObjectNumber)
                && m_FirstLevelPayGroupNumber.Equals(obj.m_FirstLevelPayGroupNumber)
                && m_SecondLevelSybRnk.Equals(obj.m_SecondLevelSybRnk)
                && m_SecondLevelObjectNumber.Equals(obj.m_SecondLevelObjectNumber)
                && m_SecondLevelPayGroupNumber.Equals(obj.m_SecondLevelPayGroupNumber)
                );
        }

        public override int GetHashCode()
        {
            return Hash.Generate(m_FirstLevelSybRnk, m_FirstLevelObjectNumber, m_FirstLevelPayGroupNumber,
                m_SecondLevelSybRnk, m_SecondLevelObjectNumber, m_SecondLevelPayGroupNumber);
        }
    }
}
