﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AskueCtp.Common;

namespace AskueCtp.Entities
{
    [Serializable]
    public class Detail1Cid
    {
        private int m_SybRnk;
        private int m_BalanceElem;
        private int m_Id;

        public Detail1Cid()
        {
        }

        public Detail1Cid(int sybRnk, int balanceElem, int id)
        {
            m_SybRnk = sybRnk;
            m_BalanceElem = balanceElem;
            m_Id = id;
        }

        public virtual int SybRnk
        {
            get { return m_SybRnk; }
        }

        public virtual int BalanceElem
        {
            get { return m_BalanceElem; }
        }

        public virtual int Id
        {
            get { return m_Id; }
        }

        public override bool Equals(object obj)
        {
            if (!base.Equals(obj)) return false;

            if (obj == null) return false;

            if (this.GetType() != obj.GetType()) return false;

            Detail1Cid other = (Detail1Cid)obj;

            return (
                m_SybRnk.Equals(other.m_SybRnk)
                && m_BalanceElem.Equals(other.m_BalanceElem)
                && m_Id.Equals(other.m_Id)
                );
        }

        public override int GetHashCode()
        {
            return Hash.Generate(m_SybRnk, m_BalanceElem, m_Id);
        }
    }
}
