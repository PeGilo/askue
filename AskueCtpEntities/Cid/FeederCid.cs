﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AskueCtp.Common;

namespace AskueCtp.Entities
{
    public class FeederCid
    {
        private int m_SybRnk = default(int);
        private int m_ObjectNumber = default(int);
        private int m_FeederNumber = default(int);

        public virtual int FeederNumber
        {
            get { return m_FeederNumber; }
        }

        public virtual int ObjectNumber
        {
            get { return m_ObjectNumber; }
        }

        public virtual int SybRnk
        {
            get { return m_SybRnk; }
        }

        public override bool Equals(object obj)
        {
            if (!base.Equals(obj)) return false;

            if (obj == null) return false;

            if (this.GetType() != obj.GetType()) return false;

            FeederCid other = (FeederCid)obj;

            return (
                m_SybRnk.Equals(other.m_SybRnk)
                && m_ObjectNumber.Equals(other.m_ObjectNumber)
                && m_FeederNumber.Equals(other.m_FeederNumber)
                );
        }

        public override int GetHashCode()
        {
            return Hash.Generate(m_SybRnk, m_ObjectNumber, m_FeederNumber);
        }
    }
}
