﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AskueCtp.Common;

namespace AskueCtp.Entities
{
    [Serializable]
    public class Detail2Cid
    {
        private int m_SybRnk;
        private int m_BalanceElem;
        private int m_Detail1Id;
        private int m_Id;

        public Detail2Cid()
        {
        }

        public Detail2Cid(int sybRnk, int balanceElem, int detail1Id, int id)
        {
            m_SybRnk = sybRnk;
            m_BalanceElem = balanceElem;
            m_Detail1Id = detail1Id;
            m_Id = id;
        }

        public virtual int SybRnk
        {
            get { return m_SybRnk; }
        }

        public virtual int BalanceElem
        {
            get { return m_BalanceElem; }
        }

        public virtual int Detail1Id
        {
            get { return m_Detail1Id; }
        }

        public virtual int Id
        {
            get { return m_Id; }
        }

        public override bool Equals(object obj)
        {
            if (!base.Equals(obj)) return false;

            if (obj == null) return false;

            if (this.GetType() != obj.GetType()) return false;

            Detail2Cid other = (Detail2Cid)obj;

            return (
                m_SybRnk.Equals(other.m_SybRnk)
                && m_BalanceElem.Equals(other.m_BalanceElem)
                && m_Detail1Id.Equals(other.m_Detail1Id)
                && m_Id.Equals(other.m_Id)
                );
        }

        public override int GetHashCode()
        {
            return Hash.Generate(m_SybRnk, m_BalanceElem, m_Detail1Id, m_Id);
        }
    }
}
