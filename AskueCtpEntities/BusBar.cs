﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AskueCtp.Entities
{
    /// <summary>
    /// Шина, которая содержит фидеры
    /// </summary>
    public class BusBar
    {
        private BusBarCid m_Cid = null;
        private string m_Description = String.Empty;
        private int? m_AttachedPower = default(int?);
        private int? m_AttachedPowerReal = default(int?);
        private VoltageClass m_VoltageClass = null;

        public virtual BusBarCid Cid
        {
            get { return m_Cid; }
        }

        /// <summary>
        /// Can be null
        /// </summary>
        public virtual string Description
        {
            get { return m_Description; }
        }

        public virtual int? AttachedPower
        {
            get { return m_AttachedPower; }
        }

        public virtual int? AttachedPowerReal
        {
            get { return m_AttachedPowerReal; }
        }

        /// <summary>
        /// Can be null
        /// </summary>
        public virtual VoltageClass VoltageClass
        {
            get { return m_VoltageClass; }
        }

        public override bool Equals(object obj)
        {
            if (!base.Equals(obj)) return false;

            if (obj == null) return false;

            if (this.GetType() != obj.GetType()) return false;

            BusBar other = (BusBar)obj;

            return (
                m_Cid.Equals(other.m_Cid)
                );
        }

        public override int GetHashCode()
        {
            return m_Cid.GetHashCode();
        }
    }
}
