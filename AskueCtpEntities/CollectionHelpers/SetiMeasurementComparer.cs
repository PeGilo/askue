﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AskueCtp.Entities.CollectionHelpers
{
    public class SetiMeasurementComparer : IComparer
    {
        int IComparer.Compare(System.Object lhs, System.Object rhs)
        {
            SetiMeasurement s1 = lhs as SetiMeasurement;
            SetiMeasurement s2 = rhs as SetiMeasurement;
            if (s1.Cid.Date < s2.Cid.Date) return -1;
            else if (s1.Cid.Date > s2.Cid.Date) return 1;
            else return 0;
        }
    }
}
