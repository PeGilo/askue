﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AskueCtp.Entities
{
    /// <summary>
    /// Измерение входящее в группу 2-го уровня {Группа 1-го уровня, Знак}
    /// </summary>
    public class FirstLevelMeasurement
    {
        private FirstLevelMeasurementCid m_Cid = null;
        private FirstLevelPayGroup m_MeasureGroup = null;
        private SecondLevelPayGroup m_ParentGroup = null;
        private Sign m_Sign;

        public virtual FirstLevelMeasurementCid Cid
        {
            get { return m_Cid; }
        }

        public virtual FirstLevelPayGroup MeasureGroup
        {
            get { return m_MeasureGroup; }
            set { m_MeasureGroup = value; }
        }

        public virtual SecondLevelPayGroup ParentGroup
        {
            get { return m_ParentGroup; }
            set { m_ParentGroup = value; }
        }

        public virtual Sign Sign
        {
            get { return m_Sign; }
            set { m_Sign = value; }
        }
    }
}
