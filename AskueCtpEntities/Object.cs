﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AskueCtp.Entities
{
    public class Object
    {
        private ObjectCid m_Cid = null;

        private AskueCtp.Entities.Object m_ParentObject = null;
        private SybRnk m_SybRnk = null;
        private ICollection<AskueCtp.Entities.Object> m_Objects = new List<AskueCtp.Entities.Object>();
        private ICollection<FirstLevelPayGroup> m_FirstLevelPayGroups = new List<FirstLevelPayGroup>();
        private ICollection<SecondLevelPayGroup> m_SecondLevelPayGroups = new List<SecondLevelPayGroup>();
        private ICollection<Feeder> m_Feeders = new List<Feeder>();

        private string m_AccountantFio = String.Empty;
        private string m_AccountantPhone = String.Empty;
        private string m_Address = String.Empty;
        private Int32? m_CodeOKONH = default(int?);
        private DateTime? m_ContractDate = default(DateTime?);
        private string m_ContractState = String.Empty;
        private string m_Description = String.Empty;
        private string m_DirectorFio = String.Empty;
        private string m_DirectorPhone = String.Empty;
        private string m_EngineerFio = String.Empty;
        private string m_EngineerPhone = String.Empty;
        private string m_Fax = String.Empty;
        private DateTime? m_LiquidationDate = default(DateTime?);

        public virtual string ContractState
        {
            get { return m_ContractState; }
        }

        public virtual string Address
        {
            get { return m_Address; }
        }

        public virtual string Fax
        {
            get { return m_Fax; }
        }

        public virtual string DirectorFio
        {
            get { return m_DirectorFio; }
        }

        public virtual string DirectorPhone
        {
            get { return m_DirectorPhone; }
        }

        public virtual string AccountantFio
        {
            get { return m_AccountantFio; }
        }

        public virtual string AccountantPhone
        {
            get { return m_AccountantPhone; }
        }

        public virtual string EngineerFio
        {
            get { return m_EngineerFio; }
        }

        public virtual string EngineerPhone
        {
            get { return m_EngineerPhone; }
        }

        public virtual DateTime? ContractDate
        {
            get { return m_ContractDate; }
        }

        public virtual DateTime? LiquidationDate
        {
            get { return m_LiquidationDate; }
        }

        public virtual Int32? CodeOKONH
        {
            get { return m_CodeOKONH; }
        }

        public virtual ObjectCid Cid
        {
            get { return m_Cid; }
        }

        public virtual string Description
        {
            get { return m_Description; }
        }

        public virtual AskueCtp.Entities.Object ParentObject
        {
            get { return m_ParentObject; }
        }

        public virtual SybRnk SybRnk
        {
            get { return m_SybRnk; }
        }

        public virtual ICollection<FirstLevelPayGroup> FirstLevelPayGroups
        {
            get { return m_FirstLevelPayGroups; }
        }
        
        public virtual ICollection<SecondLevelPayGroup> SecondLevelPayGroups
        {
            get { return m_SecondLevelPayGroups; }
        }

        public virtual ICollection<Feeder> Feeders
        {
            get { return m_Feeders; }
        }

        public virtual ICollection<AskueCtp.Entities.Object> Objects
        {
            get { return m_Objects; }
        }

        public override bool Equals(object obj)
        {
            if (!base.Equals(obj)) return false;

            if (obj == null) return false;

            if (this.GetType() != obj.GetType()) return false;

            AskueCtp.Entities.Object other = (AskueCtp.Entities.Object)obj;

            return (this.m_Cid == other.m_Cid);
        }

        public override int GetHashCode()
        {
            return m_Cid.GetHashCode();
        }

        public override string ToString()
        {
            return "Cid=" + m_Cid.ToString() + " Text:" + m_Description;
        }
    }
}
