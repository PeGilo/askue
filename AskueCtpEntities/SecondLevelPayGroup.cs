﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AskueCtp.Entities
{
    public class SecondLevelPayGroup : IPayGroup
    {
        private PayGroupCid m_Cid = null;
        private AskueCtp.Entities.Object m_ParentObject = null;
        private ICollection<FirstLevelMeasurement> m_Measurements = new List<FirstLevelMeasurement>();
        private string m_Description = String.Empty;


        private string m_Creator = String.Empty;
        private DateTime? m_BeginDate = default(DateTime?);
        private DateTime? m_EndDate = default(DateTime?);
        private DateTime? m_CreationDate = default(DateTime?);
        private ReadingType m_ReadingType = null;
        private Detail1 m_Detail1 = null;
        private BalanceElement m_BalanceElement = null;

        public virtual PayGroupCid Cid
        {
            get { return m_Cid; }
        }

        public virtual string Description
        {
            get { return m_Description; }
            set { m_Description = value; }
        }

        public virtual AskueCtp.Entities.Object ParentObject
        {
            get { return m_ParentObject; }
            set { m_ParentObject = value; }
        }

        public virtual ICollection<FirstLevelMeasurement> Measurements
        {
            get { return m_Measurements; }
        }

        /// <summary>
        /// Тип показаний (АЭпр, АЭотд, РЭпр, ...)
        /// </summary>
        public virtual ReadingType ReadingType
        {
            get { return m_ReadingType; }
        }

        /// <summary>
        /// Детализация 1
        /// </summary>
        public virtual Detail1 Detail1
        {
            get { return m_Detail1; }
        }

        public virtual BalanceElement BalanceElement
        {
            get { return m_BalanceElement; }
        }

        public virtual DateTime? BeginDate
        {
            get { return m_BeginDate; }
        }

        public virtual DateTime? EndDate
        {
            get { return m_EndDate; }
        }

        public virtual DateTime? CreationDate
        {
            get { return m_CreationDate; }
        }

        /// <summary>
        /// Can be null
        /// </summary>
        public virtual string Creator
        {
            get { return m_Creator; }
        }

        public override bool Equals(object obj)
        {
            if (!base.Equals(obj)) return false;

            if (obj == null) return false;

            if (this.GetType() != obj.GetType()) return false;

            SecondLevelPayGroup other = (SecondLevelPayGroup)obj;

            return (
                m_Cid.Equals(other.m_Cid)
                );
        }

        public override int GetHashCode()
        {
            return m_Cid.GetHashCode();
        }
    }
}
