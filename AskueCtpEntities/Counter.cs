﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AskueCtp.Entities
{
    public class Counter
    {
        private Int32 m_Number = default(Int32);
        private Single? m_KT = default(Single?);
        private Single? m_KN = default(Single?);

        //private SortedList m_SetiMeasurements = new SortedList();
        private ICollection m_SetiMeasurements = new List<SetiMeasurement>();
        
        public virtual Int32 Number
        {
            get { return m_Number; }
        }

        public virtual Single? KT
        {
            get { return m_KT; }
        }

        public virtual Single? KN
        {
            get { return m_KN; }
        }

        public virtual ICollection SetiMeasurements
        {
            get { return m_SetiMeasurements; }
        }

        public override bool Equals(object obj)
        {
            if (!base.Equals(obj)) return false;

            if (obj == null) return false;

            if (this.GetType() != obj.GetType()) return false;

            Counter other = (Counter)obj;

            return (
                m_Number.Equals(other.m_Number)
                );
        }

        public override int GetHashCode()
        {
            return m_Number;
        }
    }
}
