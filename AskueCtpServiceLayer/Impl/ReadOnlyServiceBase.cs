﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AskueCtp.Common;
using AskueCtp.DataAccess;

namespace AskueCtp.ServiceLayer.Impl
{
    public class ReadOnlyServiceBase<T> : IReadOnlyService<T>
    {
        protected IReadOnlyRepository<T> m_Repository;
        protected ICache m_Cache = null;

        public ReadOnlyServiceBase(IReadOnlyRepository<T> rep)
        {
            m_Repository = rep;
        }

        public ReadOnlyServiceBase(IReadOnlyRepository<T> rep, ICache cache)
        {
            m_Repository = rep;
            m_Cache = null;
        }

        T IReadOnlyService<T>.GetById(Object id)
        {
            return m_Repository.GetById(id);
        }

        Object IReadOnlyService.GetById(Object id)
        {
            return m_Repository.GetById(id);
        }

        T[] IReadOnlyService<T>.GetAll()
        {
            return m_Repository.GetAll();
        }

        Object[] IReadOnlyService.GetAll()
        {
            return m_Repository.GetAll().Cast<Object>().ToArray<Object>();
        }
    }
}
