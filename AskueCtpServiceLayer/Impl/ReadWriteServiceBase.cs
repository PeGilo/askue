﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AskueCtp.DataAccess;

namespace AskueCtp.ServiceLayer.Impl
{
    public class ReadWriteServiceBase<T> : ReadOnlyServiceBase<T>, IReadWriteService<T>
    {
        private IReadWriteRepository<T> m_Repository;

        public ReadWriteServiceBase(IReadWriteRepository<T> rep)
            : base(rep as IReadOnlyRepository<T>)
        {
            m_Repository = rep;
        }

        void IReadWriteService<T>.Save(T entity)
        {
            m_Repository.Save(entity);
        }

        void IReadWriteService.Save(Object entity)
        {
            m_Repository.Save(entity);

//            ((IReadWriteRepository<T>)this).Save((T)entity);
        }
    }
}
