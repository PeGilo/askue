﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AskueCtp.Common;
using AskueCtp.DataAccess;
using AskueCtp.Entities;

namespace AskueCtp.ServiceLayer.Impl
{
    public class FirstLevelPayGroupService : ReadOnlyServiceBase<FirstLevelPayGroup>, IFirstLevelPayGroupService
    {
        //private IFirstLevelPayGroupRepository m_groupRepository = null;
        //private ISetiRepository m_setiRep = null;

        //public FirstLevelPayGroupService(IFirstLevelPayGroupRepository groupRep, ISetiRepository setiRep)
        //    : base(groupRep)
        //{
        //    m_groupRepository = groupRep;
        //    m_setiRep = setiRep;
        //}

        public FirstLevelPayGroupService(IFirstLevelPayGroupRepository groupRep)
            : base(groupRep)
        {
        }

        public FirstLevelPayGroupService(IFirstLevelPayGroupRepository groupRep, ICache cache)
            : base(groupRep, cache)
        {
        }
    }
}
