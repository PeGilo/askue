﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AskueCtp.DataAccess;
using AskueCtp.Entities;

namespace AskueCtp.ServiceLayer.Impl
{
    public class SybRnkService : ReadOnlyServiceBase<SybRnk>, ISybRnkService
    {
        private ISybRnkRepository m_repository;

        public SybRnkService(ISybRnkRepository rep)
            :base(rep)
        {
            m_repository = rep;
        }

        public SybRnk[] GetAllVisible()
        {
            return m_repository.GetAllVisible();
        }
    }
}
