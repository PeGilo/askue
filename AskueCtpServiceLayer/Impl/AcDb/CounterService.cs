﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AskueCtp.DataAccess;
using AskueCtp.Entities;

namespace AskueCtp.ServiceLayer.Impl
{
    public class CounterService : ReadOnlyServiceBase<Counter>, ICounterService
    {
        public CounterService(IReadOnlyRepository<Counter> rep)
            :base(rep)
        {
            //m_repository = rep;
        }
    }
}
