﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AskueCtp.Common;
using AskueCtp.DataAccess;
using AskueCtp.Entities;

namespace AskueCtp.ServiceLayer.Impl
{
    public class FeederService : ReadOnlyServiceBase<Feeder>, IFeederService
    {
        private IFeederRepository m_feederRep;

        public FeederService(IFeederRepository rep)
            :base(rep)
        {
            m_feederRep = rep;
        }

        public FeederService(IFeederRepository rep, ICache cache)
            : base(rep, cache)
        {
            m_feederRep = rep;
        }

        public string GetNameByCounterNumber(int counterNumber)
        {
            return m_feederRep.GetNameByCounterNumber(counterNumber);
        }
    }
}
