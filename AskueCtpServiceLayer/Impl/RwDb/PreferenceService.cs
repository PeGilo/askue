﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AskueCtp.Common;
using AskueCtp.DataAccess;
//using AskueCtp.Entities;
using AskueCtp.ServiceLayer.Entities;

namespace AskueCtp.ServiceLayer.Impl
{
    public class PreferenceService : IPreferenceService,
        IReadOnlyService<Preference>, 
        IReadWriteService<Preference>
    {
        private const int MAX_VALUE_LENGTH = 2000;

        private IPreferenceRepository m_repository;

        public PreferenceService(IPreferenceRepository rep)
        {
            m_repository = rep;
        }

        //Preference IPreferenceService.GetByCid(PreferenceCid cid)
        //{
        //    AskueCtp.Entities.PreferenceCid daoCid =
        //        new AskueCtp.Entities.PreferenceCid(cid.PreferenceId, cid.UserId, 1);
        //    // Here converter Service Cid -> Dao Cid
        //    //
        //    AskueCtp.Entities.Preference daoPref = m_repository.GetById(daoCid);
        //    // Here converter Dao Pref -> Service Pref
        //    //
        //    Preference pref = new Preference(cid, daoPref.Value);
        //    return pref;
        //}

        //public void Save(Preference preference)
        //{
        //    AskueCtp.Entities.PreferenceCid daoCid =
        //        new AskueCtp.Entities.PreferenceCid(preference.Cid.PreferenceId, preference.Cid.UserId);

        //    AskueCtp.Entities.Preference daoPref =
        //        new AskueCtp.Entities.Preference(daoCid, preference.Value);

        //    m_repository.Save(daoPref);
        //}

        // RW
        void IReadWriteService<Preference>.Save(Preference preference)
        {
            StringBuilder sb = new StringBuilder();

            int countWritten = 0; // How many symbols are written
            int partNo = 1; // Initial value
            string partValue;
            int partValueLength = 0;

            // One part should be written in any way, even if the length of the value is zero.
            do
            {
                // Get part of the value
                partValueLength = Math.Min(preference.Value.Length - countWritten, MAX_VALUE_LENGTH);
                partValue = preference.Value.Substring(countWritten, partValueLength);

                // Write the part
                AskueCtp.Entities.PreferenceCid daoCid =
                    new AskueCtp.Entities.PreferenceCid(preference.Cid.PreferenceId, preference.Cid.UserId, partNo);

                AskueCtp.Entities.Preference daoPref =
                    new AskueCtp.Entities.Preference(daoCid, partValue);

                m_repository.Save(daoPref);

                countWritten += partValueLength;
                partNo++;

                Console.WriteLine("[Save]: Written {0} symbols", countWritten);
            } while (countWritten != preference.Value.Length);
        }

        void IReadWriteService.Save(System.Object entity)
        {
            Preference pref = entity as Preference;
            ((IReadWriteService<Preference>)this).Save(entity);
        }

        // RO
        Preference IReadOnlyService<Preference>.GetById(System.Object id)
        {
            PreferenceCid cid = id as PreferenceCid;

            if (cid != null)
            {
                IList<AskueCtp.Entities.Preference> daoPrefs = m_repository.GetAllPartsSorted(
                    cid.PreferenceId, cid.UserId);

                if (daoPrefs != null && daoPrefs.Count > 0)
                {
                    // Concatenate all parts of the preference
                    StringBuilder value = new StringBuilder();
                    for (int i = 0; i < daoPrefs.Count; i++)
                    {
                        value.Append(daoPrefs[i].Value);
                    }

                    Preference pref = new Preference(cid, value.ToString());
                    return pref;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                throw new ArgumentException("Preference Id can not be null");
            }
        }

        System.Object IReadOnlyService.GetById(System.Object id)
        {
            return ((IReadOnlyService<Preference>)this).GetById(id);
        }

        Preference[] IReadOnlyService<Preference>.GetAll()
        {
            // Converting array of Dao Pref to array Service Pref
            // Parted preferences must be concatinated.
            //
            AskueCtp.Entities.Preference[] daoPrefs = m_repository.GetAll();

            IEnumerable<Preference> prefs = from g in
                                              from p in daoPrefs
                                              orderby p.Cid.PreferenceId, p.Cid.UserId, p.Cid.PartNo
                                              group p by new PreferenceKey(p.Cid.PreferenceId, p.Cid.UserId)
                                            select g.MyConcat<PreferenceKey>();
            return prefs.ToArray();
        }

        System.Object[] IReadOnlyService.GetAll()
        {
            return m_repository.GetAll().Cast<System.Object>().ToArray<System.Object>();
        }

        /// <summary>
        /// Class that realizes the Key entity for using in dictionaries when operating with
        /// preferences with LINQ.
        /// </summary>
        protected class PreferenceKey
        {
            private string m_PreferenceId;
            private string m_UserId;

            public string PreferenceId
            {
                get { return m_PreferenceId; }
                set { m_PreferenceId = value; }
            }

            public string UserId
            {
                get { return m_UserId; }
                set { m_UserId = value; }
            }

            public PreferenceKey()
            {
                m_PreferenceId = String.Empty;
                m_UserId = String.Empty;
            }

            public PreferenceKey(string preferenceId, string userId)
            {
                m_PreferenceId = preferenceId;
                m_UserId = userId;
            }

            public override bool Equals(object obj)
            {
                if (!(obj is PreferenceKey)) return false;

                return this.Equals((PreferenceKey)obj);
            }

            public virtual bool Equals(PreferenceKey obj)
            {
                return (m_PreferenceId.Equals(obj.m_PreferenceId)
                    && m_UserId == obj.m_UserId
                    );
            }

            public override int GetHashCode()
            {
                return Hash.Generate(m_PreferenceId.GetHashCode(),
                    ((m_UserId == null) ? 0 : m_UserId.GetHashCode()));
            }

            public override string ToString()
            {
                return "PrefId=" + m_PreferenceId.ToString()
                    + ((m_UserId == null) ? String.Empty : " UserId=" + m_UserId.ToString()
                    );
            }
        }
    }

    /// <summary>
    /// Contains methods for working with lists of preferences with LINQ
    /// </summary>
    static class ServicesExts
    {
        /// <summary>
        /// Concatenates a group of Preferences (DAO). Unites their Values.
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <param name="group"></param>
        /// <returns></returns>
        /// <devnote>NOTE: It takes Dao.Preferences and returns Service.Preferences</devnote>
        public static Preference MyConcat<TKey>(this IGrouping<TKey, AskueCtp.Entities.Preference> group)
        {
            int count = group.Count<AskueCtp.Entities.Preference>();

            if (count > 0)
            {
                Preference result = new Preference();
                // Build Cid
                AskueCtp.Entities.PreferenceCid daoPrefCid = group.ElementAt<AskueCtp.Entities.Preference>(0).Cid;
                result.Cid = new PreferenceCid(daoPrefCid.PreferenceId, daoPrefCid.UserId);
                // Build Value
                StringBuilder concat = new StringBuilder();
                foreach (AskueCtp.Entities.Preference pref in group)
                {
                    concat.Append(pref.Value);
                }
                result.Value = concat.ToString();
                return result;
            }
            else
            {
                return null;
            }
        }
    }
}
