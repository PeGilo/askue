﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AskueCtp.Common;
using AskueCtp.DataAccess;
using AskueCtp.Entities;

namespace AskueCtp.ServiceLayer.Impl
{
    public class MonitoringService : IMonitoringService
    {
        private class TotalConsumptionResult
        {
            public double Value { get; set; }
            public DateTime Start { get; set; }
            public DateTime End { get; set; }
            public Enums.MeasureQuality Quality { get; set; }
        }

        private ICache m_Cache;
        private IFirstLevelPayGroupRepository m_groupRepository = null;
        private ISetiRepository m_setiRep = null;
        private const double MAX_TIME_INTERVAL = 30.0;

        public MonitoringService()
        {
        }

        public MonitoringService(IFirstLevelPayGroupRepository groupRep, ISetiRepository setiRep, ICache cache)
        {
            m_groupRepository = groupRep;
            m_setiRep = setiRep;
            m_Cache = cache;
        }

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="groupId"></param>
        ///// <param name="time"></param>
        ///// <returns></returns>
        ///// <example>InvalidOperation</example>
        //public double GetTotalConsumption(PayGroupCid groupId, DateTime time, out DateTime start, out DateTime end,
        //    out Enums.MeasureQuality quality)
        //{
        //    return GetTotalConsumption(groupId, time.AddMinutes(-15), time.AddMinutes(15), out start, out end, out quality);
        //}

        public double GetTotalConsumption(PayGroupCid groupId, DateTime timeStart, DateTime timeEnd,
            out DateTime start, out DateTime end, out Enums.MeasureQuality quality)
        {

            string cacheKey = groupId.GetHashCode().ToString() + timeStart.ToString() + timeEnd.ToString();
            TotalConsumptionResult cacheValue = m_Cache.Get(cacheKey) as TotalConsumptionResult;
            if (cacheValue == null)
            {
                cacheValue = new TotalConsumptionResult();

                cacheValue.Value = RetrieveTotalConsumption(groupId, timeStart, timeEnd, out start, out end, out quality);
                cacheValue.Start = start;
                cacheValue.End = end;
                cacheValue.Quality = quality;

                m_Cache.Insert(cacheKey, cacheValue);
            }
            else
            {
                start = cacheValue.Start;
                end = cacheValue.End;
                quality = cacheValue.Quality;
            }

            return cacheValue.Value;
        }

        private double RetrieveTotalConsumption(PayGroupCid groupId, DateTime timeStart, DateTime timeEnd, 
            out DateTime start, out DateTime end, out Enums.MeasureQuality quality)
        {
            quality = (Enums.MeasureQuality)0;

            FirstLevelPayGroup groupObj = m_groupRepository.GetById(groupId);

            IList<SetiMeasurement> measures = m_setiRep.GetMeasurements(timeStart, timeEnd);
            
            // Filter measurements by Group
            Dictionary<int, int> groupCounters = (from pgm in groupObj.PayGroupMeasurements select pgm.CounterNumber).ToDictionary<int, int>(n => n);
            IList<SetiMeasurement> filteredMeasures = (from m in measures where groupCounters.ContainsKey(m.Cid.CounterNumber) select m).ToList<SetiMeasurement>();

            double totalConsumption = 0;
            DateTime minTime = DateTime.MaxValue, maxTime = DateTime.MinValue;
            ICollection<PayGroupMeasurement> measurements = groupObj.PayGroupMeasurements;
            foreach (PayGroupMeasurement m in measurements)
            {
                // Find measure
                SetiMeasurement measure = measures.FirstOrDefault<SetiMeasurement>(sm => sm.Cid.CounterNumber == m.CounterNumber);
                if (measure != null)
                {
                    // Определить минимальное и максимальное время измерений
                    if (measure.Cid.Date > maxTime)
                        maxTime = measure.Cid.Date;
                    if (measure.Cid.Date < minTime)
                        minTime = measure.Cid.Date;

                    // Getting first measurement in collection. It should be latest by time.
                    totalConsumption += measure.FullEnergy;// CalculateFullEnergy(measure);
                }
                else
                {
                    //throw new InvalidOperationException("No measurement for counter #" + m.Counter.Number);
                    quality |= Enums.MeasureQuality.MissingMeasures;
                }
            }

            if (groupObj.PayGroupMeasurements.Count > 0 && filteredMeasures.Count == 0)
            {
                quality |= Enums.MeasureQuality.NoMeasures;
            }

            // Проверить что все измерения в приемлемом интервале 
            if ((maxTime - minTime).TotalMinutes > MAX_TIME_INTERVAL)
            {
                throw new InvalidOperationException("Measurements have to large interval " + (maxTime - minTime).ToString());
            }
            start = minTime;
            end = maxTime;

            // test
            //quality = Enums.MeasureQuality.NoMeasures;

            return totalConsumption;
        }

        public virtual IList<SetiMeasurement> GetConsumptionByCounter(PayGroupCid groupId, 
            DateTime timeStart, 
            DateTime timeEnd)
        {
            FirstLevelPayGroup groupObj = m_groupRepository.GetById(groupId);

            IList<SetiMeasurement> measures = m_setiRep.GetMeasurements(timeStart, timeEnd);

            // Filter measurements by Group
            Dictionary<int, int> groupCounters = (from pgm in groupObj.PayGroupMeasurements select pgm.CounterNumber).ToDictionary<int, int>(n => n);
            IList<SetiMeasurement> filteredMeasures = (from m in measures where groupCounters.ContainsKey(m.Cid.CounterNumber) select m).ToList<SetiMeasurement>();

            return filteredMeasures;
        }

        /// <summary>
        /// Проверка на неполнофазный режим
        /// </summary>
        /// <param name="setiMeasurement">Значения мгновенных напряжений на фазах</param>
        /// <returns>True если неполнофазный режим, false - если все ok.</returns>
        public virtual bool CheckPhaseFailure(SetiMeasurement setiMeasurement)
        {
            // Для проверки мы проверяем есть ли фазы, отличающиеся по значению напряжения
            // в 10 раз. Достаточно проверить пары (A, B) и (B, C).
            // Примечание: деление на нуль можно не проверять, т.к. float.
            if (setiMeasurement.Ua.HasValue && setiMeasurement.Ub.HasValue && setiMeasurement.Uc.HasValue)
            {
                float a = setiMeasurement.Ua.Value;
                float b = setiMeasurement.Ub.Value;
                float c = setiMeasurement.Uc.Value;
                float e = 0.00001f;

                // Если все напряжение меньше 1, то не надо проверять
                if (Math.Abs(a) < 1.0f && Math.Abs(b) < 1.0f && Math.Abs(c) < 1.0f)
                {
                    return false;
                }

                // Если числа не равны, то проверить порядок разницы
                if (Math.Abs(a - b) > e)
                {
                    float ab = (Math.Abs(a) > Math.Abs(b)) ? b / a : a / b;
                    if (ab <= 0.1f)
                    {
                        return true;
                    }
                }

                if (Math.Abs(b - c) > e)
                {
                    float bc = (Math.Abs(b) > Math.Abs(c)) ? c / b : b / c;
                    if (bc <= 0.1f)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        //protected virtual double CalculateFullEnergy(SetiMeasurement seti)
        //{
        //    if (seti == null || !seti.Ia.HasValue || !seti.Ib.HasValue || !seti.Ic.HasValue
        //        || !seti.Ua.HasValue || !seti.Ub.HasValue || !seti.Uc.HasValue
        //        || !seti.PfAngA.HasValue || !seti.PfAngB.HasValue || !seti.PfAngC.HasValue)
        //    {
        //        throw new InvalidOperationException("Measurement contains null value");
        //    }

        //    double Pa, Pb, Pc, P;
        //    double Qa, Qb, Qc, Q;
        //    double S;

        //    // Активная мощность
        //    Pa = seti.Ia.Value * seti.Ua.Value * Math.Cos(seti.PfAngA.Value);
        //    Pb = seti.Ib.Value * seti.Ub.Value * Math.Cos(seti.PfAngB.Value);
        //    Pc = seti.Ic.Value * seti.Uc.Value * Math.Cos(seti.PfAngC.Value);

        //    // Реактивная мощность
        //    Qa = seti.Ia.Value * seti.Ua.Value * Math.Sin(seti.PfAngA.Value);
        //    Qb = seti.Ib.Value * seti.Ub.Value * Math.Sin(seti.PfAngB.Value);
        //    Qc = seti.Ic.Value * seti.Uc.Value * Math.Sin(seti.PfAngC.Value);

        //    // Полноая мощность
        //    P = Pa + Pb + Pc;
        //    Q = Qa + Qb + Qc;

        //    S = Math.Sqrt(P * P + Q * Q);
        //    return S;
        //}

    }
}
