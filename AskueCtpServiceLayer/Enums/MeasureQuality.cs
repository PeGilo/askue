﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AskueCtp.ServiceLayer.Enums
{
    [Flags]
    public enum MeasureQuality
    {
        MissingMeasures = 0x001,
        NoMeasures = 0x002
    }
}
