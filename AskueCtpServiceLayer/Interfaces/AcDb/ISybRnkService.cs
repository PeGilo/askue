﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AskueCtp.Entities;

namespace AskueCtp.ServiceLayer
{
    public interface ISybRnkService : IReadOnlyService<SybRnk>
    {
        SybRnk[] GetAllVisible();
    }
}
