﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AskueCtp.ServiceLayer
{
    public interface IReadOnlyService
    {
        object[] GetAll();
        object GetById(Object id);
    }

    public interface IReadOnlyService<T> : IReadOnlyService
    {
        new T[] GetAll();
        new T GetById(Object id);
    }
}
