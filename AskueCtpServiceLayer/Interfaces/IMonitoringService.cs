﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AskueCtp.Entities;

namespace AskueCtp.ServiceLayer
{
    public interface IMonitoringService
    {
        //double GetTotalConsumption(PayGroupCid groupId, DateTime time, 
        //    out DateTime start, out DateTime end, out Enums.MeasureQuality quality);
        double GetTotalConsumption(PayGroupCid groupId, DateTime timeStart, DateTime timeEnd, 
            out DateTime start, out DateTime end, out Enums.MeasureQuality quality);
        IList<SetiMeasurement> GetConsumptionByCounter(PayGroupCid groupId, DateTime timeStart, DateTime timeEnd);
        bool CheckPhaseFailure(SetiMeasurement setiMeasurement);
    }
}
