﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AskueCtp.ServiceLayer
{
    public interface IReadWriteService : IReadOnlyService
    {
        void Save(Object entity);
    }

    public interface IReadWriteService<T> : IReadWriteService
    {
        void Save(T entity);
        //void Delete(T entity);
    }
}
