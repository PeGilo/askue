﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using AskueCtp.Common;
using AskueCtp.Common.Collections;

namespace AskueCtpCommonTests
{
    [TestClass]
    public class CollectionHelperTests
    {
        class TestItem
        {
            public string Text { get; set; }
            public int Id { get; set; }
        }

        List<TestItem> items;

        [TestInitialize]        
        public virtual void InitTest()
        {
            items = new List<TestItem>();
            items.Add(new TestItem() { Id = 1, Text = "Один" });
            items.Add(new TestItem() { Id = 2, Text = "Два" });
            items.Add(new TestItem() { Id = 3, Text = "Три" });
        }

        [TestMethod]
        public virtual void Test1()
        {

            TestItem result1 = items.FindNextAfterOrFirst<TestItem>(i => i.Id == 2);
            Assert.IsNotNull(result1);
            Assert.AreEqual(3, result1.Id);

            TestItem result2 = items.FindNextAfterOrFirst<TestItem>(i => i.Id == 3);
            Assert.IsNotNull(result2);
            Assert.AreEqual(1, result2.Id);

            TestItem result3 = items.FindNextAfterOrFirst<TestItem>(i => i.Id == 5);
            Assert.IsNotNull(result3);
            Assert.AreEqual(1, result3.Id);
        }
    }
}
