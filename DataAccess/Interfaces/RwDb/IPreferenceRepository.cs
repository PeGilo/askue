﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AskueCtp.Entities;

namespace AskueCtp.DataAccess
{
    public interface IPreferenceRepository : IReadWriteRepository<Preference>
    {
        IList<Preference> GetAllPartsSorted(string preferenceId, string userId);
    }
}
