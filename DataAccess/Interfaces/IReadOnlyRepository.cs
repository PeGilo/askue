﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AskueCtp.DataAccess
{
    public interface IReadOnlyRepository
    {
        object[] GetAll();
        object GetById(Object id);
    }

    public interface IReadOnlyRepository<T> : IReadOnlyRepository
    {
        new T[] GetAll();
        new T GetById(Object id);
    }
}
