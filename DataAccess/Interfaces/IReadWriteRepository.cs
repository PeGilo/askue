﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AskueCtp.DataAccess
{
    /// <summary>
    /// Used for testing - unique interface for saving/getting entities.
    /// </summary>
    public interface IReadWriteRepository : IReadOnlyRepository
    {
        void Save(Object entity);
        void Delete(Object entity);
    }

    //    public interface IRepository<T> where T : PersistentObject
    public interface IReadWriteRepository<T> : IReadWriteRepository , IReadOnlyRepository<T>
    {
        void Save(T entity);
        void Delete(T entity);
    }
}
