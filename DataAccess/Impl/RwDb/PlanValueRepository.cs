﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NHibernate;

using AskueCtp.Entities;

namespace AskueCtp.DataAccess.Impl
{
    public class PlanValueRepository : ReadWriteRepositoryBase<PlanValue>, IPlanValueRepository
    {
        public PlanValueRepository(ISessionBuilder sessionBuilder)
            : base(sessionBuilder)
		{
		}
    }
}
