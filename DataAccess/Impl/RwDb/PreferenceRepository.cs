﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NHibernate;

using AskueCtp.Entities;

namespace AskueCtp.DataAccess.Impl
{
    public class PreferenceRepository : ReadWriteRepositoryBase<Preference>, 
        IPreferenceRepository, IReadOnlyRepository<Preference>//, IReadWriteRepository<Preference>
    {
        public PreferenceRepository(ISessionBuilder sessionBuilder)
            : base(sessionBuilder)
		{
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cid"></param>
        /// <returns>null if no such preference</returns>
        /// <exception cref="System.ArgumentException"></exception>
        Preference IReadOnlyRepository<Preference>.GetById(System.Object id)
        {
            PreferenceCid cid = id as PreferenceCid;
            if (cid == null)
            {
                throw new ArgumentException("Parameter is not composit id - PreferenceId");
            }

            ISession session = GetSession();
            IQuery query;
            if (cid.UserId != null)
            {
                query = session.CreateQuery("from Preference p where p.Cid.UserId = :userid and p.Cid.PreferenceId = :prefid and p.Cid.PartNo = :partno");
                query.SetParameter("userid", cid.UserId, NHibernateUtil.String);
            }
            else
            {
                query = session.CreateQuery("from Preference p where p.Cid.UserId is null and p.Cid.PreferenceId = :prefid and p.Cid.PartNo = :partno");
            }
            query.SetParameter("prefid", cid.PreferenceId);
            query.SetParameter("partno", cid.PartNo);
            IList<Preference> list = query.List<Preference>();
            if (list.Count > 0)
                return list[0];
            else
                return null;
        }

        public IList<Preference> GetAllPartsSorted(string preferenceId, string userId)
        {
            if (String.IsNullOrEmpty(preferenceId) || String.IsNullOrEmpty(userId))
            {
                throw new ArgumentException("Id of preference and user must not be null");
            }

            ISession session = GetSession();
            IQuery query = session.CreateQuery(
                "from Preference p"
                + " where p.Cid.UserId = :userid and p.Cid.PreferenceId = :prefid"
                + " order by p.Cid.PartNo asc");
            query.SetParameter("userid", userId, NHibernateUtil.String);
            query.SetParameter("prefid", preferenceId, NHibernateUtil.String);
            IList<Preference> list = query.List<Preference>();

            return list;
        }

        //public void Update(Preference preference)
        //{
        //    ISession session = GetSession();
        //    session.SaveOrUpdateCopy(preference);
        //    session.Flush();
        //}
    }
}
