﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AskueCtp.Entities;

namespace AskueCtp.DataAccess.Impl
{
    public class DeviationRepository : ReadWriteRepositoryBase<Deviation>, IDeviationRepository
    {
        public DeviationRepository(ISessionBuilder sessionBuilder)
            : base(sessionBuilder)
		{
		}
    }
}
