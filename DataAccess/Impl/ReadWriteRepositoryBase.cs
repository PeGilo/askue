﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NHibernate;

namespace AskueCtp.DataAccess.Impl
{
    //    public class RepositoryBase<T> : RepositoryBase, IRepository<T> where T : PersistentObject
    public class ReadWriteRepositoryBase<T> : ReadOnlyRepositoryBase<T>, IReadWriteRepository<T>
    {
        public ReadWriteRepositoryBase(ISessionBuilder sessionBuilder)
            : base(sessionBuilder)
        {
        }

        void IReadWriteRepository<T>.Save(T entity)
        {
            using (ISession session = GetSession())
            using (ITransaction tx = session.BeginTransaction())
            {
                session.SaveOrUpdateCopy(entity);
                tx.Commit();
            }
        }

        void IReadWriteRepository.Save(Object entity)
        {
            ((IReadWriteRepository<T>)this).Save((T)entity);
        }

        void IReadWriteRepository<T>.Delete(T entity)
        {
            using (ISession session = GetSession())
            using (ITransaction tx = session.BeginTransaction())
            {
                session.Delete(entity);
                tx.Commit();
            }
        }

        void IReadWriteRepository.Delete(Object entity)
        {
            ((IReadWriteRepository<T>)this).Delete((T)entity);
        }
    }
}
