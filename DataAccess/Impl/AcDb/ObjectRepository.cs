﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NHibernate;

namespace AskueCtp.DataAccess.Impl
{
    public class ObjectRepository : ReadOnlyRepositoryBase<AskueCtp.Entities.Object>, IObjectRepository
    {
        public ObjectRepository(ISessionBuilder sessionBuilder)
            : base(sessionBuilder)
		{
		}
    }
}
