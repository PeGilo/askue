﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NHibernate;

using AskueCtp.Entities;

namespace AskueCtp.DataAccess.Impl
{
    public class SecondLevelPayGroupRepository : ReadOnlyRepositoryBase<SecondLevelPayGroup>, ISecondLevelPayGroupRepository
    {
        public SecondLevelPayGroupRepository(ISessionBuilder sessionBuilder)
            : base(sessionBuilder)
        {
        }
    }
}
