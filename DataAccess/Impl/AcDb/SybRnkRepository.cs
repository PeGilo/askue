﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NHibernate;

using AskueCtp.Entities;

namespace AskueCtp.DataAccess.Impl
{
    public class SybRnkRepository : ReadOnlyRepositoryBase<SybRnk>, ISybRnkRepository
    {
        public SybRnkRepository(ISessionBuilder sessionBuilder)
            : base(sessionBuilder)
		{
		}

		public SybRnk[] GetAllVisible()
		{
			ISession session = GetSession();
            IQuery query = session.CreateQuery("from SybRnk s where s.Visibility = :visible");
            query.SetBoolean("visible", true);
			IList<SybRnk> list = query.List<SybRnk>();
			return list.ToArray();
		}
    }
}
