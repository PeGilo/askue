﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NHibernate;
using NHibernate.Criterion;

using AskueCtp.Common;
using AskueCtp.DataAccess;
using AskueCtp.Entities;

namespace AskueCtp.DataAccess.Impl
{
    public class FeederRepository : ReadOnlyRepositoryBase<Feeder>, IFeederRepository
    {
        public FeederRepository(ISessionBuilder sessionBuilder)
            : base(sessionBuilder)
		{
		}

        public FeederRepository(ISessionBuilder sessionBuilder, ICache cache)
            : base(sessionBuilder, cache)
        {
        }

//        public string GetNameByCounterNumber(int counterNumber)
//        {
//            ISession session = GetSession();

//            ISQLQuery query = session.CreateSQLQuery(
//"select FID.N_OB, FID.SYB_RNK, FID.N_SHINY, FID.N_FID, FID.TXT, FID.KT_1, FID.KT_2, FID.KN_1, FID.KN_2, PROF_FID.N_SH "
//+ " from FID join PROF_FID on FID.N_FID = PROF_FID.N_FID"
//                );

//            IList result = query.List();
//            int length = result.Count;
//            int number;
//            for (int i = 0; i < length; i++)
//            {
//                Array array = result[i] as Array;//[9];
//                if(array != null && array.Length >= 10)
//                {
//                    object numberObj = array.GetValue(9);
//                    if(numberObj != null && Int32.TryParse(numberObj.ToString(), out number) && number == counterNumber)
//                    {
//                        object nameObj = array.GetValue(4);
//                        return nameObj != null ? nameObj.ToString() : String.Empty;
//                    }
//                }
//            }

//            return String.Empty;
//        }

        public string GetNameByCounterNumber(int counterNumber)
        {
            Dictionary<int, string> store = null;

            // 1. Try to get from cache
            store = (m_Cache != null) ? 
                m_Cache.Get("FeederRepository_CounterNames") as Dictionary<int, string>
                : null;

            // 2. If there is no data in cache retreive it from database
            if (store == null)
            {
                store = new Dictionary<int, string>();

                ISession session = GetSession();

                ISQLQuery query = session.CreateSQLQuery(
    "select FID.N_OB, FID.SYB_RNK, FID.N_SHINY, FID.N_FID, FID.TXT, FID.KT_1, FID.KT_2, FID.KN_1, FID.KN_2, PROF_FID.N_SH "
    + " from FID join PROF_FID on FID.N_FID = PROF_FID.N_FID and FID.N_OB = PROF_FID.N_OB and FID.SYB_RNK = PROF_FID.SYB_RNK"
                    );

                IList result = query.List();
                int length = result.Count;
                int number;

                // Iterate through result and search match
                for (int i = 0; i < length; i++)
                {
                    Array array = result[i] as Array;//[9];
                    if (array != null && array.Length >= 10)
                    {
                        object numberObj = array.GetValue(9);
                        if (numberObj != null && Int32.TryParse(numberObj.ToString(), out number))
                        {
                            object nameObj = array.GetValue(4);
                            string name = nameObj != null ? nameObj.ToString() : String.Empty;

                            // If cache is enabled then fill dictionary
                            if (m_Cache != null)
                            {
                                store[number] = name;
                            }
                            // If cache is disabled then return first match
                            else
                            {
                                if(number == counterNumber)
                                {
                                    return name;
                                }
                            }
                        }
                    }
                }

                string returnValue = (store.ContainsKey(counterNumber) ? store[counterNumber] : String.Empty);
                if (m_Cache != null)
                {
                    m_Cache.Insert("FeederRepository_CounterNames", store);
                }

                return returnValue;
            }

            else
            {
                return (store.ContainsKey(counterNumber) ? store[counterNumber] : String.Empty);
            }
        }
    }
}
