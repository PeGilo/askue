﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NHibernate;

using AskueCtp.Entities;

namespace AskueCtp.DataAccess.Impl
{
    public class FeederMeasurementRepository : ReadOnlyRepositoryBase<FeederMeasurement>, 
        IFeederMeasurementRepository
    {
        public FeederMeasurementRepository(ISessionBuilder sessionBuilder)
            : base(sessionBuilder)
		{
		}
    }
}
