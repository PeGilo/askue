﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NHibernate;

using AskueCtp.Common;
using AskueCtp.Entities;

namespace AskueCtp.DataAccess.Impl
{
    public class FirstLevelPayGroupRepository : ReadOnlyRepositoryBase<FirstLevelPayGroup>, IFirstLevelPayGroupRepository
    {
        public FirstLevelPayGroupRepository(ISessionBuilder sessionBuilder)
            : base(sessionBuilder)
		{
		}

        public FirstLevelPayGroupRepository(ISessionBuilder sessionBuilder, ICache cache)
            : base(sessionBuilder, cache)
        {
        }
    }
}
