﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NHibernate;
using NHibernate.Criterion;

using AskueCtp.Common;
using AskueCtp.DataAccess;
using AskueCtp.Entities;

namespace AskueCtp.DataAccess.Impl
{
    public class SetiRepository : ReadOnlyRepositoryBase<SetiMeasurement>, ISetiRepository
    {
        public SetiRepository(ISessionBuilder sessionBuilder)
            : base(sessionBuilder)
		{
		}

        public SetiRepository(ISessionBuilder sessionBuilder, ICache cache)
            : base(sessionBuilder, cache)
        {
        }

        public IList<SetiMeasurement> GetMeasurements(DateTime start, DateTime end)
        {
            ISession session = GetSession();
            //IQuery query = session.CreateQuery("from SetiMeasurement s where s.Cid.Date = :visible");
            //query.SetDateTime("startDate", start);
            //query.SetDateTime("endDate", end);
            //IList<SetiMeasurement> list = query.List<SetiMeasurement>();
            //return list;


            ICriteria measures = session.CreateCriteria(typeof(SetiMeasurement))
                .Add(Expression.Between("Cid.Date", start, end));


            //IQuery query = session.CreateQuery("select tA.N_sh, tA.t, tB.VAA "
            //                                + "from"
            //                                + "("
            //                                + "select N_Sh, max(testtime) as t "
            //                                + "from test_seti"
            //                                + " where testtime > :start and testtime < :end"
            //                                + " group by N_Sh"
            //                                + ") tA"
            //                                + " join "
            //                                + "("
            //                                + "select N_Sh, testtime as t, VAA "
            //                                + " from test_seti"
            //                                + " where testtime > :start and testtime < :end"
            //                                + ") tB"
            //                                + " on tA.N_Sh = tB.N_Sh and tA.t = tB.t");

            ISQLQuery query = session.CreateSQLQuery("select tA.N_SH, tA.TESTTIME, tB.IA, tB.IB, tB.IC, tB.UA, tB.UB, tB.UC, tB.PFANGA, tB.PFANGB, tB.PFANGC "
                                            + "from"
                                            + "("
                                            + "select N_Sh, max(testtime) as TESTTIME "
                                            + "from test_seti"
                                            + " where testtime > :start and testtime < :end"
                                            + " group by N_Sh"
                                            + ") tA"
                                            + " join "
                                            + "("
                                            + "select N_Sh, testtime as TESTTIME, IA, IB, IC, UA, UB, UC, PFANGA, PFANGB, PFANGC "
                                            + " from test_seti"
                                            + " where testtime > :start and testtime < :end"
                                            + ") tB"
                                            + " on tA.N_Sh = tB.N_Sh and tA.TESTTIME = tB.TESTTIME").AddEntity(typeof(SetiMeasurement));

            query.SetDateTime("start", start);
            query.SetDateTime("end", end);
            return query.List<SetiMeasurement>();
        }
    }
}
