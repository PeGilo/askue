﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NHibernate;

using AskueCtp.Common;

namespace AskueCtp.DataAccess.Impl
{
    public class ReadOnlyRepositoryBase<T> : RepositoryBase, IReadOnlyRepository<T>
    {
        public ReadOnlyRepositoryBase(ISessionBuilder sessionBuilder)
            : base(sessionBuilder)
        {
        }

        public ReadOnlyRepositoryBase(ISessionBuilder sessionBuilder, ICache cache)
            : base(sessionBuilder, cache)
        {
        }

        T IReadOnlyRepository<T>.GetById(Object id)
        {
            ISession session = GetSession();
            return session.Get<T>(id);
        }

        Object IReadOnlyRepository.GetById(Object id)
        {
            return ((IReadOnlyRepository<T>)this).GetById(id);
        }

        T[] IReadOnlyRepository<T>.GetAll()
        {
            ISession session = GetSession();
            ICriteria criteria = session.CreateCriteria(typeof(T));
            return criteria.List<T>().ToArray();
        }

        Object[] IReadOnlyRepository.GetAll()
        {
            ISession session = GetSession();
            ICriteria criteria = session.CreateCriteria(typeof(T));
            return criteria.List<T>().Cast<Object>().ToArray<Object>();
        }
    }
}
