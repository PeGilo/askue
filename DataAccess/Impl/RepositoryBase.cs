﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NHibernate;
using NHibernate.Cfg;

using AskueCtp.Common;

namespace AskueCtp.DataAccess
{
    public class RepositoryBase
    {
        private readonly ISessionBuilder m_sessionBuilder;
        protected ICache m_Cache = null;

        protected RepositoryBase(ISessionBuilder sessionBuilder)
        {
            m_sessionBuilder = sessionBuilder;
        }

        protected RepositoryBase(ISessionBuilder sessionBuilder, ICache cache)
        {
            m_sessionBuilder = sessionBuilder;
            m_Cache = cache;
        }

        protected ISession GetSession()
        {
            return m_sessionBuilder.GetSession();
        }
    }
}
