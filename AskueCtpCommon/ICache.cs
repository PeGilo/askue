﻿using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;

namespace AskueCtp.Common
{
    public interface ICache
    {
        void Insert(string key, object value);

        void Insert(string key, object value, DateTime absoluteExpiration);

        Object Get(string key);
    }
}