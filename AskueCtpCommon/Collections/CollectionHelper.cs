﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AskueCtp.Common.Collections
{
    public static class CollectionHelper
    {
        public static T FindNextAfterOrFirst<T>(this IEnumerable<T> collection, Predicate<T> predicate)
        {
            IEnumerator<T> en = collection.GetEnumerator();
            while (en.MoveNext())
            {
                if (predicate(en.Current))
                {
                    break;
                }
            }

            if (en.MoveNext())
            {
                return en.Current;
            }

            en.Reset();
            if (en.MoveNext())
            {
                return en.Current;
            }

            return default(T);
        }
    }
}
