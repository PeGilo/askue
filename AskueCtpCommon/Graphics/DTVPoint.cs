﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AskueCtp.Common.Graphics
{
    public class DTVPoint
    {
        public DateTime Time { get; set; }
        public Double? Value { get; set; }

        public DTVPoint()
        {
            Time = DateTime.MinValue;
            Value = 0;
        }

        public DTVPoint(DateTime time, double? value)
        {
            Time = time;
            Value = value;
        }
    }
}
