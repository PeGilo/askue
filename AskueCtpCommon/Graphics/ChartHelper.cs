﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Text;
using System.Threading;
//using System.Windows.Forms;

using NPlot;
using NPlot.Windows;


namespace AskueCtp.Common.Graphics
{
    public class ChartHelper
    {
        private NPlot.Windows.PlotSurface2D m_PlotSurface;
        private string m_StorePath;
        private int m_Width;
        private int m_Height;
        private Color m_PanColor = Color.Black; // default value

        /// <summary>
        /// 
        /// </summary>
        /// <param name="storePath">Path to store generated charts</param>
        public ChartHelper(int width, int height, string storePath, Color panColor)
        {
            m_Width = width;
            m_Height = height;
            m_StorePath = storePath;
            m_PanColor = panColor;

            m_PlotSurface = new NPlot.Windows.PlotSurface2D();
            this.m_PlotSurface.Size = new System.Drawing.Size(m_Width, m_Height);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="points"></param>
        /// <param name="fileName">file short name</param>
        /// <returns></returns>
        public void SaveChart(IEnumerable<DTVPoint> points, string fileName)
        {
            GenerateChart(points);

            using (Bitmap bmp = new Bitmap(m_Width, m_Height))
            {
                m_PlotSurface.DrawToBitmap(bmp, new Rectangle(0, 0, m_Width, m_Height));

                Mutex mutex = new Mutex(false, fileName);
                mutex.WaitOne();
                try
                {
                    bmp.Save(System.IO.Path.Combine(m_StorePath, fileName), System.Drawing.Imaging.ImageFormat.Gif);
                }
                finally
                {
                    mutex.ReleaseMutex();
                }
            }
        }

        private IList<LinePlot> GeneratePlots(IEnumerable<DTVPoint> points)
        {
            Pen linePen = new Pen(m_PanColor, 3.0f);

            List<LinePlot> plots = new List<LinePlot>();
            LinePlot lp = null;// new LinePlot();
            List<DateTime> dts = null;// new List<DateTime>();
            List<double> values = null;// new List<double>();
            DateTime minDate = DateTime.MaxValue;
            DateTime maxDate = DateTime.MinValue;
            double minValue = Double.MaxValue;
            double maxValue = Double.MinValue;

            foreach (DTVPoint p in points)
            {
                if (p.Time < minDate) minDate = p.Time;
                if (p.Time > maxDate) maxDate = p.Time;
                

                if (p.Value.HasValue)
                {
                    if (p.Value.Value < minValue) minValue = p.Value.Value; 
                    if (p.Value.Value > maxValue) maxValue = p.Value.Value;

                    if (lp == null)
                    {
                        lp = new LinePlot();
                        dts = new List<DateTime>();
                        values = new List<double>();
                    }
                    dts.Add(p.Time);
                    values.Add(p.Value.Value);
                }
                else
                {
                    if (lp != null)
                    {
                        lp.AbscissaData = dts;
                        lp.OrdinateData = values;
                        lp.Pen = linePen;
                        plots.Add(lp);
                        lp = null;
                    }
                }
            }

            if (lp != null)
            {
                lp.AbscissaData = dts;
                lp.OrdinateData = values;
                lp.Pen = linePen;
                plots.Add(lp);
            }

            // Добавить график с двумя точками для расширения области рисования
            lp = new LinePlot();
            dts = new List<DateTime>() { new DateTime(minDate.Year, minDate.Month, minDate.Day, 0, 0, 0), 
                                         new DateTime(maxDate.Year, maxDate.Month, maxDate.Day, 23, 59, 59)};
            values = new List<double>() { minValue, maxValue };
            lp.AbscissaData = dts;
            lp.OrdinateData = values;
            lp.Pen = new Pen(Color.Transparent, 1.0f);
            m_PlotSurface.Add(lp);

            return plots;
        }

        private void GenerateChart(IEnumerable<DTVPoint> points)
        {
            m_PlotSurface.Clear();
            m_PlotSurface.Add(new Grid());

            IList<LinePlot> plots = GeneratePlots(points);
            foreach (var p in plots)
            {
                m_PlotSurface.Add(p);
            }

   

            //const int size = 200;
            //float[] xs = new float[size];
            //float[] ys = new float[size];

            //DateTime[] dts = new DateTime[size + 1];

            //for (int i = 0; i < size; i++)
            //{
            //    xs[i] = (float)i * 5;
            //    ys[i] = i;


            //    dts[i] = new DateTime(2011, 08, 08, i / 60, i % 60, 0);
            //}




            //dts[size] = new DateTime(2011, 08, 08, 23, 59, 59);

            

            //IList<DateTime> dts = (from p in points.AsQueryable<DTVPoint>() select p.Time).ToList();
            //IList<double> values = (from p in points.AsQueryable<DTVPoint>() select p.Value.HasValue ? p.Value.Value : 0).ToList();
            //LinePlot lp = new LinePlot();
            



            //lp.OrdinateData = values;
            //lp.AbscissaData = dts;
            //m_PlotSurface.Add(lp);

            //plotSurface.Title = "AxisConstraint.EqualScaling in action...";

            #region "Legend"
            //Legend legend = new Legend();
            //legend.AttachTo(NPlot.PlotSurface2D.XAxisPosition.Top, NPlot.PlotSurface2D.YAxisPosition.Right);
            //legend.VerticalEdgePlacement = Legend.Placement.Outside;
            //legend.HorizontalEdgePlacement = Legend.Placement.Inside;
            //legend.XOffset = 5; // note that these numbers can be negative.
            //legend.YOffset = 0;
            //plotSurface.Legend = legend;
            #endregion

            m_PlotSurface.XAxis1.Label = "Время, ч";
            m_PlotSurface.YAxis1.Label = "Мощность, КВт";
            m_PlotSurface.YAxis1.HideTickText = false;
            m_PlotSurface.YAxis1.NumberFormat = "{0:#,0}";
            //plotSurface.YAxis1.LargeTickSize = 100;
            //plotSurface.XAxis1.AutoScaleTicks = true;
            //plotSurface.XAxis1.TicksCrossAxis = true;
            //((DateTimeAxis)plotSurface.XAxis1).

            DateTimeAxis myAxis = new DateTimeAxis(this.m_PlotSurface.XAxis1);
            //myAxis.NumberOfSmallTicks = 2;
            myAxis.LargeTickStep = new TimeSpan(1, 0, 0);
            myAxis.NumberFormat = "HH";//"t";
            //myAxis.AutoScaleTicks = true;
            // myAxis.MinPhysicalLargeTickStep = 1000;
            //myAxis.AutoScaleText = true;
            //myAxis.TicksLabelAngle = 90.0F;
            //myAxis.TickTextNextToAxis = true;
            myAxis.HideTickText = false;
            //myAxis.TicksIndependentOfPhysicalExtent = true;
            //myAxis.TickTextNextToAxis = true;

            //myAxis.TicksLabelAngle = (float)45.0f;
            //myAxis.TicksBetweenText = true;

            //myAxis.FlipTicksLabel = true;
            myAxis.TickTextFont = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            //myAxis.WorldMin += plotSurface.XAxis1.WorldLength / 4.0;
            this.m_PlotSurface.XAxis1 = myAxis;



            m_PlotSurface.Refresh();
        }
    }
}
