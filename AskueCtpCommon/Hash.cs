﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AskueCtp.Common
{
    public static class Hash
    {
        public static int Generate(params int[] arglist)
        {
            uint h = 0;
            uint highorder;
            for (int i = 0; i < arglist.Length; i++)
            {
                highorder = h & 0xf8000000;
                h = h << 5;
                h = h ^ (highorder >> 27);
                h = h ^ (uint)arglist[i];
            }

            return (int)h;
        }
    }
}
