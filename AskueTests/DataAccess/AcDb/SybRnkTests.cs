﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;

using AskueCtp.Common;
using AskueCtp.DataAccess;
using AskueCtp.DataAccess.Impl;
using AskueCtp.Entities;

namespace AskueCtp.Tests.DataAccess.AcDb
{
    [TestFixture]
    class SybRnkTests : ReadOnlyRepositoryTest
    {
        private object[] m_Cids = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 100, 101 };
        private object[] m_NonExistingCids = { -1, -1000, 1001, 1002, 1003 };

        protected override string TableName { get { return "cnt.syb_rnk"; } }

        protected override IReadOnlyRepository BuildRORepository(ISessionBuilder sessionBuilder)
        {
            return new SybRnkRepository(sessionBuilder);
        }

        protected override object[] ExistingCids { get { return m_Cids; } }

        protected override object[] NonExistingCids { get { return m_NonExistingCids; } }

        [Test]
        public void GetAllVisible()
        {
            ISessionBuilder sessionBuilder = GetSessionBuiler();
            ISybRnkRepository rep = new SybRnkRepository(sessionBuilder);

            SybRnk[] readItems = rep.GetAllVisible();

            Assert.AreNotEqual(null, readItems, "Could not read SybRnk items");
            foreach (SybRnk item in readItems)
            {
                Assert.AreEqual(true, item.Visibility, "There are unvisible SybRnk");
            }
        }
    }
}
