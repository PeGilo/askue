﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;

using AskueCtp.Common;
using AskueCtp.DataAccess;
using AskueCtp.DataAccess.Impl;
using AskueCtp.Entities;

namespace AskueCtp.Tests.DataAccess.AcDb
{
    [TestFixture]
    class FirstLevelPayGroupTests : ReadOnlyRepositoryTest
    {
        private object[] m_Cids = { 
                                      new PayGroupCid(1, 1, 1),
                                      new PayGroupCid(3, 2, 1)
                                  };
        private object[] m_NonExistingCids = { 
                                                 new PayGroupCid(-1, -1, -1),
                                                 new PayGroupCid(0, 0, 0)
                                             };

        protected override string TableName { get { return "cnt.gr_integr"; } }

        protected override IReadOnlyRepository BuildRORepository(ISessionBuilder sessionBuilder)
        {
            return new FirstLevelPayGroupRepository(sessionBuilder);
        }

        protected override object[] ExistingCids { get { return m_Cids; } }

        protected override object[] NonExistingCids { get { return m_NonExistingCids; } }

        protected override void CheckItem(object item)
        {
            FirstLevelPayGroup group = item as FirstLevelPayGroup;

            Assert.AreNotEqual(null, group, "Item should be of type FirstLevelPayGroup");
            Assert.AreNotEqual(null, group.ParentObject, "Must be a parent");
            Assert.AreNotEqual(null, group.BalanceElement, "Balance element should not be NULL");
            Assert.AreNotEqual(null, group.Detail1, "Detail1 should not be NULL");
            Assert.AreNotEqual(null, group.Detail2, "Detail2 should not be NULL");
            Assert.AreNotEqual(null, group.TimeZone, "TimeZone should not be NULL");

            foreach (PayGroupMeasurement measure in group.PayGroupMeasurements)
            {
                Assert.AreNotEqual(null, measure.ParentGroup, "Must be parent group");
                Assert.AreNotEqual(null, measure.Feeder, "Feeder should not be NULL");
                Assert.AreNotEqual(null, measure.ReadingType, "ReadingType should not be NULL");
            }
        }
    }
}
