﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;

using AskueCtp.Common;
using AskueCtp.DataAccess;
using AskueCtp.DataAccess.Impl;
using AskueCtp.Entities;

namespace AskueCtp.Tests.DataAccess.AcDb
{
    [TestFixture]
    class ObjectTests : ReadOnlyRepositoryTest
    {
        private object[] m_Cids = { 
                                      new ObjectCid(0, 0),
                                      new ObjectCid(11,10),
                                      new ObjectCid(7, 10010),
                                      new ObjectCid(150, 10),
                                      new ObjectCid(151, 10010),
                                      new ObjectCid(1, 1),
                                      new ObjectCid(3, 2)
                                  };
        private object[] m_NonExistingCids = { 
                                                 new ObjectCid(-1, -1),
                                                 new ObjectCid(32000, 32000),
                                                 new ObjectCid(123, 123)
                                             };

        protected override string TableName { get { return "cnt.obekt"; } }

        protected override IReadOnlyRepository BuildRORepository(ISessionBuilder sessionBuilder)
        {
            return new ObjectRepository(sessionBuilder);
        }

        protected override object[] ExistingCids { get { return m_Cids; } }

        protected override object[] NonExistingCids { get { return m_NonExistingCids; } }

        protected override void CheckItem(object item)
        {
            AskueCtp.Entities.Object obj = item as AskueCtp.Entities.Object;

            Assert.AreNotEqual(null, obj, "Item should be of type Object");

            Assert.AreNotEqual(null, obj.SybRnk, "SybRnk should not be NULL");
            Assert.AreNotEqual(null, obj.Feeders, "Feeders collection should not be NULL");
            Assert.AreNotEqual(null, obj.FirstLevelPayGroups, "FirstLevelPayGroups collection should not be NULL");
            Assert.AreNotEqual(null, obj.SecondLevelPayGroups, "SecondLevelPayGroups collection should not be NULL");
            Assert.AreNotEqual(null, obj.Objects, "Children Objects should not be NULL");


            foreach (Feeder f in obj.Feeders)
            { }
            foreach (FirstLevelPayGroup payGroup in obj.FirstLevelPayGroups)
            { }
            foreach (SecondLevelPayGroup payGroup in obj.SecondLevelPayGroups)
            { }
            foreach (AskueCtp.Entities.Object o in obj.Objects)
            { }
            //Console.WriteLine(obj.ToString() + ((obj.ParentObject == null)? "": obj.ParentObject.ToString()));
        }
    }
}