﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;

using AskueCtp.Common;
using AskueCtp.DataAccess;
using AskueCtp.DataAccess.Impl;
using AskueCtp.Entities;

namespace AskueCtp.Tests.DataAccess.AcDb
{
    [TestFixture]
    class FeederMeasurementTests : ReadOnlyRepositoryTest
    {

        private object[] m_Cids = {
                                    new FeederMeasurementCid(3, 2, 21, (ProfileType)1, 
                                        new DateTime(2007, 12, 25), 23, 1, "0", 30),
                                    new FeederMeasurementCid(3, 2, 28, (ProfileType)3, 
                                        new DateTime(2008, 05, 05), 4, 1, "0", 30)
                                  };
        private object[] m_NonExistingCids = {
                                    new FeederMeasurementCid(10, 200, 21, (ProfileType)1, 
                                        new DateTime(2007, 12, 25), 23, 1, "n/a", 3),
                                    new FeederMeasurementCid(101, 201, 28, (ProfileType)3, 
                                        new DateTime(2008, 05, 05), 4, 1, "n/a", 3)
                                             };

        protected override string TableName { get { return "cnt.buf_v_int"; } }

        protected override IReadOnlyRepository BuildRORepository(ISessionBuilder sessionBuilder)
        {
            return new FeederMeasurementRepository(sessionBuilder);
        }

        protected override object[] ExistingCids { get { return m_Cids; } }

        protected override object[] NonExistingCids { get { return m_NonExistingCids; } }

        [Ignore("Too many rows")]
        public override void GetAll()
        {
            base.GetAll();
        }
    }
}
