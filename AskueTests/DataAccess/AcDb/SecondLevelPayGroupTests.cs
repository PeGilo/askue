﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;

using AskueCtp.Common;
using AskueCtp.DataAccess;
using AskueCtp.DataAccess.Impl;
using AskueCtp.Entities;

namespace AskueCtp.Tests.DataAccess.AcDb
{
    [TestFixture]
    class SecondLevelPayGroupTests : ReadOnlyRepositoryTest
    {
        private object[] m_Cids = { 
                                  };
        private object[] m_NonExistingCids = { 
                                                 new PayGroupCid(-1, -1, -1),
                                                 new PayGroupCid(0, 0, 0)
                                             };

        protected override string TableName { get { return "cnt.gr_integr_2"; } }

        protected override IReadOnlyRepository BuildRORepository(ISessionBuilder sessionBuilder)
        {
            return new SecondLevelPayGroupRepository(sessionBuilder);
        }

        protected override object[] ExistingCids { get { return m_Cids; } }

        protected override object[] NonExistingCids { get { return m_NonExistingCids; } }

        /// <summary>
        /// No records in the table
        /// </summary>
        [Test]
        public override void CheckEquity()
        {
        }

        protected override void CheckItem(object item)
        {
            SecondLevelPayGroup group = item as SecondLevelPayGroup;

            Assert.AreNotEqual(null, group, "Item should be of type FirstLevelPayGroup");
            Assert.AreNotEqual(null, group.ParentObject, "Must be a parent");
            Assert.AreNotEqual(null, group.BalanceElement, "Balance element should not be NULL");
            Assert.AreNotEqual(null, group.Detail1, "Detail1 should not be NULL");

            foreach (FirstLevelMeasurement measure in group.Measurements)
            {
                Assert.AreNotEqual(null, measure.ParentGroup, "Must be parent group");
                Assert.AreNotEqual(null, measure.MeasureGroup, "MeasureGroup should not be NULL");
            }
        }
    }
}
