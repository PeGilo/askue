﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;

using NHibernate;
using NHibernate.Cfg;

using AskueCtp.Common;
using AskueCtp.DataAccess;
using AskueCtp.DataAccess.Impl;
using AskueCtp.Entities;

namespace AskueCtp.Tests.DataAccess.RwDb
{
    /// <summary>
    /// Checks operations with PlanValues table.
    /// Required operations:
    /// - getting
    /// - adding
    /// - updating
    /// - setting value to null (clearing)
    /// </summary>
    [TestFixture]
    public class PlanValuesTests : RepositoryTests
    {
        private const string tableName = "sys.app_planvalues";

        private static object[] m_cids = {
                                      new PlanValueCid("energy", new DateTime(2009, 01, 01), 30, 1),
                                      new PlanValueCid("energy", new DateTime(2009, 01, 01), 30, 2),
                                      new PlanValueCid("energy", new DateTime(2009, 01, 01), 30, 3),
                                      new PlanValueCid("power", new DateTime(2009, 01, 01), 30, 1),
                                      new PlanValueCid("power", new DateTime(2009, 01, 01), 30, 2),
                                      new PlanValueCid("power", new DateTime(2009, 01, 01), 30, 3),
                                      new PlanValueCid("energy", new DateTime(2009, 01, 01), 15, 1),
                                      new PlanValueCid("energy", new DateTime(2009, 01, 01), 15, 2),
                                      new PlanValueCid("energy", new DateTime(2009, 01, 01), 15, 3)
                                  };

        private object[] m_items = {
                                       new PlanValue((PlanValueCid)m_cids[0], 111),
                                       new PlanValue((PlanValueCid)m_cids[1], 222),
                                       new PlanValue((PlanValueCid)m_cids[2], 333),
                                       new PlanValue((PlanValueCid)m_cids[3], 444),
                                       new PlanValue((PlanValueCid)m_cids[4], 555),
                                       new PlanValue((PlanValueCid)m_cids[5], 666),
                                       new PlanValue((PlanValueCid)m_cids[6], null),
                                       new PlanValue((PlanValueCid)m_cids[7], null),
                                       new PlanValue((PlanValueCid)m_cids[8], null),
                                   };

        protected override string TableName { get { return tableName; } }

        protected override object[] Cids { get { return m_cids; } }

        protected override object[] Items { get { return m_items; } }

        protected override object NonExistCid { get { return new PlanValueCid("na", new DateTime(2000, 01, 01), 30, 1); } }

        protected override IReadWriteRepository BuildRepository(ISessionBuilder sessionBuilder)
        {
            return new PlanValueRepository(sessionBuilder);
        }

        protected override string FormatInsertCommand(object item)
        {
            PlanValue planValue = item as PlanValue;

            return String.Format(
                "insert into " + tableName + " (paramid, dat, interval, n_int, value) "
                + "values ('{0}', '{1}', {2}, {3}, {4})",
                planValue.Cid.ParameterId, planValue.Cid.Date.ToShortDateString(), planValue.Cid.Interval, planValue.Cid.IntervalNumber, 
                (planValue.Value.HasValue ? planValue.Value.Value.ToString() : "NULL"));
        }       
    }
}
