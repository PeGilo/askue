﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Soap;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

using NUnit.Framework;

using NHibernate;
using NHibernate.Cfg;

using AskueCtp.Common;
using AskueCtp.DataAccess;
using AskueCtp.DataAccess.Impl;
using AskueCtp.Entities;

namespace AskueCtp.Tests.DataAccess.RwDb
{
    /// <summary>
    /// Checks operations with Preference table.
    /// Required operations:
    /// getting,
    /// adding,
    /// updating,
    /// adding/getting with UserId = NULL
    /// </summary>
    [TestFixture]
    public class PreferenceTests : RepositoryTests
    {
        [Serializable]
        protected class LargePreference
        {
            private int m_integer;
            private string m_string;
            //public int[] li = new int[] { 1, 2, 3 };

            public int Integer
            {
                get { return m_integer; }
            }

            public string String
            {
                get { return m_string; }
            }

            internal LargePreference(int i, string s)
            {
                m_integer = i;
                m_string = s;
            }

            public override bool Equals(object obj)
            {
                //if (!base.Equals(obj)) return false;

                if (obj == null) return false;

                if (this.GetType() != obj.GetType()) return false;

                LargePreference other = (LargePreference)obj;

                return (
                    m_integer.Equals(other.m_integer)
                    && m_string.Equals(other.m_string)
                    );
            }

            public override int GetHashCode()
            {
                return m_integer;
            }

            public override string ToString()
            {
                return "{i=" + m_integer.ToString() + " s='" + m_string + "'}";
            }

            public static bool operator==(LargePreference lhs, LargePreference rhs)
            {
                return lhs.Equals(rhs);
            }

            public static bool operator!=(LargePreference lhs, LargePreference rhs)
            {
                return !lhs.Equals(rhs);
            }
        }

        private const string tableName = "sys.app_prefs";

        private static object[] m_cids = {
                                      new PreferenceCid("pref1", "user1", 1),
                                      new PreferenceCid("pref1", "user1", 2),

                                      new PreferenceCid("pref2", "user1", 1),
                                      new PreferenceCid("pref2", "user1", 2),

                                      new PreferenceCid("pref3", "user1", 1),

                                      new PreferenceCid("pref1", "user2", 1),
                                      new PreferenceCid("pref1", "user2", 2),

                                      new PreferenceCid("pref2", "user2", 1),
                                      new PreferenceCid("pref2", "user2", 2),

                                      new PreferenceCid("pref3", "user2", 1),
                                      //new PreferenceCid("pref1", null),
                                      //new PreferenceCid("pref2", null),
                                      //new PreferenceCid("pref3", null),
                                      new PreferenceCid("pref1", "__COMMON_SETTING__", 1),
                                      new PreferenceCid("pref2", "__COMMON_SETTING__", 1),
                                      new PreferenceCid("pref3", "__COMMON_SETTING__", 1)
                                  };

        private object[] m_prefs = {
                                       new Preference((PreferenceCid)m_cids[0], "111"),
                                       new Preference((PreferenceCid)m_cids[1], "111"),
                                       new Preference((PreferenceCid)m_cids[2], "222"),
                                       new Preference((PreferenceCid)m_cids[3], "222"),
                                       new Preference((PreferenceCid)m_cids[4], "333"),
                                       new Preference((PreferenceCid)m_cids[5], "444"),
                                       new Preference((PreferenceCid)m_cids[6], "444"),
                                       new Preference((PreferenceCid)m_cids[7], "555"),
                                       new Preference((PreferenceCid)m_cids[8], "555"),
                                       new Preference((PreferenceCid)m_cids[9], "666"),
                                       new Preference((PreferenceCid)m_cids[10], "777"),
                                       new Preference((PreferenceCid)m_cids[11], "888"),
                                       new Preference((PreferenceCid)m_cids[12], "999")
                                   };

        protected override string TableName { get { return tableName; } }

        protected override object[] Cids { get { return m_cids; } }

        protected override object[] Items { get { return m_prefs; } }

        protected override object NonExistCid { get { return new PreferenceCid("na", "na", 0); } }

        protected override IReadWriteRepository BuildRepository(ISessionBuilder sessionBuilder)
        {
            return new PreferenceRepository(sessionBuilder);
        }

        protected override string FormatInsertCommand(object item)
        {
            Preference pref = item as Preference;

            return String.Format(
                "insert into " + tableName + " (prefid, userid, partno, prefvalue) "
                + "values ('{0}', '{1}', '{2}', '{3}')",
                pref.Cid.PreferenceId, pref.Cid.UserId, pref.Cid.PartNo, pref.Value);
        }

        [Test]
        public virtual void Complex_ClearTable()
        {
            ISessionBuilder sessionBuilderRW = GetRWSessionBuiler();
            ClearTable(TableName, sessionBuilderRW);
            IPreferenceRepository rep = new PreferenceRepository(sessionBuilderRW);

            for (int i = 0; i < Items.Length; i++)
            {
                rep.Save(Items[i]);
            }

            Preference[] prefs = rep.GetAll();
            foreach (AskueCtp.Entities.Preference pref in prefs)
            {
                rep.Delete(rep.GetById(pref.Cid));
            }

            // Check amount of items
            Preference[] allObjects = rep.GetAll();
            Assert.AreEqual(0, allObjects.Length, "[RepositoryTests] After removing count of all objects are incorrect");
        }

        [Test]
        public virtual void Complex_GetAllPartsSorted()
        {
            ISessionBuilder sessionBuilderRW = GetRWSessionBuiler();
            ClearTable(TableName, sessionBuilderRW);
            IPreferenceRepository rep = new PreferenceRepository(sessionBuilderRW);

            rep.Save(new Preference(new PreferenceCid("pref1", "user1", 2), "2"));
            rep.Save(new Preference(new PreferenceCid("pref1", "user1", 4), "4"));
            rep.Save(new Preference(new PreferenceCid("pref1", "user1", 3), "3"));
            rep.Save(new Preference(new PreferenceCid("pref1", "user1", 1), "1"));

            IList<Preference> prefs = rep.GetAllPartsSorted("pref1", "user1");

            Assert.NotNull(prefs, "Can not read preferences");
            Assert.AreEqual(4, prefs.Count, "Can not read preferences");

            for (int i = 0; i < prefs.Count; i++)
            {
                Assert.AreEqual(i + 1, prefs[i].Cid.PartNo, "Parts are not sorted");
            }
        }

        [Test]
        public virtual void SerializedPreferences()
        {
            ISessionBuilder sessionBuilderRW = GetRWSessionBuiler();
            ClearTable(TableName, sessionBuilderRW);
            IPreferenceRepository rep = new PreferenceRepository(sessionBuilderRW);

            LargePreference lp1 = new LargePreference(10, "Large Preference");
            LargePreference lp2 = new LargePreference(20, "Русский");

            string serializedPref1 = Serialize(lp1);
            string serializedPref2 = Serialize(lp2);

            PreferenceCid cid1 = new PreferenceCid("sp_pref1", "sp_user1", 1);
            Preference p1 = new Preference(cid1, serializedPref1);

            PreferenceCid cid2 = new PreferenceCid("sp_pref2", "sp_user2", 1);
            Preference p2 = new Preference(cid2, serializedPref2);

            rep.Save(p1);
            rep.Save(p2);

            p1 = rep.GetById(cid1);
            p2 = rep.GetById(cid2);

            Assert.NotNull(p1, "Can not read preference 1");
            Assert.NotNull(p2, "Can not read preference 2");

            LargePreference lp1_Copy = Deserialize(p1.Value);
            LargePreference lp2_Copy = Deserialize(p2.Value);

            Assert.NotNull(lp1_Copy, "Can not deserialize preference 1");
            Assert.NotNull(lp2_Copy, "Can not deserialize preference 2");

            Assert.AreEqual(lp1.Equals(lp1_Copy), true, "Prefs are not equal 1");
            Assert.AreEqual(lp2, lp2_Copy, "Prefs are not equal 2");
        }

        protected virtual string Serialize(LargePreference lp)
        {
            Stream stream = new MemoryStream();
            SoapFormatter formatter = new SoapFormatter();
            //BinaryFormatter formatter = new BinaryFormatter();

            formatter.Serialize(stream, lp);

            // writing to string
            stream.Seek(0, SeekOrigin.Begin);

            byte[] byteArray = new byte[stream.Length];
            int readByte = 0;
            int count = 0;
            for (; count < stream.Length; count++ )
            {
                readByte = stream.ReadByte();
                if (readByte == -1)
                    break;

                byteArray[count] = Convert.ToByte(readByte);
            }

            stream.Close();

            return Encoding.UTF8.GetString(byteArray, 0, (int)count);
        }

        protected virtual LargePreference Deserialize(string s)
        {
            byte[] byteArray = Encoding.UTF8.GetBytes(s);
            Stream stream = new MemoryStream(byteArray);
            SoapFormatter formatter = new SoapFormatter();
            //BinaryFormatter formatter = new BinaryFormatter();

            LargePreference lp = formatter.Deserialize(stream) as LargePreference;
            stream.Close();

            return lp;
        }
    }
}
