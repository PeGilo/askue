﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;

using AskueCtp.Common;
using AskueCtp.DataAccess;
using AskueCtp.DataAccess.Impl;
using AskueCtp.Entities;

namespace AskueCtp.Tests.DataAccess.RwDb
{
    [TestFixture]
    public class DeviationTests : RepositoryTests
    {
        private const string tableName = "sys.app_deviations";

        private static object[] m_cids = {
                                      "energy",
                                      "power",
                                      "kvar",
                                      "kva",
                                      "currency"
                                  };

        private object[] m_items = {
                                       new Deviation((String)m_cids[0], 10, 20, Unit.Absolute),
                                       new Deviation((String)m_cids[1], 10, 20, Unit.Percent),
                                       new Deviation((String)m_cids[2], 10, null, Unit.Absolute),
                                       new Deviation((String)m_cids[3], null, 20, Unit.Percent),
                                       new Deviation((String)m_cids[4], null, null, Unit.Absolute)
                                   };

        protected override string TableName { get { return tableName; } }

        protected override object[] Cids { get { return m_cids; } }

        protected override object[] Items { get { return m_items; } }

        protected override object NonExistCid { get { return "n/a"; } }

        protected override IReadWriteRepository BuildRepository(ISessionBuilder sessionBuilder)
        {
            return new DeviationRepository(sessionBuilder);
        }

        protected override string FormatInsertCommand(object item)
        {
            Deviation dev = item as Deviation;

            return String.Format(
                "insert into " + tableName + " (paramid, warningdiff, limitdiff, unit) "
                + "values ('{0}', {1}, {2}, {3})",
                dev.ParameterId,
                (dev.WarningDiff.HasValue ? dev.WarningDiff.Value.ToString() : "NULL"),
                (dev.LimitDiff.HasValue ? dev.LimitDiff.Value.ToString() : "NULL"),
                (Int32)dev.Unit
                );
        }
    }
}
