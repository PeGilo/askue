﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;

using NHibernate;
using NHibernate.Cfg;

using AskueCtp.DataAccess;

namespace AskueCtp.Tests
{
    abstract class ReadWriteRepositoryTest : ReadOnlyRepositoryTest
    {
        protected abstract string FormatInsertCommand(object item);

        protected abstract object[] Cids { get; }
        protected abstract object[] Items { get; }

        protected abstract object NonExistCid { get; }

        protected abstract IReadWriteRepository BuildRWRepository(ISessionBuilder sessionBuilder); 

        protected override ISessionBuilder GetSessionBuiler()
        {
            return SessionBuilderFactory.GetSessionBuilder(DBSessionType.RWDataBase);
        }

        [SetUp]
        public override void Init()
        {
            base.Init();
        }

        [TearDown]
        public override void Cleanup()
        {
            base.Cleanup();

            // Disposing RW session
            ISession existingWebSession = SessionBuilderFactory.GetSessionBuilder(DBSessionType.RWDataBase).GetExistingWebSession();
            if (existingWebSession != null)
            {
                Console.WriteLine("Disposing of RW ISession " + existingWebSession.GetHashCode());
                existingWebSession.Dispose();
            }
        }

        [Test]
        public override void GetAll()
        {
            base.GetAll();
        }

        protected static void ClearTable(string tableName, ISessionBuilder sessionBuilder)
        {
            ISession session = sessionBuilder.GetSession();

            ISQLQuery ins = session.CreateSQLQuery("delete " + tableName);
            ins.ExecuteUpdate();
        }

    }
}
