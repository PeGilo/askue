﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;

using NHibernate;
using NHibernate.Cfg;

using AskueCtp.DataAccess;

namespace AskueCtp.Tests
{
    public abstract class RepositoryTests : AskueCtp.Tests.Base.TestBase
    {
        [SetUp]
        public override void Init()
        {
            base.Init();
        }

        [TearDown]
        public override void Cleanup()
        {
            //ISession existingWebSession = SessionBuilderFactory.GetSessionBuilder(DBSessionType.RODataStore).GetExistingWebSession();
            //if (existingWebSession != null)
            //{
            //    Console.WriteLine("Disposing of AC ISession " + existingWebSession.GetHashCode());
            //    existingWebSession.Dispose();
            //}

            //// Disposing RW session
            //existingWebSession = SessionBuilderFactory.GetSessionBuilder(DBSessionType.RWDataBase).GetExistingWebSession();
            //if (existingWebSession != null)
            //{
            //    Console.WriteLine("Disposing of RW ISession " + existingWebSession.GetHashCode());
            //    existingWebSession.Dispose();
            //}
            base.Cleanup();
        }

        #region "IRepository testing methods"

        protected abstract string FormatInsertCommand(object item);

        protected abstract string TableName { get; }

        protected abstract object[] Cids { get; }

        protected abstract object[] Items { get; }

        protected abstract object NonExistCid { get; }

        protected abstract IReadWriteRepository BuildRepository(ISessionBuilder sessionBuilder); 

        /// <summary>
        /// Inserts one record to the DB using native SQL query, not repository's methods.
        /// </summary>
        /// <param name="rowAttr"></param>
        /// <param name="sessionBuilder"></param>
        protected virtual void InsertRecordToDB(object item, ISessionBuilder sessionBuilder)
        {
            ISession session = sessionBuilder.GetSession();

            string query = FormatInsertCommand(item);

            ISQLQuery ins = session.CreateSQLQuery(query);
            ins.ExecuteUpdate();
        }

        [Test]
        public virtual void GetAll()
        {
            ISessionBuilder sessionBuilderRW = GetRWSessionBuiler();
            ClearTable(TableName, sessionBuilderRW);

            foreach (object row in Items)
            {
                InsertRecordToDB(row, sessionBuilderRW);
            }

            IReadWriteRepository rep = BuildRepository(sessionBuilderRW);

            Object[] allObjects = rep.GetAll();
            Assert.AreEqual(Items.Length, allObjects.Length, "[RepositoryTests] Count of all objects are incorrect");
        }

        [Test]
        public virtual void GetById()
        {
            ISessionBuilder sessionBuilderRW = GetRWSessionBuiler();
            ClearTable(TableName, sessionBuilderRW);

            IReadWriteRepository rep = BuildRepository(sessionBuilderRW);

            for (int i = 0; i < Items.Length; i++)
            {
                InsertRecordToDB(Items[i], sessionBuilderRW);

                // Getting concrete
                Object dbItem = rep.GetById(Cids[i]);

                Assert.AreNotEqual(null, dbItem, "Could not get an item by cid");
                Assert.AreEqual(Items[i], dbItem, "Objects are not equal. Expected: {0}; Actual: {1}", Items[i].ToString(), dbItem.ToString());
            }
        }

        [Test]
        public virtual void GetNonExistItem()
        {
            ISessionBuilder sessionBuilderRW = GetRWSessionBuiler();
            IReadWriteRepository rep = BuildRepository(sessionBuilderRW);

            Object item = rep.GetById(NonExistCid);
            Assert.AreEqual(null, item, "Has gotten nonexistent preference");
        }

        [Test]
        public virtual void SaveOnAdding()
        {
            ISessionBuilder sessionBuilderRW = GetRWSessionBuiler();
            ClearTable(TableName, sessionBuilderRW);

            IReadWriteRepository rep = BuildRepository(sessionBuilderRW);

            for (int i = 0; i < Items.Length; i++)
            {
                rep.Save(Items[i]);
            }

            for (int i = 0; i < Items.Length; i++)
            {
                Object dbItem = rep.GetById(Cids[i]);

                Assert.AreNotEqual(null, dbItem, "Could not get an item by cid");
                Assert.AreEqual(Items[i], dbItem, "Objects are not equal. Expected: {0}; Actual: {1}", Items[i].ToString(), dbItem.ToString());
            }

            // Check amount of items
            Object[] allObjects = rep.GetAll();
            Assert.AreEqual(Items.Length, allObjects.Length, "[RepositoryTests] Count of all objects are incorrect");
        }

        [Test]
        public virtual void SaveOnUpdating()
        {
            ISessionBuilder sessionBuilderRW = GetRWSessionBuiler();
            ClearTable(TableName, sessionBuilderRW);

            IReadWriteRepository rep = BuildRepository(sessionBuilderRW);

            for (int i = 0; i < Items.Length; i++)
            {
                rep.Save(Items[i]);
                rep.Save(Items[i]);
                rep.Save(Items[i]);
                rep.Save(Items[i]);
            }

            // Check amount of items
            Object[] allObjects = rep.GetAll();
            Assert.AreEqual(Items.Length, allObjects.Length, "[RepositoryTests] Count of all objects are incorrect");
        }

        [Test]
        public virtual void Delete()
        {
            ISessionBuilder sessionBuilderRW = GetRWSessionBuiler();
            IReadWriteRepository rep = BuildRepository(sessionBuilderRW);

            // Add items
            SaveOnAdding();

            // Delete items
            for (int i = 0; i < Items.Length; i++)
            {
                rep.Delete(rep.GetById(Cids[i]));
            }

            // Check amount of items
            Object[] allObjects = rep.GetAll();
            Assert.AreEqual(0, allObjects.Length, "[RepositoryTests] After removing count of all objects are incorrect");
        }

        [Test]
        public virtual void Complex_AddDelete()
        {
            ISessionBuilder sessionBuilderRW = GetRWSessionBuiler();
            ClearTable(TableName, sessionBuilderRW);
            
            IReadWriteRepository rep = BuildRepository(sessionBuilderRW);
            for (int i = 0; i < Items.Length; i++)
            {
                rep.Save(Items[i]);
            }

            for (int i = 0; i < Items.Length; i++)
            {
                rep.Delete(rep.GetById(Cids[i]));
            }

            // Check amount of items
            Object[] allObjects = rep.GetAll();
            Assert.AreEqual(0, allObjects.Length, "[RepositoryTests] After removing count of all objects are incorrect");
        }



        #endregion
    }
}
