﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;

using NHibernate;
using NHibernate.Cfg;

using AskueCtp.DataAccess;

namespace AskueCtp.Tests
{
    abstract class ReadOnlyRepositoryTest
    {
        protected abstract string TableName { get; }

        protected abstract IReadOnlyRepository BuildRORepository(ISessionBuilder sessionBuilder);

        /// <summary>
        /// Test: Reading by this cids and checking that such items exist
        /// </summary>
        protected abstract object[] ExistingCids { get; }
        // Can not create readonly objects. No such constructor.
        //protected abstract object[] ExistingItems { get; }

        /// <summary>
        /// Test: Reading by this cids and checking that such items don't exist
        /// </summary>
        protected abstract object[] NonExistingCids { get; }


        [SetUp]
        public virtual void Init()
        {

        }

        [TearDown]
        public virtual void Cleanup()
        {
            ISession existingWebSession = SessionBuilderFactory.GetSessionBuilder(DBSessionType.RODataStore).GetExistingWebSession();
            if (existingWebSession != null)
            {
                Console.WriteLine("Disposing of AC ISession " + existingWebSession.GetHashCode());
                existingWebSession.Dispose();
            }
        }

        [Test]
        public virtual void GetAll()
        {
            ISessionBuilder sessionBuilder = GetSessionBuiler();
            IReadOnlyRepository rep = BuildRORepository(sessionBuilder);

            int rowsCount = GetRowsCount(sessionBuilder);
            object[] readItems = rep.GetAll();

            Assert.AreEqual(rowsCount, readItems.Length, "Count of all objects are not correct");
        }

        [Test]
        public virtual void GetById()
        {
            ISessionBuilder sessionBuilder = GetSessionBuiler();
            IReadOnlyRepository rep = BuildRORepository(sessionBuilder);

            for (int i = 0; i < ExistingCids.Length; i++)
            {
                Object dbItem = rep.GetById(ExistingCids[i]);

                Assert.AreNotEqual(null, dbItem, "Could not get an item by cid");
                CheckItem(dbItem);
            }
        }

        [Test]
        public virtual void GetNonExistingItemsById()
        {
            ISessionBuilder sessionBuilder = GetSessionBuiler();
            IReadOnlyRepository rep = BuildRORepository(sessionBuilder);

            for (int i = 0; i < NonExistingCids.Length; i++)
            {
                Object dbItem = rep.GetById(NonExistingCids[i]);

                Assert.AreEqual(null, dbItem, "Got an nonexisting item O_o");
            }
        }

        /// <summary>
        /// Should be two or more existing cids
        /// </summary>
        [Test]
        public virtual void CheckEquity()
        {
            ISessionBuilder sessionBuilder = GetSessionBuiler();
            IReadOnlyRepository rep = BuildRORepository(sessionBuilder);

            Object item1 = rep.GetById(ExistingCids[0]);
            Object item2 = rep.GetById(ExistingCids[0]);
            Object item3 = rep.GetById(ExistingCids[1]);

            Assert.AreEqual(item1, item2, "Items should be equal");
            Assert.AreNotEqual(item1, item3, "Items should be not equal, cause different cids");
        }


        /// <summary>
        /// Inherited test classes may override this method to check each item that has been read.
        /// </summary>
        /// <param name="item"></param>
        protected virtual void CheckItem(Object item)
        {
        }

        /// <summary>
        /// Gets count of rows in the table.
        /// </summary>
        /// <param name="sessionBuilder"></param>
        /// <returns></returns>
        protected virtual int GetRowsCount(ISessionBuilder sessionBuilder)
        {
            ISession session = sessionBuilder.GetSession();

            string query = "select count(*) from " + TableName;

            ISQLQuery sel = session.CreateSQLQuery(query);
            System.Collections.IList res = sel.List();
            return Int32.Parse(res[0].ToString());
        }

        protected virtual ISessionBuilder GetSessionBuiler()
        {
            return SessionBuilderFactory.GetSessionBuilder(DBSessionType.RODataStore);
        }
    }
}
