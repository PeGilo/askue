﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;

using NHibernate;
using NHibernate.Cfg;

using AskueCtp.Common;
using AskueCtp.DataAccess;
using AskueCtp.DataAccess.Impl;
using AskueCtp.Entities;
using AskueCtp.ServiceLayer;
using AskueCtp.ServiceLayer.Impl;
using AskueCtp.Web;

namespace AskueCtp.Tests.Web.Settings
{
    [TestFixture]
    public class UserSettingsTest : AskueCtp.Tests.Base.TestBase
    {
        private const string m_TableName = "sys.app_prefs";

        public class TestUser : IUser
        {
            private string m_name;
            private Role m_role;

            public string Name
            {
                get { return m_name; }
            }

            public Role Role
            {
                get { return m_role; }
            }

            public TestUser(string name, Role role)
            {
                m_name = name;
                m_role = role;
            }
        }
        //protected static ISessionBuilder GetACSessionBuiler()
        //{
        //    return SessionBuilderFactory.GetSessionBuilder(DBSessionType.RODataStore);
        //}

        //protected static ISessionBuilder GetRWSessionBuiler()
        //{
        //    return SessionBuilderFactory.GetSessionBuilder(DBSessionType.RWDataBase);
        //}

        //[SetUp]
        //public virtual void Init()
        //{

        //}

        //[TearDown]
        //public virtual void Cleanup()
        //{
        //    ISession existingWebSession = SessionBuilderFactory.GetSessionBuilder(DBSessionType.RODataStore).GetExistingWebSession();
        //    if (existingWebSession != null)
        //    {
        //        Console.WriteLine("Disposing of AC ISession " + existingWebSession.GetHashCode());
        //        existingWebSession.Dispose();
        //    }

        //    // Disposing RW session
        //    existingWebSession = SessionBuilderFactory.GetSessionBuilder(DBSessionType.RWDataBase).GetExistingWebSession();
        //    if (existingWebSession != null)
        //    {
        //        Console.WriteLine("Disposing of RW ISession " + existingWebSession.GetHashCode());
        //        existingWebSession.Dispose();
        //    }
        //}

        [Test]
        public virtual void Common()
        {
            ISessionBuilder sessionBuilderRW = GetRWSessionBuiler();
            ClearTable(m_TableName, sessionBuilderRW);

            // 1. Get empty settings
            IUser user = new TestUser("user1", Role.Admin);
            UserSettings us = new UserSettings(user, new PreferenceService(new PreferenceRepository(sessionBuilderRW)));

            WindowSet ws = us.WindowSet;

            Assert.AreNotEqual(null, ws, "Window set must not be null");
            Assert.AreNotEqual(null, ws.WindowTabs, "WindowTabs must not be null");
            Assert.AreEqual(0, ws.WindowTabs.Count, "Why do WindowTabs have items");

            // 2. Creating Window Set and Saving
            Window w1 = new Window("w1");
            Window w2 = new Window("w2");
            Window w3 = new Window("w3");
            Window w4 = new Window("w4");

            WindowTab wt1 = new WindowTab();
            wt1.Windows.Add(w1);
            wt1.Windows.Add(w2);

            WindowTab wt2 = new WindowTab();
            wt2.Windows.Add(w3);
            wt2.Windows.Add(w4);
            wt2.TabAlignment = TabAlignment.Vertical;

            ws.WindowTabs.Add(wt1);
            ws.WindowTabs.Add(wt2);

            us.Save();

            // 3. Checking what do we have now, after saving
            us = new UserSettings(user, new PreferenceService(new PreferenceRepository(sessionBuilderRW)));
            ws = us.WindowSet;
            Assert.AreNotEqual(null, ws, "[After save] Window set must not be null");
            Assert.AreNotEqual(null, ws.WindowTabs, "[After save] WindowTabs must not be null");
            Assert.AreEqual(2, ws.WindowTabs.Count, "[After save] WindowTabs must have items");
            
        }
    }
}
