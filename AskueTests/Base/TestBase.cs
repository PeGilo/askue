﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NHibernate;
using NUnit.Framework;

using AskueCtp.DataAccess;

namespace AskueCtp.Tests.Base
{
    public abstract class TestBase
    {
        protected static ISessionBuilder GetACSessionBuiler()
        {
            return SessionBuilderFactory.GetSessionBuilder(DBSessionType.RODataStore);
        }

        protected static ISessionBuilder GetRWSessionBuiler()
        {
            return SessionBuilderFactory.GetSessionBuilder(DBSessionType.RWDataBase);
        }

        protected static void ClearTable(string tableName, ISessionBuilder sessionBuilder)
        {
            ISession session = sessionBuilder.GetSession();

            ISQLQuery ins = session.CreateSQLQuery("delete " + tableName);
            ins.ExecuteUpdate();
        }

        [SetUp]
        public virtual void Init()
        {

        }

        [TearDown]
        public virtual void Cleanup()
        {
            ISession existingWebSession = SessionBuilderFactory.GetSessionBuilder(DBSessionType.RODataStore).GetExistingWebSession();
            if (existingWebSession != null)
            {
                Console.WriteLine("Disposing of AC ISession " + existingWebSession.GetHashCode());
                existingWebSession.Dispose();
            }

            // Disposing RW session
            existingWebSession = SessionBuilderFactory.GetSessionBuilder(DBSessionType.RWDataBase).GetExistingWebSession();
            if (existingWebSession != null)
            {
                Console.WriteLine("Disposing of RW ISession " + existingWebSession.GetHashCode());
                existingWebSession.Dispose();
            }
        }
    }
}
