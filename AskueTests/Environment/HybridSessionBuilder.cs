﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NHibernate;
using NHibernate.Cfg;

using AskueCtp.Common;
using AskueCtp.DataAccess;

namespace AskueCtp.Tests
{
    /// <summary>
    /// Using: for tests
    /// new HybridSessionBuilder().GetSession(); // creates new or returns existing session
    /// new HybridSessionBuilder().GetExistingWebSession(); // returns existing session
    /// </summary>
    public class HybridSessionBuilder : ISessionBuilder
    {
        private static ISessionFactory _sessionFactory = null;
        private static ISession _currentSession = null;

        public Configuration GetConfiguration()
        {
            var configuration = new Configuration();
            Dictionary<string, string> confDict = new Dictionary<string,string>();
            confDict.Add("dialect","NHibernate.Dialect.Oracle9Dialect");
            confDict.Add("connection.provider","NHibernate.Connection.DriverConnectionProvider");
            confDict.Add("connection.connection_string","server=TESTAC_CNT;uid=cnt;pwd=cnt");
            confDict.Add("show_sql","true");
            configuration.AddProperties(confDict);
            configuration.AddAssembly("AskueCtpEntities");
            //configuration.Configure(;
            return configuration;
        }

        /// <summary>
        /// Does not create new session
        /// </summary>
        /// <returns></returns>
        public ISession GetExistingWebSession()
        {
            //return HttpContext.Current.Items[GetType().FullName] as ISession;
            return _currentSession;
        }

        /// <summary>
        /// Returns existing or creates new session
        /// </summary>
        /// <returns></returns>
        public ISession GetSession()
        {
            ISessionFactory factory = getSessionFactory();
            ISession session = getExistingOrNewSession(factory);
            //Logger.Info("Using ISession " + session.GetHashCode());
            return session;
        }

        private ISession getExistingOrNewSession(ISessionFactory factory)
        {
            //if (HttpContext.Current != null)
            //{
                ISession session = GetExistingWebSession();
                if (session == null)
                {
                    session = openSessionAndAddToContext(factory);
                    //Logger.Info("Creating new Session (HttpContext+)" + session.GetHashCode());
                }
                else if (!session.IsOpen)
                {
                    session = openSessionAndAddToContext(factory);
                }
                return session;
            //}
            //if (_currentSession == null)
            //{
            //    _currentSession = factory.OpenSession();
            //    //Logger.Info("Creating new Session (HttpContext-)" + _currentSession.GetHashCode());
            //}
            //else if (!_currentSession.IsOpen)
            //{
            //    _currentSession = factory.OpenSession();
            //}
            //return _currentSession;
        }

        private ISessionFactory getSessionFactory()
        {
            if (_sessionFactory == null)
            {
                Configuration configuration = GetConfiguration();
                _sessionFactory = configuration.BuildSessionFactory();

                //Logger.Info("Creating new SessionFactory " + _sessionFactory.GetHashCode());
            }
            return _sessionFactory;
        }

        private ISession openSessionAndAddToContext(ISessionFactory factory)
        {
            ISession session = factory.OpenSession();
            //HttpContext.Current.Items.Remove(GetType().FullName);
            //HttpContext.Current.Items.Add(GetType().FullName, session);
            _currentSession = session;
            return session;
        }
    }

}
