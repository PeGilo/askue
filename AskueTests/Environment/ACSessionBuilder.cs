﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Xml.Linq;

using NHibernate;
using NHibernate.Cfg;

using AskueCtp.Common;
using AskueCtp.DataAccess;

namespace AskueCtp.Tests
{
    /// <summary>
    /// Summary description for ACSessionBuilder
    /// </summary>
    public class ACSessionBuilder : BaseSessionBuilder
    {
        private static ISessionFactory _sessionFactory;
        private static ISession _currentSession;

        protected override ISessionFactory SessionFactory
        {
            get { return _sessionFactory; }
            set { _sessionFactory = value; }
        }
        protected override ISession CurrentSession
        {
            get { return _currentSession; }
            set { _currentSession = value; }
        }

        public override Configuration GetConfiguration()
        {
            var configuration = new Configuration();
            Dictionary<string, string> confDict = new Dictionary<string, string>();
            confDict.Add("dialect", "NHibernate.Dialect.Oracle9Dialect");
            confDict.Add("connection.provider", "NHibernate.Connection.DriverConnectionProvider");
            confDict.Add("connection.connection_string", "server=TESTAC_CNT;uid=cnt;pwd=cnt");
            confDict.Add("show_sql", "true");
            configuration.AddProperties(confDict);
            configuration.AddAssembly("AskueCtpEntities");
            //configuration.Configure(;
            return configuration;
        }
    }
}