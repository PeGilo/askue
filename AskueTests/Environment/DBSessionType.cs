﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace AskueCtp.Tests
{
    /// <summary>
    /// Summary description for DBSessionType
    /// </summary>
    public enum DBSessionType
    {
        RODataStore,
        RWDataBase
    }
}