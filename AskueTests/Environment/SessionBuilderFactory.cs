﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Xml.Linq;

using AskueCtp.DataAccess;

namespace AskueCtp.Tests
{
    /// <summary>
    /// Summary description for SessionBuilderFactory
    /// </summary>
    public static class SessionBuilderFactory
    {
        public static ISessionBuilder GetSessionBuilder(DBSessionType sessionType)
        {
            if (sessionType == DBSessionType.RODataStore)
                return new ACSessionBuilder();
            else if (sessionType == DBSessionType.RWDataBase)
                return new RWSessionBuilder();

            return null;
        }
    }
}