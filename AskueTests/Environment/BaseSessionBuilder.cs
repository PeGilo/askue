﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

using NHibernate;
using NHibernate.Cfg;

using AskueCtp.Common;
using AskueCtp.DataAccess;

namespace AskueCtp.Tests
{
    /// <summary>
    /// Base class for session builders. Used when session builders should use different config file.
    /// Therefore different session and hibernate session builder.
    /// </summary>
    public abstract class BaseSessionBuilder : ISessionBuilder
    {
        protected abstract ISessionFactory SessionFactory
        {
            get;
            set;
        }
        protected abstract ISession CurrentSession
        {
            get;
            set;
        }


        public abstract Configuration GetConfiguration();

        /// <summary>
        /// Does not create new session
        /// </summary>
        /// <returns></returns>
        public virtual ISession GetExistingWebSession()
        {
            return CurrentSession;
        }

        /// <summary>
        /// Returns existing or creates new session
        /// </summary>
        /// <returns></returns>
        public virtual ISession GetSession()
        {
            ISessionFactory factory = getSessionFactory();
            ISession session = getExistingOrNewSession(factory);
            return session;
        }


        protected virtual ISessionFactory getSessionFactory()
        {
            if (SessionFactory == null)
            {
                NHibernate.Cfg.Configuration configuration = GetConfiguration();
                SessionFactory = configuration.BuildSessionFactory();
            }
            return SessionFactory;
        }

        protected virtual ISession getExistingOrNewSession(ISessionFactory factory)
        {
            ISession session = GetExistingWebSession();
            if (session == null)
            {
                session = openSessionAndAddToContext(factory);
            }
            else if (!session.IsOpen)
            {
                session = openSessionAndAddToContext(factory);
            }
            return session;
        }

        protected virtual ISession openSessionAndAddToContext(ISessionFactory factory)
        {
            ISession session = factory.OpenSession();
            CurrentSession = session;
            return session;
        }
    }
}
