﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;

using NHibernate;
using NHibernate.Cfg;

using AskueCtp.Common;
using AskueCtp.DataAccess;
using AskueCtp.DataAccess.Impl;
using AskueCtp.ServiceLayer;
using AskueCtp.ServiceLayer.Impl;
using AskueCtp.ServiceLayer.Entities;

namespace AskueCtp.Tests.ServiceLayer.RwDb
{
    [TestFixture]
    public class PreferenceServiceTests : AskueCtp.Tests.Base.TestBase
    {

        private static object[] m_cids = {
                                      new PreferenceCid("pref1", "user1"),
                                      new PreferenceCid("pref2", "user1"),
                                      new PreferenceCid("pref3", "user1"),
                                      new PreferenceCid("pref1", "user2"),
                                      new PreferenceCid("pref2", "user2"),
                                      new PreferenceCid("pref3", "user2"),
                                      new PreferenceCid("pref1", "__COMMON_SETTING__"),
                                      new PreferenceCid("pref2", "__COMMON_SETTING__"),
                                      new PreferenceCid("pref3", "__COMMON_SETTING__"),
                                      new PreferenceCid("hugepref", "user")
                                  };

        private object[] m_prefs = {
                                       new Preference((PreferenceCid)m_cids[0], "111"),
                                       new Preference((PreferenceCid)m_cids[1], "111"),
                                       new Preference((PreferenceCid)m_cids[2], "222"),
                                       new Preference((PreferenceCid)m_cids[3], "222"),
                                       new Preference((PreferenceCid)m_cids[4], "333"),
                                       new Preference((PreferenceCid)m_cids[5], "444"),
                                       new Preference((PreferenceCid)m_cids[6], "444"),
                                       new Preference((PreferenceCid)m_cids[7], "555"),
                                       new Preference((PreferenceCid)m_cids[8], "555"),
                                       new Preference((PreferenceCid)m_cids[9], m_hugeString)
                                   };

        [Test]
        public void GetById_SimpleRequests()
        {
            ISessionBuilder sessionBuilderRW = GetRWSessionBuiler();
            IPreferenceRepository rep = new PreferenceRepository(sessionBuilderRW);
            IPreferenceService service = new PreferenceService(rep);

            ClearTable();

            // Prepare data in the database
            rep.Save(new AskueCtp.Entities.Preference(
                new AskueCtp.Entities.PreferenceCid("pref1", "user1", 1), "value1"));
            rep.Save(new AskueCtp.Entities.Preference(
                new AskueCtp.Entities.PreferenceCid("pref2", "user2", 1), "value2"));
            rep.Save(new AskueCtp.Entities.Preference(
                new AskueCtp.Entities.PreferenceCid("pref3", "user3", 1), "value3"));

            Preference pref1 = ((IReadOnlyService<Preference>)service).GetById(
                new PreferenceCid("pref1", "user1"));
            Preference pref2 = ((IReadOnlyService<Preference>)service).GetById(
                new PreferenceCid("pref2", "user2"));
            Preference pref3 = ((IReadOnlyService<Preference>)service).GetById(
                new PreferenceCid("pref3", "user3"));

            Assert.NotNull(pref1, "Can not read preference");
            Assert.NotNull(pref2, "Can not read preference");
            Assert.NotNull(pref3, "Can not read preference");

            Assert.AreEqual("value1", pref1.Value, "Wrong value of preference");
            Assert.AreEqual("value2", pref2.Value, "Wrong value of preference");
            Assert.AreEqual("value3", pref3.Value, "Wrong value of preference");
        }

        [Test]
        public void GetById_ComplexRequests()
        {
            ISessionBuilder sessionBuilderRW = GetRWSessionBuiler();
            IPreferenceRepository rep = new PreferenceRepository(sessionBuilderRW);
            IPreferenceService service = new PreferenceService(rep);

            const string part1 = "part1";
            const string part2 = "part2";
            const string part3 = "part3";

            ClearTable();

            // Prepare data in the database
            rep.Save(new AskueCtp.Entities.Preference(
                new AskueCtp.Entities.PreferenceCid("pref1", "user1", 1), part1));
            rep.Save(new AskueCtp.Entities.Preference(
                new AskueCtp.Entities.PreferenceCid("pref1", "user1", 2), part2));
            rep.Save(new AskueCtp.Entities.Preference(
                new AskueCtp.Entities.PreferenceCid("pref1", "user1", 3), part3));

            Preference pref1 = ((IReadOnlyService<Preference>)service).GetById(
                new PreferenceCid("pref1", "user1"));

            Assert.NotNull(pref1, "Can not read preference");
            Assert.AreEqual(part1+part2+part3, pref1.Value, "Wrong value of preference");
        }

        [Test]
        public void GetNonExisting()
        {
            ISessionBuilder sessionBuilderRW = GetRWSessionBuiler();
            IPreferenceRepository rep = new PreferenceRepository(sessionBuilderRW);
            IPreferenceService service = new PreferenceService(rep);

            Preference pref = ((IReadOnlyService<Preference>)service).GetById(new PreferenceCid("na", "na"));
            Assert.Null(pref, "Has written non existing item");
        }

        [Test]
        public void GetAll()
        {
            ISessionBuilder sessionBuilderRW = GetRWSessionBuiler();
            IPreferenceRepository rep = new PreferenceRepository(sessionBuilderRW);
            IPreferenceService service = new PreferenceService(rep);

            const string part1 = "part1";
            const string part2 = "part2";
            const string part3 = "part3";

            ClearTable();

            // Prepare data in the database
            rep.Save(new AskueCtp.Entities.Preference(
                new AskueCtp.Entities.PreferenceCid("pref1", "user1", 1), "value1"));
            rep.Save(new AskueCtp.Entities.Preference(
                new AskueCtp.Entities.PreferenceCid("pref2", "user2", 1), "value2"));
            rep.Save(new AskueCtp.Entities.Preference(
                new AskueCtp.Entities.PreferenceCid("pref3", "user3", 1), "value3"));
            rep.Save(new AskueCtp.Entities.Preference(
                new AskueCtp.Entities.PreferenceCid("hugepref", "user1", 1), part1));
            rep.Save(new AskueCtp.Entities.Preference(
                new AskueCtp.Entities.PreferenceCid("hugepref", "user1", 2), part2));
            rep.Save(new AskueCtp.Entities.Preference(
                new AskueCtp.Entities.PreferenceCid("hugepref", "user1", 3), part3));

            Preference[] prefs = ((IReadOnlyService<Preference>)service).GetAll();

            // Check count
            Assert.NotNull(prefs, "Can not read preferences");
            Assert.AreEqual(4, prefs.Length, "Count of preferences do not equal");

            // Check values
            Assert.NotNull(FindPreferenceByValue(prefs, "value1"), "Some preference is lost");
            Assert.NotNull(FindPreferenceByValue(prefs, "value2"), "Some preference is lost");
            Assert.NotNull(FindPreferenceByValue(prefs, "value3"), "Some preference is lost");
            Assert.NotNull(FindPreferenceByValue(prefs, part1+part2+part3), "Some preference is lost");
        }

        [Test]
        public void Save_Simple()
        {
            ISessionBuilder sessionBuilderRW = GetRWSessionBuiler();
            IPreferenceRepository rep = new PreferenceRepository(sessionBuilderRW);
            IPreferenceService service = new PreferenceService(rep);

            ClearTable();

            service.Save(new Preference(new PreferenceCid("pref1", "user1"), "value11"));
            service.Save(new Preference(new PreferenceCid("pref2", "user1"), "value21"));
            service.Save(new Preference(new PreferenceCid("pref1", "user2"), "value12"));
            service.Save(new Preference(new PreferenceCid("pref2", "user2"), "value22"));

            // Check
            AskueCtp.Entities.Preference[] prefs = rep.GetAll();

            // ... Check count
            Assert.NotNull(prefs, "Can not read preferences");
            Assert.AreEqual(4, prefs.Length, "Count of preferences do not equal");

            // ... Check content
            AskueCtp.Entities.Preference pref = rep.GetById(
                new AskueCtp.Entities.PreferenceCid("pref1", "user1", 1));
            Assert.NotNull(pref, "Can not read preference");
            pref = rep.GetById(new AskueCtp.Entities.PreferenceCid("pref2", "user1", 1));
            Assert.NotNull(pref, "Can not read preference");
            pref = rep.GetById(new AskueCtp.Entities.PreferenceCid("pref1", "user2", 1));
            Assert.NotNull(pref, "Can not read preference");
            pref = rep.GetById(new AskueCtp.Entities.PreferenceCid("pref2", "user2", 1));
            Assert.NotNull(pref, "Can not read preference");
        }

        [Test]
        public void SaveLongValue()
        {
            ISessionBuilder sessionBuilderRW = GetRWSessionBuiler();
            IPreferenceRepository rep = new PreferenceRepository(sessionBuilderRW);
            IPreferenceService service = new PreferenceService(rep);

            ClearTable();

            // Generate huge preference
            StringBuilder sb = new StringBuilder();
            sb.Append('s', 15000);

            service.Save(new Preference(new PreferenceCid("hugepref", "user"), sb.ToString()));

            // Check records in the database
            AskueCtp.Entities.Preference[] prefs = rep.GetAll();

            // ... Check count
            Assert.NotNull(prefs, "Can not read preferences");
            Assert.Greater(prefs.Length, 1, "Count of preferences in the database is 1 or 0");

            // ... Check whole length of part values
            int wholeCount = 0;
            foreach (AskueCtp.Entities.Preference pref in prefs)
            {
                wholeCount += pref.Value.Length;
            }

            Assert.AreEqual(15000, wholeCount, "Whole count of part values is wrong");
        }

        [Test]
        public void SaveZeroLength()
        {
            ISessionBuilder sessionBuilderRW = GetRWSessionBuiler();
            IPreferenceRepository rep = new PreferenceRepository(sessionBuilderRW);
            IPreferenceService service = new PreferenceService(rep);

            ClearTable();

            service.Save(new Preference(new PreferenceCid("hugepref", "user"), ""));

            // Check records in the database
            AskueCtp.Entities.Preference[] prefs = rep.GetAll();

            // ... Check count
            Assert.NotNull(prefs, "Can not read preferences");
            Assert.Greater(prefs.Length, 0, "Count of preferences in the database is 0");
        }

        [Test]
        public void Complex_SaveGet()
        {
            ISessionBuilder sessionBuilderRW = GetRWSessionBuiler();
            IPreferenceRepository rep = new PreferenceRepository(sessionBuilderRW);
            IPreferenceService service = new PreferenceService(rep);

            ClearTable();

            // Generate huge preference
            StringBuilder sb = new StringBuilder();
            sb.Append('s', 15000);

            PreferenceCid cid = new PreferenceCid("hugepref", "user");
            Preference prefWrite = new Preference(cid, sb.ToString());
            service.Save(prefWrite);

            // Check record in the database
            Preference prefRead = ((IReadOnlyService<Preference>)service).GetById(cid);

            Assert.NotNull(prefRead, "Can not read preference");
            Assert.AreEqual(prefWrite, prefRead, "The preference that has been read is not equal");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="prefs"></param>
        /// <param name="value"></param>
        /// <returns>null if not found</returns>
        protected Preference FindPreferenceByValue(IEnumerable<Preference> prefs, string value)
        {
            foreach (Preference pref in prefs)
            {
                if (pref.Value == value)
                    return pref;
            }
            return null;
        }

        protected void ClearTable()
        {
            ISessionBuilder sessionBuilderRW = GetRWSessionBuiler();
            IPreferenceRepository rep = new PreferenceRepository(sessionBuilderRW);
            AskueCtp.Entities.Preference[] prefs = rep.GetAll();

            foreach(AskueCtp.Entities.Preference pref in prefs)
            {
                rep.Delete(rep.GetById(pref.Cid));
            }
        }

        private const string m_hugeString = @"
NHibernate: delete sys.app_prefs
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref1', :p1 = 'user1', :p2 = '1'
NHibernate: INSERT INTO sys.APP_PREFS (prefValue, prefId, userId, partNo) VALUES (:p0, :p1, :p2, :p3); :p0 = '111', :p1 = 'pref1', :p2 = 'user1', :p3 = '1'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref1', :p1 = 'user1', :p2 = '2'
NHibernate: INSERT INTO sys.APP_PREFS (prefValue, prefId, userId, partNo) VALUES (:p0, :p1, :p2, :p3); :p0 = '111', :p1 = 'pref1', :p2 = 'user1', :p3 = '2'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref2', :p1 = 'user1', :p2 = '1'
NHibernate: INSERT INTO sys.APP_PREFS (prefValue, prefId, userId, partNo) VALUES (:p0, :p1, :p2, :p3); :p0 = '222', :p1 = 'pref2', :p2 = 'user1', :p3 = '1'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref2', :p1 = 'user1', :p2 = '2'
NHibernate: INSERT INTO sys.APP_PREFS (prefValue, prefId, userId, partNo) VALUES (:p0, :p1, :p2, :p3); :p0 = '222', :p1 = 'pref2', :p2 = 'user1', :p3 = '2'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref3', :p1 = 'user1', :p2 = '1'
NHibernate: INSERT INTO sys.APP_PREFS (prefValue, prefId, userId, partNo) VALUES (:p0, :p1, :p2, :p3); :p0 = '333', :p1 = 'pref3', :p2 = 'user1', :p3 = '1'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref1', :p1 = 'user2', :p2 = '1'
NHibernate: INSERT INTO sys.APP_PREFS (prefValue, prefId, userId, partNo) VALUES (:p0, :p1, :p2, :p3); :p0 = '444', :p1 = 'pref1', :p2 = 'user2', :p3 = '1'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref1', :p1 = 'user2', :p2 = '2'
NHibernate: INSERT INTO sys.APP_PREFS (prefValue, prefId, userId, partNo) VALUES (:p0, :p1, :p2, :p3); :p0 = '444', :p1 = 'pref1', :p2 = 'user2', :p3 = '2'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref2', :p1 = 'user2', :p2 = '1'
NHibernate: INSERT INTO sys.APP_PREFS (prefValue, prefId, userId, partNo) VALUES (:p0, :p1, :p2, :p3); :p0 = '555', :p1 = 'pref2', :p2 = 'user2', :p3 = '1'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref2', :p1 = 'user2', :p2 = '2'
NHibernate: INSERT INTO sys.APP_PREFS (prefValue, prefId, userId, partNo) VALUES (:p0, :p1, :p2, :p3); :p0 = '555', :p1 = 'pref2', :p2 = 'user2', :p3 = '2'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref3', :p1 = 'user2', :p2 = '1'
NHibernate: INSERT INTO sys.APP_PREFS (prefValue, prefId, userId, partNo) VALUES (:p0, :p1, :p2, :p3); :p0 = '666', :p1 = 'pref3', :p2 = 'user2', :p3 = '1'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref1', :p1 = '__COMMON_SETTING__', :p2 = '1'
NHibernate: INSERT INTO sys.APP_PREFS (prefValue, prefId, userId, partNo) VALUES (:p0, :p1, :p2, :p3); :p0 = '777', :p1 = 'pref1', :p2 = '__COMMON_SETTING__', :p3 = '1'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref2', :p1 = '__COMMON_SETTING__', :p2 = '1'
NHibernate: INSERT INTO sys.APP_PREFS (prefValue, prefId, userId, partNo) VALUES (:p0, :p1, :p2, :p3); :p0 = '888', :p1 = 'pref2', :p2 = '__COMMON_SETTING__', :p3 = '1'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref3', :p1 = '__COMMON_SETTING__', :p2 = '1'
NHibernate: INSERT INTO sys.APP_PREFS (prefValue, prefId, userId, partNo) VALUES (:p0, :p1, :p2, :p3); :p0 = '999', :p1 = 'pref3', :p2 = '__COMMON_SETTING__', :p3 = '1'
NHibernate: select preference0_.prefId as prefId2_, preference0_.userId as userId2_, preference0_.partNo as partNo2_, preference0_.prefValue as prefValue2_ from sys.APP_PREFS preference0_ where (preference0_.userId=:p0 )and(preference0_.prefId=:p1 )and(preference0_.partNo=:p2 ); :p0 = 'user1', :p1 = 'pref1', :p2 = '1'
NHibernate: select preference0_.prefId as prefId2_, preference0_.userId as userId2_, preference0_.partNo as partNo2_, preference0_.prefValue as prefValue2_ from sys.APP_PREFS preference0_ where (preference0_.userId=:p0 )and(preference0_.prefId=:p1 )and(preference0_.partNo=:p2 ); :p0 = 'user1', :p1 = 'pref1', :p2 = '2'
NHibernate: select preference0_.prefId as prefId2_, preference0_.userId as userId2_, preference0_.partNo as partNo2_, preference0_.prefValue as prefValue2_ from sys.APP_PREFS preference0_ where (preference0_.userId=:p0 )and(preference0_.prefId=:p1 )and(preference0_.partNo=:p2 ); :p0 = 'user1', :p1 = 'pref2', :p2 = '1'
NHibernate: select preference0_.prefId as prefId2_, preference0_.userId as userId2_, preference0_.partNo as partNo2_, preference0_.prefValue as prefValue2_ from sys.APP_PREFS preference0_ where (preference0_.userId=:p0 )and(preference0_.prefId=:p1 )and(preference0_.partNo=:p2 ); :p0 = 'user1', :p1 = 'pref2', :p2 = '2'
NHibernate: select preference0_.prefId as prefId2_, preference0_.userId as userId2_, preference0_.partNo as partNo2_, preference0_.prefValue as prefValue2_ from sys.APP_PREFS preference0_ where (preference0_.userId=:p0 )and(preference0_.prefId=:p1 )and(preference0_.partNo=:p2 ); :p0 = 'user1', :p1 = 'pref3', :p2 = '1'
NHibernate: select preference0_.prefId as prefId2_, preference0_.userId as userId2_, preference0_.partNo as partNo2_, preference0_.prefValue as prefValue2_ from sys.APP_PREFS preference0_ where (preference0_.userId=:p0 )and(preference0_.prefId=:p1 )and(preference0_.partNo=:p2 ); :p0 = 'user2', :p1 = 'pref1', :p2 = '1'
NHibernate: select preference0_.prefId as prefId2_, preference0_.userId as userId2_, preference0_.partNo as partNo2_, preference0_.prefValue as prefValue2_ from sys.APP_PREFS preference0_ where (preference0_.userId=:p0 )and(preference0_.prefId=:p1 )and(preference0_.partNo=:p2 ); :p0 = 'user2', :p1 = 'pref1', :p2 = '2'
NHibernate: select preference0_.prefId as prefId2_, preference0_.userId as userId2_, preference0_.partNo as partNo2_, preference0_.prefValue as prefValue2_ from sys.APP_PREFS preference0_ where (preference0_.userId=:p0 )and(preference0_.prefId=:p1 )and(preference0_.partNo=:p2 ); :p0 = 'user2', :p1 = 'pref2', :p2 = '1'
NHibernate: select preference0_.prefId as prefId2_, preference0_.userId as userId2_, preference0_.partNo as partNo2_, preference0_.prefValue as prefValue2_ from sys.APP_PREFS preference0_ where (preference0_.userId=:p0 )and(preference0_.prefId=:p1 )and(preference0_.partNo=:p2 ); :p0 = 'user2', :p1 = 'pref2', :p2 = '2'
NHibernate: select preference0_.prefId as prefId2_, preference0_.userId as userId2_, preference0_.partNo as partNo2_, preference0_.prefValue as prefValue2_ from sys.APP_PREFS preference0_ where (preference0_.userId=:p0 )and(preference0_.prefId=:p1 )and(preference0_.partNo=:p2 ); :p0 = 'user2', :p1 = 'pref3', :p2 = '1'
NHibernate: select preference0_.prefId as prefId2_, preference0_.userId as userId2_, preference0_.partNo as partNo2_, preference0_.prefValue as prefValue2_ from sys.APP_PREFS preference0_ where (preference0_.userId=:p0 )and(preference0_.prefId=:p1 )and(preference0_.partNo=:p2 ); :p0 = '__COMMON_SETTING__', :p1 = 'pref1', :p2 = '1'
NHibernate: select preference0_.prefId as prefId2_, preference0_.userId as userId2_, preference0_.partNo as partNo2_, preference0_.prefValue as prefValue2_ from sys.APP_PREFS preference0_ where (preference0_.userId=:p0 )and(preference0_.prefId=:p1 )and(preference0_.partNo=:p2 ); :p0 = '__COMMON_SETTING__', :p1 = 'pref2', :p2 = '1'
NHibernate: select preference0_.prefId as prefId2_, preference0_.userId as userId2_, preference0_.partNo as partNo2_, preference0_.prefValue as prefValue2_ from sys.APP_PREFS preference0_ where (preference0_.userId=:p0 )and(preference0_.prefId=:p1 )and(preference0_.partNo=:p2 ); :p0 = '__COMMON_SETTING__', :p1 = 'pref3', :p2 = '1'
NHibernate: SELECT this_.prefId as prefId2_0_, this_.userId as userId2_0_, this_.partNo as partNo2_0_, this_.prefValue as prefValue2_0_ FROM sys.APP_PREFS this_
NHibernate: select preference0_.prefId as prefId2_, preference0_.userId as userId2_, preference0_.partNo as partNo2_, preference0_.prefValue as prefValue2_ from sys.APP_PREFS preference0_ where (preference0_.userId=:p0 )and(preference0_.prefId=:p1 )and(preference0_.partNo=:p2 ); :p0 = 'user1', :p1 = 'pref1', :p2 = '1'
NHibernate: DELETE FROM sys.APP_PREFS WHERE prefId = :p0 AND userId = :p1 AND partNo = :p2; :p0 = 'pref1', :p1 = 'user1', :p2 = '1'
NHibernate: select preference0_.prefId as prefId2_, preference0_.userId as userId2_, preference0_.partNo as partNo2_, preference0_.prefValue as prefValue2_ from sys.APP_PREFS preference0_ where (preference0_.userId=:p0 )and(preference0_.prefId=:p1 )and(preference0_.partNo=:p2 ); :p0 = 'user1', :p1 = 'pref1', :p2 = '2'
NHibernate: DELETE FROM sys.APP_PREFS WHERE prefId = :p0 AND userId = :p1 AND partNo = :p2; :p0 = 'pref1', :p1 = 'user1', :p2 = '2'
NHibernate: select preference0_.prefId as prefId2_, preference0_.userId as userId2_, preference0_.partNo as partNo2_, preference0_.prefValue as prefValue2_ from sys.APP_PREFS preference0_ where (preference0_.userId=:p0 )and(preference0_.prefId=:p1 )and(preference0_.partNo=:p2 ); :p0 = 'user1', :p1 = 'pref2', :p2 = '1'
NHibernate: DELETE FROM sys.APP_PREFS WHERE prefId = :p0 AND userId = :p1 AND partNo = :p2; :p0 = 'pref2', :p1 = 'user1', :p2 = '1'
NHibernate: select preference0_.prefId as prefId2_, preference0_.userId as userId2_, preference0_.partNo as partNo2_, preference0_.prefValue as prefValue2_ from sys.APP_PREFS preference0_ where (preference0_.userId=:p0 )and(preference0_.prefId=:p1 )and(preference0_.partNo=:p2 ); :p0 = 'user1', :p1 = 'pref2', :p2 = '2'
NHibernate: DELETE FROM sys.APP_PREFS WHERE prefId = :p0 AND userId = :p1 AND partNo = :p2; :p0 = 'pref2', :p1 = 'user1', :p2 = '2'
NHibernate: select preference0_.prefId as prefId2_, preference0_.userId as userId2_, preference0_.partNo as partNo2_, preference0_.prefValue as prefValue2_ from sys.APP_PREFS preference0_ where (preference0_.userId=:p0 )and(preference0_.prefId=:p1 )and(preference0_.partNo=:p2 ); :p0 = 'user1', :p1 = 'pref3', :p2 = '1'
NHibernate: DELETE FROM sys.APP_PREFS WHERE prefId = :p0 AND userId = :p1 AND partNo = :p2; :p0 = 'pref3', :p1 = 'user1', :p2 = '1'
NHibernate: select preference0_.prefId as prefId2_, preference0_.userId as userId2_, preference0_.partNo as partNo2_, preference0_.prefValue as prefValue2_ from sys.APP_PREFS preference0_ where (preference0_.userId=:p0 )and(preference0_.prefId=:p1 )and(preference0_.partNo=:p2 ); :p0 = 'user2', :p1 = 'pref1', :p2 = '1'
NHibernate: DELETE FROM sys.APP_PREFS WHERE prefId = :p0 AND userId = :p1 AND partNo = :p2; :p0 = 'pref1', :p1 = 'user2', :p2 = '1'
NHibernate: select preference0_.prefId as prefId2_, preference0_.userId as userId2_, preference0_.partNo as partNo2_, preference0_.prefValue as prefValue2_ from sys.APP_PREFS preference0_ where (preference0_.userId=:p0 )and(preference0_.prefId=:p1 )and(preference0_.partNo=:p2 ); :p0 = 'user2', :p1 = 'pref1', :p2 = '2'
NHibernate: DELETE FROM sys.APP_PREFS WHERE prefId = :p0 AND userId = :p1 AND partNo = :p2; :p0 = 'pref1', :p1 = 'user2', :p2 = '2'
NHibernate: select preference0_.prefId as prefId2_, preference0_.userId as userId2_, preference0_.partNo as partNo2_, preference0_.prefValue as prefValue2_ from sys.APP_PREFS preference0_ where (preference0_.userId=:p0 )and(preference0_.prefId=:p1 )and(preference0_.partNo=:p2 ); :p0 = 'user2', :p1 = 'pref2', :p2 = '1'
NHibernate: DELETE FROM sys.APP_PREFS WHERE prefId = :p0 AND userId = :p1 AND partNo = :p2; :p0 = 'pref2', :p1 = 'user2', :p2 = '1'
NHibernate: select preference0_.prefId as prefId2_, preference0_.userId as userId2_, preference0_.partNo as partNo2_, preference0_.prefValue as prefValue2_ from sys.APP_PREFS preference0_ where (preference0_.userId=:p0 )and(preference0_.prefId=:p1 )and(preference0_.partNo=:p2 ); :p0 = 'user2', :p1 = 'pref2', :p2 = '2'
NHibernate: DELETE FROM sys.APP_PREFS WHERE prefId = :p0 AND userId = :p1 AND partNo = :p2; :p0 = 'pref2', :p1 = 'user2', :p2 = '2'
NHibernate: select preference0_.prefId as prefId2_, preference0_.userId as userId2_, preference0_.partNo as partNo2_, preference0_.prefValue as prefValue2_ from sys.APP_PREFS preference0_ where (preference0_.userId=:p0 )and(preference0_.prefId=:p1 )and(preference0_.partNo=:p2 ); :p0 = 'user2', :p1 = 'pref3', :p2 = '1'
NHibernate: DELETE FROM sys.APP_PREFS WHERE prefId = :p0 AND userId = :p1 AND partNo = :p2; :p0 = 'pref3', :p1 = 'user2', :p2 = '1'
NHibernate: select preference0_.prefId as prefId2_, preference0_.userId as userId2_, preference0_.partNo as partNo2_, preference0_.prefValue as prefValue2_ from sys.APP_PREFS preference0_ where (preference0_.userId=:p0 )and(preference0_.prefId=:p1 )and(preference0_.partNo=:p2 ); :p0 = '__COMMON_SETTING__', :p1 = 'pref1', :p2 = '1'
NHibernate: DELETE FROM sys.APP_PREFS WHERE prefId = :p0 AND userId = :p1 AND partNo = :p2; :p0 = 'pref1', :p1 = '__COMMON_SETTING__', :p2 = '1'
NHibernate: select preference0_.prefId as prefId2_, preference0_.userId as userId2_, preference0_.partNo as partNo2_, preference0_.prefValue as prefValue2_ from sys.APP_PREFS preference0_ where (preference0_.userId=:p0 )and(preference0_.prefId=:p1 )and(preference0_.partNo=:p2 ); :p0 = '__COMMON_SETTING__', :p1 = 'pref2', :p2 = '1'
NHibernate: DELETE FROM sys.APP_PREFS WHERE prefId = :p0 AND userId = :p1 AND partNo = :p2; :p0 = 'pref2', :p1 = '__COMMON_SETTING__', :p2 = '1'
NHibernate: select preference0_.prefId as prefId2_, preference0_.userId as userId2_, preference0_.partNo as partNo2_, preference0_.prefValue as prefValue2_ from sys.APP_PREFS preference0_ where (preference0_.userId=:p0 )and(preference0_.prefId=:p1 )and(preference0_.partNo=:p2 ); :p0 = '__COMMON_SETTING__', :p1 = 'pref3', :p2 = '1'
NHibernate: DELETE FROM sys.APP_PREFS WHERE prefId = :p0 AND userId = :p1 AND partNo = :p2; :p0 = 'pref3', :p1 = '__COMMON_SETTING__', :p2 = '1'
NHibernate: SELECT this_.prefId as prefId2_0_, this_.userId as userId2_0_, this_.partNo as partNo2_0_, this_.prefValue as prefValue2_0_ FROM sys.APP_PREFS this_
Disposing of RW ISession 27632982
***** AskueCtp.Tests.DataAccess.RwDb.PreferenceTests.GetAll
NHibernate: delete sys.app_prefs
NHibernate: insert into sys.app_prefs (prefid, userid, partno, prefvalue) values ('pref1', 'user1', '1', '111')
NHibernate: insert into sys.app_prefs (prefid, userid, partno, prefvalue) values ('pref1', 'user1', '2', '111')
NHibernate: insert into sys.app_prefs (prefid, userid, partno, prefvalue) values ('pref2', 'user1', '1', '222')
NHibernate: insert into sys.app_prefs (prefid, userid, partno, prefvalue) values ('pref2', 'user1', '2', '222')
NHibernate: insert into sys.app_prefs (prefid, userid, partno, prefvalue) values ('pref3', 'user1', '1', '333')
NHibernate: insert into sys.app_prefs (prefid, userid, partno, prefvalue) values ('pref1', 'user2', '1', '444')
NHibernate: insert into sys.app_prefs (prefid, userid, partno, prefvalue) values ('pref1', 'user2', '2', '444')
NHibernate: insert into sys.app_prefs (prefid, userid, partno, prefvalue) values ('pref2', 'user2', '1', '555')
NHibernate: insert into sys.app_prefs (prefid, userid, partno, prefvalue) values ('pref2', 'user2', '2', '555')
NHibernate: insert into sys.app_prefs (prefid, userid, partno, prefvalue) values ('pref3', 'user2', '1', '666')
NHibernate: insert into sys.app_prefs (prefid, userid, partno, prefvalue) values ('pref1', '__COMMON_SETTING__', '1', '777')
NHibernate: insert into sys.app_prefs (prefid, userid, partno, prefvalue) values ('pref2', '__COMMON_SETTING__', '1', '888')
NHibernate: insert into sys.app_prefs (prefid, userid, partno, prefvalue) values ('pref3', '__COMMON_SETTING__', '1', '999')
NHibernate: SELECT this_.prefId as prefId2_0_, this_.userId as userId2_0_, this_.partNo as partNo2_0_, this_.prefValue as prefValue2_0_ FROM sys.APP_PREFS this_
Disposing of RW ISession 1684448
***** AskueCtp.Tests.DataAccess.RwDb.PreferenceTests.GetById
NHibernate: delete sys.app_prefs
NHibernate: insert into sys.app_prefs (prefid, userid, partno, prefvalue) values ('pref1', 'user1', '1', '111')
NHibernate: select preference0_.prefId as prefId2_, preference0_.userId as userId2_, preference0_.partNo as partNo2_, preference0_.prefValue as prefValue2_ from sys.APP_PREFS preference0_ where (preference0_.userId=:p0 )and(preference0_.prefId=:p1 )and(preference0_.partNo=:p2 ); :p0 = 'user1', :p1 = 'pref1', :p2 = '1'
NHibernate: insert into sys.app_prefs (prefid, userid, partno, prefvalue) values ('pref1', 'user1', '2', '111')
NHibernate: select preference0_.prefId as prefId2_, preference0_.userId as userId2_, preference0_.partNo as partNo2_, preference0_.prefValue as prefValue2_ from sys.APP_PREFS preference0_ where (preference0_.userId=:p0 )and(preference0_.prefId=:p1 )and(preference0_.partNo=:p2 ); :p0 = 'user1', :p1 = 'pref1', :p2 = '2'
NHibernate: insert into sys.app_prefs (prefid, userid, partno, prefvalue) values ('pref2', 'user1', '1', '222')
NHibernate: select preference0_.prefId as prefId2_, preference0_.userId as userId2_, preference0_.partNo as partNo2_, preference0_.prefValue as prefValue2_ from sys.APP_PREFS preference0_ where (preference0_.userId=:p0 )and(preference0_.prefId=:p1 )and(preference0_.partNo=:p2 ); :p0 = 'user1', :p1 = 'pref2', :p2 = '1'
NHibernate: insert into sys.app_prefs (prefid, userid, partno, prefvalue) values ('pref2', 'user1', '2', '222')
NHibernate: select preference0_.prefId as prefId2_, preference0_.userId as userId2_, preference0_.partNo as partNo2_, preference0_.prefValue as prefValue2_ from sys.APP_PREFS preference0_ where (preference0_.userId=:p0 )and(preference0_.prefId=:p1 )and(preference0_.partNo=:p2 ); :p0 = 'user1', :p1 = 'pref2', :p2 = '2'
NHibernate: insert into sys.app_prefs (prefid, userid, partno, prefvalue) values ('pref3', 'user1', '1', '333')
NHibernate: select preference0_.prefId as prefId2_, preference0_.userId as userId2_, preference0_.partNo as partNo2_, preference0_.prefValue as prefValue2_ from sys.APP_PREFS preference0_ where (preference0_.userId=:p0 )and(preference0_.prefId=:p1 )and(preference0_.partNo=:p2 ); :p0 = 'user1', :p1 = 'pref3', :p2 = '1'
NHibernate: insert into sys.app_prefs (prefid, userid, partno, prefvalue) values ('pref1', 'user2', '1', '444')
NHibernate: select preference0_.prefId as prefId2_, preference0_.userId as userId2_, preference0_.partNo as partNo2_, preference0_.prefValue as prefValue2_ from sys.APP_PREFS preference0_ where (preference0_.userId=:p0 )and(preference0_.prefId=:p1 )and(preference0_.partNo=:p2 ); :p0 = 'user2', :p1 = 'pref1', :p2 = '1'
NHibernate: insert into sys.app_prefs (prefid, userid, partno, prefvalue) values ('pref1', 'user2', '2', '444')
NHibernate: select preference0_.prefId as prefId2_, preference0_.userId as userId2_, preference0_.partNo as partNo2_, preference0_.prefValue as prefValue2_ from sys.APP_PREFS preference0_ where (preference0_.userId=:p0 )and(preference0_.prefId=:p1 )and(preference0_.partNo=:p2 ); :p0 = 'user2', :p1 = 'pref1', :p2 = '2'
NHibernate: insert into sys.app_prefs (prefid, userid, partno, prefvalue) values ('pref2', 'user2', '1', '555')
NHibernate: select preference0_.prefId as prefId2_, preference0_.userId as userId2_, preference0_.partNo as partNo2_, preference0_.prefValue as prefValue2_ from sys.APP_PREFS preference0_ where (preference0_.userId=:p0 )and(preference0_.prefId=:p1 )and(preference0_.partNo=:p2 ); :p0 = 'user2', :p1 = 'pref2', :p2 = '1'
NHibernate: insert into sys.app_prefs (prefid, userid, partno, prefvalue) values ('pref2', 'user2', '2', '555')
NHibernate: select preference0_.prefId as prefId2_, preference0_.userId as userId2_, preference0_.partNo as partNo2_, preference0_.prefValue as prefValue2_ from sys.APP_PREFS preference0_ where (preference0_.userId=:p0 )and(preference0_.prefId=:p1 )and(preference0_.partNo=:p2 ); :p0 = 'user2', :p1 = 'pref2', :p2 = '2'
NHibernate: insert into sys.app_prefs (prefid, userid, partno, prefvalue) values ('pref3', 'user2', '1', '666')
NHibernate: select preference0_.prefId as prefId2_, preference0_.userId as userId2_, preference0_.partNo as partNo2_, preference0_.prefValue as prefValue2_ from sys.APP_PREFS preference0_ where (preference0_.userId=:p0 )and(preference0_.prefId=:p1 )and(preference0_.partNo=:p2 ); :p0 = 'user2', :p1 = 'pref3', :p2 = '1'
NHibernate: insert into sys.app_prefs (prefid, userid, partno, prefvalue) values ('pref1', '__COMMON_SETTING__', '1', '777')
NHibernate: select preference0_.prefId as prefId2_, preference0_.userId as userId2_, preference0_.partNo as partNo2_, preference0_.prefValue as prefValue2_ from sys.APP_PREFS preference0_ where (preference0_.userId=:p0 )and(preference0_.prefId=:p1 )and(preference0_.partNo=:p2 ); :p0 = '__COMMON_SETTING__', :p1 = 'pref1', :p2 = '1'
NHibernate: insert into sys.app_prefs (prefid, userid, partno, prefvalue) values ('pref2', '__COMMON_SETTING__', '1', '888')
NHibernate: select preference0_.prefId as prefId2_, preference0_.userId as userId2_, preference0_.partNo as partNo2_, preference0_.prefValue as prefValue2_ from sys.APP_PREFS preference0_ where (preference0_.userId=:p0 )and(preference0_.prefId=:p1 )and(preference0_.partNo=:p2 ); :p0 = '__COMMON_SETTING__', :p1 = 'pref2', :p2 = '1'
NHibernate: insert into sys.app_prefs (prefid, userid, partno, prefvalue) values ('pref3', '__COMMON_SETTING__', '1', '999')
NHibernate: select preference0_.prefId as prefId2_, preference0_.userId as userId2_, preference0_.partNo as partNo2_, preference0_.prefValue as prefValue2_ from sys.APP_PREFS preference0_ where (preference0_.userId=:p0 )and(preference0_.prefId=:p1 )and(preference0_.partNo=:p2 ); :p0 = '__COMMON_SETTING__', :p1 = 'pref3', :p2 = '1'
Disposing of RW ISession 44975361
***** AskueCtp.Tests.DataAccess.RwDb.PreferenceTests.GetNonExistItem
NHibernate: select preference0_.prefId as prefId2_, preference0_.userId as userId2_, preference0_.partNo as partNo2_, preference0_.prefValue as prefValue2_ from sys.APP_PREFS preference0_ where (preference0_.userId=:p0 )and(preference0_.prefId=:p1 )and(preference0_.partNo=:p2 ); :p0 = 'na', :p1 = 'na', :p2 = '0'
Disposing of RW ISession 19242328
***** AskueCtp.Tests.DataAccess.RwDb.PreferenceTests.SaveOnAdding
NHibernate: delete sys.app_prefs
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref1', :p1 = 'user1', :p2 = '1'
NHibernate: INSERT INTO sys.APP_PREFS (prefValue, prefId, userId, partNo) VALUES (:p0, :p1, :p2, :p3); :p0 = '111', :p1 = 'pref1', :p2 = 'user1', :p3 = '1'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref1', :p1 = 'user1', :p2 = '2'
NHibernate: INSERT INTO sys.APP_PREFS (prefValue, prefId, userId, partNo) VALUES (:p0, :p1, :p2, :p3); :p0 = '111', :p1 = 'pref1', :p2 = 'user1', :p3 = '2'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref2', :p1 = 'user1', :p2 = '1'
NHibernate: INSERT INTO sys.APP_PREFS (prefValue, prefId, userId, partNo) VALUES (:p0, :p1, :p2, :p3); :p0 = '222', :p1 = 'pref2', :p2 = 'user1', :p3 = '1'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref2', :p1 = 'user1', :p2 = '2'
NHibernate: INSERT INTO sys.APP_PREFS (prefValue, prefId, userId, partNo) VALUES (:p0, :p1, :p2, :p3); :p0 = '222', :p1 = 'pref2', :p2 = 'user1', :p3 = '2'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref3', :p1 = 'user1', :p2 = '1'
NHibernate: INSERT INTO sys.APP_PREFS (prefValue, prefId, userId, partNo) VALUES (:p0, :p1, :p2, :p3); :p0 = '333', :p1 = 'pref3', :p2 = 'user1', :p3 = '1'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref1', :p1 = 'user2', :p2 = '1'
NHibernate: INSERT INTO sys.APP_PREFS (prefValue, prefId, userId, partNo) VALUES (:p0, :p1, :p2, :p3); :p0 = '444', :p1 = 'pref1', :p2 = 'user2', :p3 = '1'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref1', :p1 = 'user2', :p2 = '2'
NHibernate: INSERT INTO sys.APP_PREFS (prefValue, prefId, userId, partNo) VALUES (:p0, :p1, :p2, :p3); :p0 = '444', :p1 = 'pref1', :p2 = 'user2', :p3 = '2'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref2', :p1 = 'user2', :p2 = '1'
NHibernate: INSERT INTO sys.APP_PREFS (prefValue, prefId, userId, partNo) VALUES (:p0, :p1, :p2, :p3); :p0 = '555', :p1 = 'pref2', :p2 = 'user2', :p3 = '1'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref2', :p1 = 'user2', :p2 = '2'
NHibernate: INSERT INTO sys.APP_PREFS (prefValue, prefId, userId, partNo) VALUES (:p0, :p1, :p2, :p3); :p0 = '555', :p1 = 'pref2', :p2 = 'user2', :p3 = '2'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref3', :p1 = 'user2', :p2 = '1'
NHibernate: INSERT INTO sys.APP_PREFS (prefValue, prefId, userId, partNo) VALUES (:p0, :p1, :p2, :p3); :p0 = '666', :p1 = 'pref3', :p2 = 'user2', :p3 = '1'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref1', :p1 = '__COMMON_SETTING__', :p2 = '1'
NHibernate: INSERT INTO sys.APP_PREFS (prefValue, prefId, userId, partNo) VALUES (:p0, :p1, :p2, :p3); :p0 = '777', :p1 = 'pref1', :p2 = '__COMMON_SETTING__', :p3 = '1'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref2', :p1 = '__COMMON_SETTING__', :p2 = '1'
NHibernate: INSERT INTO sys.APP_PREFS (prefValue, prefId, userId, partNo) VALUES (:p0, :p1, :p2, :p3); :p0 = '888', :p1 = 'pref2', :p2 = '__COMMON_SETTING__', :p3 = '1'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref3', :p1 = '__COMMON_SETTING__', :p2 = '1'
NHibernate: INSERT INTO sys.APP_PREFS (prefValue, prefId, userId, partNo) VALUES (:p0, :p1, :p2, :p3); :p0 = '999', :p1 = 'pref3', :p2 = '__COMMON_SETTING__', :p3 = '1'
NHibernate: select preference0_.prefId as prefId2_, preference0_.userId as userId2_, preference0_.partNo as partNo2_, preference0_.prefValue as prefValue2_ from sys.APP_PREFS preference0_ where (preference0_.userId=:p0 )and(preference0_.prefId=:p1 )and(preference0_.partNo=:p2 ); :p0 = 'user1', :p1 = 'pref1', :p2 = '1'
NHibernate: select preference0_.prefId as prefId2_, preference0_.userId as userId2_, preference0_.partNo as partNo2_, preference0_.prefValue as prefValue2_ from sys.APP_PREFS preference0_ where (preference0_.userId=:p0 )and(preference0_.prefId=:p1 )and(preference0_.partNo=:p2 ); :p0 = 'user1', :p1 = 'pref1', :p2 = '2'
NHibernate: select preference0_.prefId as prefId2_, preference0_.userId as userId2_, preference0_.partNo as partNo2_, preference0_.prefValue as prefValue2_ from sys.APP_PREFS preference0_ where (preference0_.userId=:p0 )and(preference0_.prefId=:p1 )and(preference0_.partNo=:p2 ); :p0 = 'user1', :p1 = 'pref2', :p2 = '1'
NHibernate: select preference0_.prefId as prefId2_, preference0_.userId as userId2_, preference0_.partNo as partNo2_, preference0_.prefValue as prefValue2_ from sys.APP_PREFS preference0_ where (preference0_.userId=:p0 )and(preference0_.prefId=:p1 )and(preference0_.partNo=:p2 ); :p0 = 'user1', :p1 = 'pref2', :p2 = '2'
NHibernate: select preference0_.prefId as prefId2_, preference0_.userId as userId2_, preference0_.partNo as partNo2_, preference0_.prefValue as prefValue2_ from sys.APP_PREFS preference0_ where (preference0_.userId=:p0 )and(preference0_.prefId=:p1 )and(preference0_.partNo=:p2 ); :p0 = 'user1', :p1 = 'pref3', :p2 = '1'
NHibernate: select preference0_.prefId as prefId2_, preference0_.userId as userId2_, preference0_.partNo as partNo2_, preference0_.prefValue as prefValue2_ from sys.APP_PREFS preference0_ where (preference0_.userId=:p0 )and(preference0_.prefId=:p1 )and(preference0_.partNo=:p2 ); :p0 = 'user2', :p1 = 'pref1', :p2 = '1'
NHibernate: select preference0_.prefId as prefId2_, preference0_.userId as userId2_, preference0_.partNo as partNo2_, preference0_.prefValue as prefValue2_ from sys.APP_PREFS preference0_ where (preference0_.userId=:p0 )and(preference0_.prefId=:p1 )and(preference0_.partNo=:p2 ); :p0 = 'user2', :p1 = 'pref1', :p2 = '2'
NHibernate: select preference0_.prefId as prefId2_, preference0_.userId as userId2_, preference0_.partNo as partNo2_, preference0_.prefValue as prefValue2_ from sys.APP_PREFS preference0_ where (preference0_.userId=:p0 )and(preference0_.prefId=:p1 )and(preference0_.partNo=:p2 ); :p0 = 'user2', :p1 = 'pref2', :p2 = '1'
NHibernate: select preference0_.prefId as prefId2_, preference0_.userId as userId2_, preference0_.partNo as partNo2_, preference0_.prefValue as prefValue2_ from sys.APP_PREFS preference0_ where (preference0_.userId=:p0 )and(preference0_.prefId=:p1 )and(preference0_.partNo=:p2 ); :p0 = 'user2', :p1 = 'pref2', :p2 = '2'
NHibernate: select preference0_.prefId as prefId2_, preference0_.userId as userId2_, preference0_.partNo as partNo2_, preference0_.prefValue as prefValue2_ from sys.APP_PREFS preference0_ where (preference0_.userId=:p0 )and(preference0_.prefId=:p1 )and(preference0_.partNo=:p2 ); :p0 = 'user2', :p1 = 'pref3', :p2 = '1'
NHibernate: select preference0_.prefId as prefId2_, preference0_.userId as userId2_, preference0_.partNo as partNo2_, preference0_.prefValue as prefValue2_ from sys.APP_PREFS preference0_ where (preference0_.userId=:p0 )and(preference0_.prefId=:p1 )and(preference0_.partNo=:p2 ); :p0 = '__COMMON_SETTING__', :p1 = 'pref1', :p2 = '1'
NHibernate: select preference0_.prefId as prefId2_, preference0_.userId as userId2_, preference0_.partNo as partNo2_, preference0_.prefValue as prefValue2_ from sys.APP_PREFS preference0_ where (preference0_.userId=:p0 )and(preference0_.prefId=:p1 )and(preference0_.partNo=:p2 ); :p0 = '__COMMON_SETTING__', :p1 = 'pref2', :p2 = '1'
NHibernate: select preference0_.prefId as prefId2_, preference0_.userId as userId2_, preference0_.partNo as partNo2_, preference0_.prefValue as prefValue2_ from sys.APP_PREFS preference0_ where (preference0_.userId=:p0 )and(preference0_.prefId=:p1 )and(preference0_.partNo=:p2 ); :p0 = '__COMMON_SETTING__', :p1 = 'pref3', :p2 = '1'
NHibernate: SELECT this_.prefId as prefId2_0_, this_.userId as userId2_0_, this_.partNo as partNo2_0_, this_.prefValue as prefValue2_0_ FROM sys.APP_PREFS this_
Disposing of RW ISession 35551176
***** AskueCtp.Tests.DataAccess.RwDb.PreferenceTests.SaveOnUpdating
NHibernate: delete sys.app_prefs
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref1', :p1 = 'user1', :p2 = '1'
NHibernate: INSERT INTO sys.APP_PREFS (prefValue, prefId, userId, partNo) VALUES (:p0, :p1, :p2, :p3); :p0 = '111', :p1 = 'pref1', :p2 = 'user1', :p3 = '1'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref1', :p1 = 'user1', :p2 = '1'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref1', :p1 = 'user1', :p2 = '1'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref1', :p1 = 'user1', :p2 = '1'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref1', :p1 = 'user1', :p2 = '2'
NHibernate: INSERT INTO sys.APP_PREFS (prefValue, prefId, userId, partNo) VALUES (:p0, :p1, :p2, :p3); :p0 = '111', :p1 = 'pref1', :p2 = 'user1', :p3 = '2'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref1', :p1 = 'user1', :p2 = '2'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref1', :p1 = 'user1', :p2 = '2'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref1', :p1 = 'user1', :p2 = '2'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref2', :p1 = 'user1', :p2 = '1'
NHibernate: INSERT INTO sys.APP_PREFS (prefValue, prefId, userId, partNo) VALUES (:p0, :p1, :p2, :p3); :p0 = '222', :p1 = 'pref2', :p2 = 'user1', :p3 = '1'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref2', :p1 = 'user1', :p2 = '1'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref2', :p1 = 'user1', :p2 = '1'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref2', :p1 = 'user1', :p2 = '1'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref2', :p1 = 'user1', :p2 = '2'
NHibernate: INSERT INTO sys.APP_PREFS (prefValue, prefId, userId, partNo) VALUES (:p0, :p1, :p2, :p3); :p0 = '222', :p1 = 'pref2', :p2 = 'user1', :p3 = '2'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref2', :p1 = 'user1', :p2 = '2'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref2', :p1 = 'user1', :p2 = '2'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref2', :p1 = 'user1', :p2 = '2'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref3', :p1 = 'user1', :p2 = '1'
NHibernate: INSERT INTO sys.APP_PREFS (prefValue, prefId, userId, partNo) VALUES (:p0, :p1, :p2, :p3); :p0 = '333', :p1 = 'pref3', :p2 = 'user1', :p3 = '1'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref3', :p1 = 'user1', :p2 = '1'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref3', :p1 = 'user1', :p2 = '1'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref3', :p1 = 'user1', :p2 = '1'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref1', :p1 = 'user2', :p2 = '1'
NHibernate: INSERT INTO sys.APP_PREFS (prefValue, prefId, userId, partNo) VALUES (:p0, :p1, :p2, :p3); :p0 = '444', :p1 = 'pref1', :p2 = 'user2', :p3 = '1'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref1', :p1 = 'user2', :p2 = '1'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref1', :p1 = 'user2', :p2 = '1'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref1', :p1 = 'user2', :p2 = '1'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref1', :p1 = 'user2', :p2 = '2'
NHibernate: INSERT INTO sys.APP_PREFS (prefValue, prefId, userId, partNo) VALUES (:p0, :p1, :p2, :p3); :p0 = '444', :p1 = 'pref1', :p2 = 'user2', :p3 = '2'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref1', :p1 = 'user2', :p2 = '2'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref1', :p1 = 'user2', :p2 = '2'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref1', :p1 = 'user2', :p2 = '2'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref2', :p1 = 'user2', :p2 = '1'
NHibernate: INSERT INTO sys.APP_PREFS (prefValue, prefId, userId, partNo) VALUES (:p0, :p1, :p2, :p3); :p0 = '555', :p1 = 'pref2', :p2 = 'user2', :p3 = '1'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref2', :p1 = 'user2', :p2 = '1'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref2', :p1 = 'user2', :p2 = '1'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref2', :p1 = 'user2', :p2 = '1'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref2', :p1 = 'user2', :p2 = '2'
NHibernate: INSERT INTO sys.APP_PREFS (prefValue, prefId, userId, partNo) VALUES (:p0, :p1, :p2, :p3); :p0 = '555', :p1 = 'pref2', :p2 = 'user2', :p3 = '2'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref2', :p1 = 'user2', :p2 = '2'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref2', :p1 = 'user2', :p2 = '2'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref2', :p1 = 'user2', :p2 = '2'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref3', :p1 = 'user2', :p2 = '1'
NHibernate: INSERT INTO sys.APP_PREFS (prefValue, prefId, userId, partNo) VALUES (:p0, :p1, :p2, :p3); :p0 = '666', :p1 = 'pref3', :p2 = 'user2', :p3 = '1'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref3', :p1 = 'user2', :p2 = '1'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref3', :p1 = 'user2', :p2 = '1'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref3', :p1 = 'user2', :p2 = '1'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref1', :p1 = '__COMMON_SETTING__', :p2 = '1'
NHibernate: INSERT INTO sys.APP_PREFS (prefValue, prefId, userId, partNo) VALUES (:p0, :p1, :p2, :p3); :p0 = '777', :p1 = 'pref1', :p2 = '__COMMON_SETTING__', :p3 = '1'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref1', :p1 = '__COMMON_SETTING__', :p2 = '1'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref1', :p1 = '__COMMON_SETTING__', :p2 = '1'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref1', :p1 = '__COMMON_SETTING__', :p2 = '1'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref2', :p1 = '__COMMON_SETTING__', :p2 = '1'
NHibernate: INSERT INTO sys.APP_PREFS (prefValue, prefId, userId, partNo) VALUES (:p0, :p1, :p2, :p3); :p0 = '888', :p1 = 'pref2', :p2 = '__COMMON_SETTING__', :p3 = '1'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref2', :p1 = '__COMMON_SETTING__', :p2 = '1'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref2', :p1 = '__COMMON_SETTING__', :p2 = '1'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref2', :p1 = '__COMMON_SETTING__', :p2 = '1'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref3', :p1 = '__COMMON_SETTING__', :p2 = '1'
NHibernate: INSERT INTO sys.APP_PREFS (prefValue, prefId, userId, partNo) VALUES (:p0, :p1, :p2, :p3); :p0 = '999', :p1 = 'pref3', :p2 = '__COMMON_SETTING__', :p3 = '1'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref3', :p1 = '__COMMON_SETTING__', :p2 = '1'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref3', :p1 = '__COMMON_SETTING__', :p2 = '1'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'pref3', :p1 = '__COMMON_SETTING__', :p2 = '1'
NHibernate: SELECT this_.prefId as prefId2_0_, this_.userId as userId2_0_, this_.partNo as partNo2_0_, this_.prefValue as prefValue2_0_ FROM sys.APP_PREFS this_
Disposing of RW ISession 52663698
***** AskueCtp.Tests.DataAccess.RwDb.PreferenceTests.SerializedPreferences
NHibernate: delete sys.app_prefs
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'sp_pref1', :p1 = 'sp_user1', :p2 = '1'
NHibernate: INSERT INTO sys.APP_PREFS (prefValue, prefId, userId, partNo) VALUES (:p0, :p1, :p2, :p3); :p0 = '<SOAP-ENV:Envelope xmlns:xsi=http:www.w3.org2001XMLSchema-instance xmlns:xsd=http://www.w3.org/2001/XMLSchema xmlns:SOAP-ENC=http://schemas.xmlsoap.org/soap/encoding/ xmlns:SOAP-ENV=http://schemas.xmlsoap.org/soap/envelope/ xmlns:clr=http://schemas.microsoft.com/soap/encoding/clr/1.0 SOAP-ENV:encodingStyle=http://schemas.xmlsoap.org/soap/encoding/>
<SOAP-ENV:Body>
<a1:PreferenceTests_x002B_LargePreference id=ref-1 xmlns:a1=http://schemas.microsoft.com/clr/nsassem/AskueCtp.Tests.DataAccess.RwDb/AskueTests%2C%20Version%3D1.0.0.0%2C%20Culture%3Dneutral%2C%20PublicKeyToken%3Dnull>
<m_integer>10</m_integer>
<m_string id=ref-3>Large Preference</m_string>
</a1:PreferenceTests_x002B_LargePreference>
</SOAP-ENV:Body>
</SOAP-ENV:Envelope>
', :p1 = 'sp_pref1', :p2 = 'sp_user1', :p3 = '1'
NHibernate: SELECT preference0_.prefId as prefId2_0_, preference0_.userId as userId2_0_, preference0_.partNo as partNo2_0_, preference0_.prefValue as prefValue2_0_ FROM sys.APP_PREFS preference0_ WHERE preference0_.prefId=:p0 and preference0_.userId=:p1 and preference0_.partNo=:p2; :p0 = 'sp_pref2', :p1 = 'sp_user2', :p2 = '1'
NHibernate: INSERT INTO sys.APP_PREFS (prefValue, prefId, userId, partNo) VALUES (:p0, :p1, :p2, :p3); :p0 = '<SOAP-ENV:Envelope xmlns:xsi=http://www.w3.org/2001/XMLSchema-instance xmlns:xsd=http://www.w3.org/2001/XMLSchema xmlns:SOAP-ENC=http://schemas.xmlsoap.org/soap/encoding/ xmlns:SOAP-ENV=http://schemas.xmlsoap.org/soap/envelope/ xmlns:clr=http://schemas.microsoft.com/soap/encoding/clr/1.0 SOAP-ENV:encodingStyle=http://schemas.xmlsoap.org/soap/encoding/>
<SOAP-ENV:Body>
<a1:PreferenceTests_x002B_LargePreference id=ref-1 xmlns:a1=http://schemas.microsoft.com/clr/nsassem/AskueCtp.Tests.DataAccess.RwDb/AskueTests%2C%20Version%3D1.0.0.0%2C%20Culture%3Dneutral%2C%20PublicKeyToken%3Dnull>
<m_integer>20</m_integer>
<m_string id=ref-3>Русский</m_string>
</a1:PreferenceTests_x002B_LargePreference>
</SOAP-ENV:Body>
</SOAP-ENV:Envelope>
', :p1 = 'sp_pref2', :p2 = 'sp_user2', :p3 = '1'
NHibernate: select preference0_.prefId as prefId2_, preference0_.userId as userId2_, preference0_.partNo as partNo2_, preference0_.prefValue as prefValue2_ from sys.APP_PREFS preference0_ where (preference0_.userId=:p0 )and(preference0_.prefId=:p1 )and(preference0_.partNo=:p2 ); :p0 = 'sp_user1', :p1 = 'sp_pref1', :p2 = '1'
NHibernate: select preference0_.prefId as prefId2_, preference0_.userId as userId2_, preference0_.partNo as partNo2_, preference0_.prefValue as prefValue2_ from sys.APP_PREFS preference0_ where (preference0_.userId=:p0 )and(preference0_.prefId=:p1 )and(preference0_.partNo=:p2 ); :p0 = 'sp_user2', :p1 = 'sp_pref2', :p2 = '1'
Disposing of RW ISession 47417620
    ";
    }
}
