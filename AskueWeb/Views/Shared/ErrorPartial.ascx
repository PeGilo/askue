﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AskueWeb.ViewModel.Error>" %>

<div class="error-text"><%= Html.Encode(Model.Text) %></div>
