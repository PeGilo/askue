﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<AskueWeb.ViewModel.Error>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Ошибка
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<div class="error-text"><%= Html.Encode(Model.Text) %></div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContentRight" runat="server">
</asp:Content>

