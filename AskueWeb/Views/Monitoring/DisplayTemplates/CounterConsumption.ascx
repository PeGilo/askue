﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AskueWeb.ViewModel.CounterConsumption>" %>

<%--<tr>
    <td>

    </td>
    <td>
        <%= Html.Encode(Model.Name) %>
    </td>
    <td>
        <%= Html.Encode(Model.Time.ToString("t"))%>
    </td>
    <td style="text-align:right">
        <%= Html.Encode(String.Format("{0:F}", Model.Consumption))%>
    </td>
</tr>--%>

<div class="counter-box shadow <%= Model.Warning ? "counter-warning" : "counter-normal" %>">
    <div class="name">
        <span title="<%= Html.Encode(Model.Name) %>"><%= Html.Encode(Model.Name) %></span>
    </div>
    <div class="time">
        <%= Html.Encode(Model.Time.ToString("t"))%>
    </div>
    <div class="consumption">
        <%= Html.Encode(String.Format("{0:F}", Model.Consumption))%>
    </div>
</div>
        
