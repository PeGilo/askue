﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" 
Inherits="System.Web.Mvc.ViewPage<IList<AskueWeb.ViewModel.RollerItem>>" %>
 <%--Inherits="System.Web.Mvc.ViewPage<AskueCtp.Entities.PayGroup>"--%>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Мониторинг
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<div id="monitoring-menu">
    <%for (int i = 0; i < Model.Count; i++)
      { %>
      <%= Html.ActionLink(Model[i].Name, "total", new { id = Model[i].Id }) %>
      &nbsp;
      (<%= Html.ActionLink("сокращенный вид", "totalcompact", new { id = Model[i].Id }) %>)
      <p />
    <% } %>

<%--    <%= Html.ActionLink("Суммарное потребление", "total") %>
    <p />
    <%= Html.ActionLink("Суммарное потребление (только цифра)", "totalcompact") %>--%>
</div>

</asp:Content>

