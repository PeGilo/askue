﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>

<script type="text/javascript">
    $(document).ready(function () {
        //var d2 = [[0, 3], [4, 8], [8, 5], [9, 13]];
        //$.plot($("#plot-placeholder"), [d2]);


        // Чтобы избежать отображения кэшированного рисунка, добавляем изменяющийся параметр timestamp
        $("#imgPlot").attr("src", '<%= ViewData["ImgUrl"] %>' + '?timestamp=' + (new Date()).getTime());
    });
</script>

<div id="monitoring-plot">
    <% if (ViewData["IsValid"] is bool && ((bool)ViewData["IsValid"] == true))
       {  %>
       <div id="plot-placeholder" style="text-align: center" >
    <% } else { %>
       <div id="plot-placeholder-invalid" style="text-align: center" >
    <% } %>
        <img id="imgPlot" alt="График" src=""  />
    </div>

</div>