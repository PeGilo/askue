﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<AskueCtp.Entities.BusBarCid>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Test
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Test</h2>

    <fieldset>
        <legend>Fields</legend>
        
        <div class="display-label">FeederNumber</div>
        <div class="display-field"><%= Html.Encode(Model.FeederNumber) %></div>
        
        <div class="display-label">ObjectNumber</div>
        <div class="display-field"><%= Html.Encode(Model.ObjectNumber) %></div>
        
        <div class="display-label">SybRnk</div>
        <div class="display-field"><%= Html.Encode(Model.SybRnk) %></div>
        
    </fieldset>
    <p>
        <%= Html.ActionLink("Edit", "Edit", new { /* id=Model.PrimaryKey */ }) %> |
        <%= Html.ActionLink("Back to List", "Index") %>
    </p>

</asp:Content>

