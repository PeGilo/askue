﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AskueWeb.ViewModel.Roller>" %>

    <script type="text/javascript">

        var sliding = true;

        $(document).ready(function () {
            if (sliding) {
                $("#loading-mask").fadeOut(<%= Model.FadeOutInterval %>);
                

                // set timer
                setTimeout(function () {
                    pageRedirect();
                    //setTimeout(arguments.callee, 10000);
                }, <%= Model.ShowInterval %>);
            }
        });

        function pageRedirect() {
            $("#loading-mask").fadeIn(<%= Model.FadeInInterval %>);

            setTimeout(function () {

                window.location.href = "<%= Model.NextUrl %>";

            }, <%= Model.FadeInInterval %>);
        }
    </script>

<!-- Menu -->
<% foreach (AskueWeb.ViewModel.RollerItem item in Model.Items)
    {%>
    <a href="<%= item.Url %>"><%= item.Name %></a>
    &nbsp;
<% } %>
    

