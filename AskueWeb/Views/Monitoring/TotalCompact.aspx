﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/TotalNested.Master" 
Inherits="System.Web.Mvc.ViewPage<AskueWeb.ViewModel.Total>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TotalTitleContent" runat="server">
	Суммарная потребляемая мощность
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="TotalHeaderRightContent" runat="server">
    <% Html.RenderPartial("CounterWarnings", Model.CounterWarnings); %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="TotalMainContent" runat="server">


<div id="totalvalue-container">
    <table style="width:100%">
        <tr>
            <td>
                <div class="label-date">
                    Дата
                </div>
            </td>
            <td class="label_right">
            </td>
            <td class="small-separator">
            </td>
            <td class="label_left">
            </td>
            <td>
                <div class="label-time">
                    Время измерения (местное)
                </div>
            </td>
            <td class="label_right">
            </td>
            <td class="small-separator">
            </td>
            <td class="label_left">
            </td>
            <td>
                <div class="label-time">
                    Суммарная мощность
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div class="date">
                    <% if (Model.IsValid) { %>
                    <% } else { %>
                    <% } %>

                    <% if (Model.HasMeasurements)
                       { %>
                        <%=  Html.Encode(Model.TimeStart.ToString("d MMM yyyy", Model.CurrentCulture))%>
                    <% } %>
                </div>
            </td>
            <td>
            </td>

            <td colspan="3">
                <div class="time">
                    <% if (Model.IsValid) { %>
                    <% } else { %>
                    <% } %>
                    <% if (Model.HasMeasurements)
                       { %>
                        <%= Html.Encode(Model.TimeStart.ToString("t", Model.CurrentCulture))%>
                        <span> - </span>
                        <%= Html.Encode(Model.TimeEnd.ToString("t", Model.CurrentCulture))%>
                    <% } %>
                </div>
            </td>
            <td>
            </td>

            <td></td>
            <td>
                
                <div class="value">
                    <%--<%= Html.Encode(String.Format("{0:F0}", Model.TotalValue)) %>
                    <br />--%>
                    <% if (Model.HasMeasurements)
                       { %>
                        <%= Html.Encode(String.Format("{0:#,0}", Model.TotalValue))%><span>КВт</span>
                    <% } %>
                </div>
            </td>
        </tr>
<%--        <tr>
            <td colspan="9">
                <% if (Model.IsValid) { %>
                <% } else { %>
                    <div class="error-total"><span>Данные по одному или нескольким фидерам отсутствуют</span></div>
                <% } %>
            </td>
        </tr>--%>
        <tr>
            <td colspan="9">
                <% if (!Model.HasMeasurements) { %>
                    <table class="error-total">
                        <tr>
                            <td style="padding-right: 10px;">
                                <div class="errorsign"></div>
                            </td>
                            <td>
                                <span>Данные отсутсвуют по всем фидерам</span>
                            </td>
                        </tr>
                    </table>
                <% } else if(Model.HasMissingMeasurements) { %>
                    <table class="error-total">
                        <tr>
                            <td style="padding-right: 10px;">
                                <div class="errorsign"></div>
                            </td>
                            <td>
                                <span>Данные по одному или нескольким фидерам отсутствуют</span>
                            </td>
                        </tr>
                    </table>
                <% } %>
            </td>
        </tr>

    </table>
    

    
</div>


</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="TotalFooterContentRight" runat="server">
</asp:Content>

