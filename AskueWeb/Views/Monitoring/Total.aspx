﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/TotalNested.Master" 
Inherits="System.Web.Mvc.ViewPage<AskueWeb.ViewModel.TotalExtended>" %>

<asp:Content ID="Content4" ContentPlaceHolderID="TotalHeaderRightContent" runat="server">
    <% Html.RenderPartial("CounterWarnings", Model.Total.CounterWarnings); %>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="TotalTitleContent" runat="server">
	Суммарная потребляемая мощность
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="TotalMainContent" runat="server">

<%--<%= Html.Encode(ViewData["Message"]) %>--%>
    <div id="monitoring-column-container">
        <div id="monitoring-column-left">
            <%--<% Html.RenderAction("TotalValue", new { id = (int?)ViewData["RegPointId"] }); %>--%>
            <% Html.RenderPartial("TotalValue", Model.Total); %>
            <% Html.RenderAction("Chart", new { id = (int?)ViewData["RegPointId"] }); %>
        </div>

    </div>
    <div id="monitoring-column-right">
        <%--<% Html.RenderAction("CounterList", new { id = (int?)ViewData["RegPointId"] }); %>--%>
        <% Html.RenderPartial("CounterList", Model.CounterConsumptions); %>
    </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="TotalFooterContentRight" runat="server">

</asp:Content>