﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IList<AskueWeb.ViewModel.CounterConsumption>>" %>
<div id="counterlist-container">
    <div>
        <table style="width:100%">
            <tr>
                <td class="td_lft"></td>
                <td><h3>Мощность по фидерам</h3></td>
            </tr>
        </table>
    </div>
    <% if(Model.Count > 0) { %>
        <table >
            <tr>
                <th>
                    <div class="counter-header">
                        <div class="name">&nbsp;</div>
                        <div class="time">Время</div>
                        <div class="consumption">Мощность, КВт</div>
                    </div>
                </th>
                <th>
                    <div class="counter-header">
                        <div class="name">&nbsp;</div>
                        <div class="time">Время</div>
                        <div class="consumption">Мощность, КВт</div>
                    </div>
                </th>
            </tr>
            <% for (int i = 0, half = (Model.Count / 2 + Model.Count % 2); i < half; i++)
               { %>
            <tr>
                <td>
                    <%= Html.DisplayFor(m => m[i], "CounterConsumption") %>
                </td>
                <td>
                    <% if (i + half < Model.Count)
                       { %>
                            <%= Html.DisplayFor(m => m[i + half], "CounterConsumption")%>
                    <% } %>
                </td>
            </tr>
            <% } %>
        </table>
    <% } else { %>
        <div class="error-list">
            <span>Данные отсутствуют</span>
        </div>
    <% } %>

</div>
