﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AskueWeb.ViewModel.Total>" %>
<div id="totalvalue-container">
    <table>
        <tr>
            <td>
                <div class="label-date">
                    Дата
                </div>
            </td>
            <td class="label_right">
            </td>
            <td class="small-separator">
            </td>
            <td class="label_left">
            </td>
            <td>
                <div class="label-time">
                    Время измерения (местное)
                </div>
            </td>
            <td class="label_right">
            </td>
            <td class="small-separator">
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div class="date">
                    <% if (Model.IsValid) { %>
                        
                    <% } else { %>
                        
                    <% } %>

                    <% if (Model.HasMeasurements)
                       { %>
                        <%= Html.Encode(Model.TimeStart.ToString("d MMM yyyy", Model.CurrentCulture))%>
                    <% } %>
                </div>
            </td>
            <td>
            </td>

            <td colspan="3">
                <div class="time">
                    <% if (Model.IsValid) { %>

                    <% } else { %>
                        
                    <% } %>

                    <% if (Model.HasMeasurements)
                        { %>
                        <%= Html.Encode(Model.TimeStart.ToString("t", Model.CurrentCulture))%>
                        <span> - </span>
                        <%= Html.Encode(Model.TimeEnd.ToString("t", Model.CurrentCulture))%>
                    <% } %>
                </div>
            </td>
            <td>
            </td>
        </tr>

        <tr>
            <td>
                <div class="label-value">
                    Суммарная мощность
                </div>
            </td>
            <td class="label_right">
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>

        </tr>
    </table>
    
    <div class="value">
        <%--<%= Html.Encode(String.Format("{0:F0}", Model.TotalValue)) %>
        <br />--%>
        <% if (Model.HasMeasurements)
           { %>
                <%= Html.Encode(String.Format("{0:#,0}", Model.TotalValue))%><span>КВт</span>
        <% } %>
    </div>

    <% if (!Model.HasMeasurements) { %>
        <table class="error-total">
            <tr>
                <td style="padding-left: 15px">
                    <div class="errorsign"></div>
                </td>
                <td>
                    <span>Данные отсутсвуют по всем фидерам</span>
                </td>
            </tr>
        </table>
    <% } else if(Model.HasMissingMeasurements) { %>
        <table class="error-total">
            <tr>
                <td style="padding-left: 15px">
                    <div class="errorsign"></div>
                </td>
                <td>
                    <span>Данные по одному или нескольким фидерам отсутствуют</span>
                </td>
            </tr>
        </table>
    <% } %>
    
</div>
