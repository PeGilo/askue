﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<AskueWeb.ViewModel.CounterWarning>>" %>

<div id="counterwarnings_container">
    <% foreach (var item in Model) { %>
    
        <div class="counterwarning">
            <div class="icon">!</div>
            <div class="message"><%= Html.Encode(String.Format("{0:g}", item.Message)) %>&nbsp;</div>
            <div class="name"><%= Html.Encode(String.Format("{0:g}", item.Name)) %></div>
        </div>
    
    <% } %>
</div>

