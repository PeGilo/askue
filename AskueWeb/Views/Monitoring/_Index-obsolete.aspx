﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="indexTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Мониторинг
</asp:Content>

<asp:Content ID="indexContent" ContentPlaceHolderID="MainContent" runat="server">

    <script type="text/javascript">
        var sheets_win = null;

        var Tree = Ext.tree;
        // TREE INIT
        var sheetTree = new Tree.TreePanel({
            el: 'sheet-tree',
            //animate: true,
            autoScroll: true,
            loader: new Tree.TreeLoader({ dataUrl: 'get-nodes.aspx' }),
            //containerScroll: true,
            //border: false
        });

        // add a tree sorter in folder mode
        new Tree.TreeSorter(sheetTree, { folderSort: true });

        // set the root node
        var root = new Tree.AsyncTreeNode({
            text: 'Ext JS',
            draggable: false, // disable root node dragging
            id: 'root'
        });
        sheetTree.setRootNode(root);

        // render the tree
        //tree.render();

        root.expand(false, /*no anim*/false);

        // BUTTON HANDLER
        Askue.mainToolbar.AddSheetHandler = function(button, e) {
            if (!sheets_win) {
                sheets_win = new Ext.Window({
                    el: 'sheet-win',
                    layout: 'fit',
                    width: 500,
                    height: 300,
                    closeAction: 'hide',
                    plain: true,
                    modal: true,

                    items: new Ext.Panel({
//                        el: 'sheet-panel',
                        layout: 'border',
                        items:
                            [
                                new Ext.Panel({
                                    region: 'west',
                                    //layout: 'fit',
                                    split: true,
                                    items: [sheetTree]
                                }),
                                new Ext.Panel({
                                    region: 'center',
                                    html: 'форма'
                                })                                
                            ]
                    }),

                    buttons: [{
                        text: 'Submit',
                        disabled: true
                    }, {
                        text: 'Close',
                        handler: function() {
                        sheets_win.hide();
                        }
                    }]
                });
            }
            sheets_win.show(this);
        };
    
        Ext.onReady(function(){
        
            /////////////////////////////////////////////////////////////////////////////////////////
            // Local Panel
            /////////////////////////////////////////////////////////////////////////////////////////
            var localPanel = new Ext.Panel({
                region: 'center',
                layout:'border',
                items:[
                    {region: 'west', html: 'west'}, 
                    {region: 'center', html: 'center'}
//                    {
//                        region:'west',
//                        id:'west-panel',
//                        title:'West',
//                        split:true,
//                        width: 200,
//                        minSize: 175,
//                        maxSize: 400,
//                        collapsible: true,
//                        margins:'0 0 0 5',
//                        layout:'accordion',
//                        layoutConfig:{
//                            animate:true
//                        },
//                        items: [{
//                            contentEl: 'divAux',
//                            title:'Aux',
//                            border:false,
//                            iconCls:'nav'
//                        },{
//                            contentEl: 'divSettings2',
//                            title:'Settings',
//                            border:false,
//                            iconCls:'settings'
//                        }]
//                    },
//                    {
//                        region:'east',
//                        id:'east-panel',
//                        title:'East',
//                        split:true,
//                        width: 200,
//                        minSize: 175,
//                        maxSize: 400,
//                        collapsible: true,
//                        margins:'0 0 0 5',
//                        layout:'fit',
//                        layoutConfig:{
//                            animate:true
//                        },
//                        items:[{
//                            contentEl: 'divColumnsSelection',
//                            title: 'Columns',
//                            autoScroll:true
//                        }]
//                    },
//                    tabPanel                 
                ]});
              
              // substitute content page from Master page 
              glContentPanel = localPanel;     
        });
    </script>

    <div id="sheet-win">
    </div>
    
    <div id="sheet-panel">
        дерево
    </div>
    
    <div id="sheet-tree">
    </div>
</asp:Content>
