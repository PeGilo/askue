﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using AskueCtp.Web;
using AskueCtp.Web.Base;

namespace AskueCtp.Controllers
{
    /// <summary>
    /// Summary description for UserInfoController
    /// </summary>
    public class UserInfoController : BaseController
    {
        public ActionResult Index()
        {
            ViewData["UserName"] = CurrentUser.Name;

            string role;
            if (CurrentUser.Role == Role.Admin)
                role = "Administrator";
            else if (CurrentUser.Role == Role.AdvancedUser)
                role = "Advanced user";
            else if (CurrentUser.Role == Role.User)
                role = "Simple user";
            else
                role = "Guest";

            ViewData["UserRole"] = role;

            return View();
        }
    }
}