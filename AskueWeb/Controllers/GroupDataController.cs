﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace AskueCtp.Controllers
{
    /// <summary>
    /// Summary description for GroupDataController
    /// </summary>
    public class GroupDataController : Controller
    {
        public GroupDataController()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public ActionResult Index()
        {
            ViewData["Xml"] = "<?xml version=\"1.0\"?><root><child>"
                //+ groupId.ToString() + date.ToString()
                + "</child></root>";
            return View();
        }

        public ActionResult Load(int id, DateTime date)
        {
            ViewData["Xml"] = "<?xml version=\"1.0\"?><root><child>"
                + "id: " + id.ToString() 
                + " " + date.ToString()
                + "</child></root>";

            return View("ResponseXml");
        }
    }
}