﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using AskueCtp.Web;
using AskueCtp.Web.Base;
using AskueCtp.Code.Logging;
using AskueCtp.Common;
using AskueCtp.Common.Collections;
using AskueCtp.Common.Graphics;
using AskueCtp.Config;
using AskueCtp.DataAccess;
using AskueCtp.DataAccess.Impl;
using AskueCtp.Entities;
using AskueCtp.ServiceLayer;
using AskueCtp.ServiceLayer.Impl;
using AskueCtp.ServiceLayer.Enums;

using AskueWeb.ViewModel;
using AskueWeb.Code.MvcHelpers;

namespace AskueCtp.Controllers
{
    /// <summary>
    /// Summary description for MonitoringController
    /// </summary>
    public partial class MonitoringController : BaseController
    {
        private const int INTERV_MIN = 5; // interval of measurements in minutes
        private const int PREC = 3; // precision in secondes
        private const int CHART_WIDTH = 500;
        private const int CHART_HEIGHT = 237;
        private const int K_PERIOD = 3; // коэффициент на который домножается период опроса для определения
                                        // допустимого интервала времени
        private const int MAX_COUNT_WARNINGS = 3;

        private static readonly DateTime fixedTime = new DateTime(2011, 12, 29, 15, 20, 0);
        //new DateTime(2011, 08, 24, 07, 50, 0);
        private static readonly PayGroupCid fixedGroupId = new PayGroupCid(3, 2, 1);

        private static readonly System.Object _syncChart = new System.Object();

        /// <summary>
        /// Stores the date of generated chart
        /// </summary>
        private DateTime GetChartEndDate(string regPointId)
        {
            string key = "ChartEndDate" + regPointId;
            return HttpContext.Application[key] != null ? (DateTime)HttpContext.Application[key] : DateTime.MinValue;
        }

        private void SetChartEndDate(string regPointId, DateTime value)
        {
            HttpContext.Application["ChartEndDate" + regPointId] = value;
        }

        protected virtual DateTime CurrentDateTime
        {
            get { 
                return DateTime.Now;
                //return fixedTime;
            }
        }

        public ActionResult Index()
        {
            IList<RegistrationPointElement> regPoints = Environment.GetRegPoints();
            IList<RollerItem> menuItems = (from p in regPoints
                                           select
                                           new RollerItem(p.Name, "", p.Id)).ToList();

            return View(menuItems);
        }

        public ActionResult Roller(string redirect, string currentId)
        {
            IList<RegistrationPointElement> regPoints = Environment.GetRegPoints();

            Roller roll = new Roller();
            roll.Items = (from p in regPoints
                          select
                              new RollerItem(p.Name, Url.Action(redirect, "Monitoring", new { id = p.Id, slide=1 }), p.Id)).ToList();

            RollerItem nextItem = roll.Items.FindNextAfterOrFirst<RollerItem>(i => i.Id == currentId);

            roll.NextUrl = nextItem != null ? Url.Action(redirect, "Monitoring", new { id = nextItem.Id, slide=1 }) : String.Empty;
            //roll.Slide = slideValue;

            roll.FadeInInterval = Environment.FadeInInterval;
            roll.FadeOutInterval = Environment.FadeOutInterval;
            roll.ShowInterval = Environment.ShowInterval + Environment.FadeInInterval;

            return View(roll);
        }

        [ActionTimerAttribute]
        public ActionResult Total(int? id, int? slide)
        {
            try
            {
                return TotalImpl(id, slide);
            }
            catch (ArgumentException ex)
            {
                return View("Error", new Error(ex.Message));
            }
            catch (Exception ex)
            {
                Logger.Error("Can't get total data", ex);

                return View("Error", new Error("Произошла ошибка во время чтения данных."));
            }
        }

        private ActionResult TotalImpl(int? id, int? slide)
        {
            if (id.HasValue)
            {
                RegistrationPointElement regPoint = Environment.GetRegPointConfig(id.Value);
                if (regPoint != null)
                {
                    ViewData["Message"] = this.CurrentUser.Name + " " + this.CurrentUser.Role.ToString();
                    ViewData["RegPointId"] = id;
                    ViewData["RegPointName"] = regPoint.Name;
                    ViewData["BackgroundColorPrimary"] = regPoint.DisplayLayout.BackgroundColorPrimary;
                    ViewData["BackgroundColorSecondary"] = regPoint.DisplayLayout.BackgroundColorSecondary;
                    ViewData["TextColorPrimary"] = regPoint.DisplayLayout.TextColorPrimary;
                    ViewData["TextColorSecondary"] = regPoint.DisplayLayout.TextColorSecondary;
                    ViewData["CurrentAction"] = "total";
                    ViewData["CurrentId"] = id.Value.ToString();
                    ViewData["Slide"] = slide.HasValue ? slide.Value : 0;

                    //
                    TotalExtended model = new TotalExtended();

                    IList<SetiMeasurement> measures = GetSetiMeasurements(regPoint);

                    model.Total = GetCurrentTotalValue(regPoint);

                    model.Total.CounterWarnings = BuildCounterWarningsList(measures);

                    model.CounterConsumptions = BuildCounterConsumptionList(measures);

                    //

                    return View("Total", model);
                }
                else
                {
                    return View("Error", new Error("Объект с таким идентификатором не найден."));
                }
            }
            else
            {
                return View("Error", new Error("Не указан идентификатор объекта"));
            }
        }

        [ActionTimerAttribute]
        public ActionResult TotalCompact(int? id, int? slide)
        {
            try
            {
                return TotalCompactImpl(id, slide);
            }
            catch (ArgumentException ex)
            {
                return View("Error", new Error(ex.Message));
            }
            catch (Exception ex)
            {
                Logger.Error("Can't get total value", ex);

                return View("Error", new Error("Произошла ошибка во время чтения данных."));
            }
        }

        private ActionResult TotalCompactImpl(int? id, int? slide)
        {
            if (id.HasValue)
            {
                RegistrationPointElement regPoint = Environment.GetRegPointConfig(id.Value);
                if (regPoint != null)
                {
                    Total t = GetCurrentTotalValue(regPoint);
                    ViewData["RegPointName"] = regPoint.Name;
                    ViewData["BackgroundColorPrimary"] = regPoint.DisplayLayout.BackgroundColorPrimary;
                    ViewData["BackgroundColorSecondary"] = regPoint.DisplayLayout.BackgroundColorSecondary;
                    ViewData["TextColorPrimary"] = regPoint.DisplayLayout.TextColorPrimary;
                    ViewData["TextColorSecondary"] = regPoint.DisplayLayout.TextColorSecondary;
                    ViewData["CurrentAction"] = "totalcompact";
                    ViewData["CurrentId"] = id.Value.ToString();
                    ViewData["Slide"] = slide.HasValue ? slide.Value : 0;

                    IList<SetiMeasurement> measures = GetSetiMeasurements(regPoint);
                    t.CounterWarnings = BuildCounterWarningsList(measures);

                    return View(t);
                }
                else
                {
                    return View("Error", new Error("Объект с таким идентификатором не найден."));
                }
            }
            else
            {
                return View("Error", new Error("Не указан идентификатор объекта"));
            }
        }

        protected virtual Total GetCurrentTotalValue(RegistrationPointElement regPoint)
        {
            if (regPoint != null)
            {
                DateTime time = CurrentDateTime;
                DateTime timeStart = time.AddMinutes(-K_PERIOD * regPoint.ReadingPeriod);
                DateTime timeEnd = time.AddMinutes(K_PERIOD * regPoint.ReadingPeriod);

                PayGroupCid groupId = new PayGroupCid(regPoint.Cid.SYB_RNK, regPoint.Cid.N_OB, regPoint.Cid.N_GR);

                ISessionBuilder sessionBuilder = SessionBuilderFactory.GetSessionBuilder(DBSessionType.RODataStore, this);

                IFirstLevelPayGroupRepository groupRep = new FirstLevelPayGroupRepository(sessionBuilder);
                ISetiRepository setiRep = new SetiRepository(sessionBuilder);
                IMonitoringService monitorService = new MonitoringService(groupRep, setiRep, this.Cache);

                DateTime start, end;
                MeasureQuality quality;
                double totalValue = monitorService.GetTotalConsumption(groupId,
                    timeStart, timeEnd, 
                    out start, out end, out quality);
                return new Total(start, end, totalValue / 1000.0, // Переводится в КВт
                    (quality & MeasureQuality.MissingMeasures) == MeasureQuality.MissingMeasures,
                    (quality & MeasureQuality.NoMeasures) != MeasureQuality.NoMeasures,
                    true);
            }
            else
            {
                throw new ArgumentNullException("regPoint");
            }
        }
        

        private IList<SetiMeasurement> GetSetiMeasurements(RegistrationPointElement regPoint)
        {
            if (regPoint == null || regPoint.Cid == null)
            {
                throw new ArgumentNullException();
            }

            PayGroupCid groupId = new PayGroupCid(regPoint.Cid.SYB_RNK, regPoint.Cid.N_OB, regPoint.Cid.N_GR);

            ISessionBuilder sessionBuilder = SessionBuilderFactory.GetSessionBuilder(DBSessionType.RODataStore, this);

            DateTime time = CurrentDateTime;
            DateTime timeStart = time.AddMinutes(-K_PERIOD * regPoint.ReadingPeriod);
            DateTime timeEnd = time.AddMinutes(K_PERIOD * regPoint.ReadingPeriod);

            IFirstLevelPayGroupRepository groupRep = new FirstLevelPayGroupRepository(sessionBuilder);
            ISetiRepository setiRep = new SetiRepository(sessionBuilder);
            //IFirstLevelPayGroupService groupService = new FirstLevelPayGroupService(groupRep, setiRep);
            IMonitoringService monitorService = new MonitoringService(groupRep, setiRep, this.Cache);

            IList<SetiMeasurement> measures = monitorService.GetConsumptionByCounter(groupId, timeStart, timeEnd);

            return measures;
        }

        private List<CounterConsumption> BuildCounterConsumptionList(IList<SetiMeasurement> setiMeasurements)
        {
            ISessionBuilder sessionBuilder = SessionBuilderFactory.GetSessionBuilder(DBSessionType.RODataStore, this);

            IFirstLevelPayGroupRepository groupRep = new FirstLevelPayGroupRepository(sessionBuilder);
            ISetiRepository setiRep = new SetiRepository(sessionBuilder);
            IMonitoringService monitorService = new MonitoringService(groupRep, setiRep, this.Cache);

            IFeederService feederService = new FeederService(new FeederRepository(sessionBuilder, this.Cache));

            List<CounterConsumption> list = new List<CounterConsumption>();
            foreach (SetiMeasurement measure in setiMeasurements)
            {
                CounterConsumption cc = new CounterConsumption();
                cc.Number = measure.Cid.CounterNumber;
                cc.Consumption = measure.FullEnergy / 1000.0; // Перевести в КВт
                cc.Name = feederService.GetNameByCounterNumber(measure.Cid.CounterNumber);
                cc.Time = measure.Cid.Date;
                cc.Warning = monitorService.CheckPhaseFailure(measure);
                list.Add(cc);
            }
            //list.Sort(new Comparison<CounterConsumption>(
            //    (cc1, cc2) => { return cc1.Name.CompareTo(cc2.Name); }
            //    ));
            return list;
        }

        private List<CounterWarning> BuildCounterWarningsList(IList<SetiMeasurement> setiMeasurements)
        {
            ISessionBuilder sessionBuilder = SessionBuilderFactory.GetSessionBuilder(DBSessionType.RODataStore, this);

            IFirstLevelPayGroupRepository groupRep = new FirstLevelPayGroupRepository(sessionBuilder);
            ISetiRepository setiRep = new SetiRepository(sessionBuilder);
            IMonitoringService monitorService = new MonitoringService(groupRep, setiRep, this.Cache);

            IFeederService feederService = new FeederService(new FeederRepository(sessionBuilder, this.Cache));

            List<CounterWarning> list = new List<CounterWarning>();
            foreach (SetiMeasurement measure in setiMeasurements)
            {
                if (monitorService.CheckPhaseFailure(measure))
                {
                    CounterWarning cc = new CounterWarning();
                    cc.Message = "Неполнофазный режим";
                    cc.Name = feederService.GetNameByCounterNumber(measure.Cid.CounterNumber);

                    list.Add(cc);
                }
                // Добавлять не более 3-х предупреждений
                if (list.Count >= MAX_COUNT_WARNINGS)
                {
                    break;
                }
            }

            return list;
        }

        public ActionResult Chart(int? id)
        {
            if (id.HasValue)
            {
                RegistrationPointElement regPoint = Environment.GetRegPointConfig(id.Value);
                if (regPoint != null)
                {
                    try
                    {
                        bool isValid;
                        string fileName = BuildChart(regPoint, out isValid);

                        ViewData["ImgUrl"] = Path.Combine(Environment.ChartFolderWebPath, fileName).Replace("\\", "/");

                        ViewData["IsValid"] = isValid;

                        return PartialView();
                    }
                    catch(Exception ex)
                    {
                        Logger.Error("Can't get chart: ", ex);

                        return PartialView("ErrorPartial", new Error(ex.Message));
                    }
                }
                else
                {
                    return PartialView("ErrorPartial", new Error("Объект с таким идентификатором не найден."));
                }
            }
            else
            {
                return PartialView("ErrorPartial", new Error("Не указан идентификатор объекта"));
            }
        }


        private string BuildChart(RegistrationPointElement regPoint, out bool isValid)
        {
            DateTime latestDate;
            string fileName = "plot" + regPoint.Id + ".gif";

            PayGroupCid groupId = new PayGroupCid(regPoint.Cid.SYB_RNK, regPoint.Cid.N_OB, regPoint.Cid.N_GR);

            List<DTVPoint> points = ComposePointsList(regPoint, groupId, CurrentDateTime, out latestDate, out isValid);

            // Отфильтровать точки, выходящие за границы текущих суток
            List<DTVPoint> filteredPoints = (from p in points 
                                             where p.Time.Day == CurrentDateTime.Day
                                             select p).ToList();

            lock (_syncChart)
            {
                DateTime chartEndDate = GetChartEndDate(regPoint.Id);
                // Check if chart already exists for this time
                if (chartEndDate < latestDate)
                {
                    DrawChartToFile(filteredPoints, fileName, regPoint);
                    SetChartEndDate(regPoint.Id, latestDate);

                    Logger.Debug("Chart file is updated.");
                }
                else
                {
                    Logger.Error("Chart file already exists. ChartEndDate=" + chartEndDate.ToString(), null);
                }
            }

            return fileName;
        }

        private List<DTVPoint> ComposePointsList(RegistrationPointElement regPoint, PayGroupCid groupId, DateTime time, 
            out DateTime latestDate, out bool isValid)
        {
            ISessionBuilder sessionBuilder = SessionBuilderFactory.GetSessionBuilder(DBSessionType.RODataStore, this);

            IFirstLevelPayGroupRepository groupRep = new FirstLevelPayGroupRepository(sessionBuilder);
            ISetiRepository setiRep = new SetiRepository(sessionBuilder);
            IMonitoringService monitorService = new MonitoringService(groupRep, setiRep, this.Cache);

            List<DTVPoint> points = new List<DTVPoint>();
            DateTime chartEnd = time.AddSeconds(PREC);
            DateTime chartStart = new DateTime(time.Year, time.Month, time.Day, 0, 0, 0);
            DateTime intervalStart = chartStart.AddSeconds(-PREC);
            DateTime intervalEnd = intervalStart.AddSeconds(INTERV_MIN * 60 + 2 * PREC);
            DateTime start, end;
            latestDate = DateTime.MinValue;
            double totalValue = 0;
            isValid = true;

            while (intervalEnd <= chartEnd)
            {
                //Logger.Debug("Getting chart point START: [" + intervalStart.ToString() + "] END: [" + intervalEnd.ToString() + "]" );
                try
                {
                    MeasureQuality quality;
                    totalValue = monitorService.GetTotalConsumption(groupId, 
                        intervalStart.AddMinutes(-K_PERIOD * regPoint.ReadingPeriod), intervalEnd, 
                        out start, out end, out quality);

                    // Не учитывать вычисления когда никаких значений не было выбрано вообще
                    if ((quality & MeasureQuality.NoMeasures) != MeasureQuality.NoMeasures)
                    {
                        //points.Add(new DTVPoint(end, totalValue / 1000)); // Перевести в КВт
                        points.Add(new DTVPoint(start, totalValue / 1000)); // Перевести в КВт

                        //if (end > latestDate)
                        //{
                        //    latestDate = end;
                        //}
                        if (start > latestDate)
                        {
                            latestDate = start;
                        }
                        //Logger.Debug("start: [" + start.ToString() + "] end: [" + end.ToString() + "] VALUE: " + (totalValue / 1000).ToString());
                    }
                }
                catch (Exception ex)
                {
                    //points.Add(new DTVPoint(intervalEnd.AddSeconds(-PREC), null));
                    Logger.Error("CHART: Can't get interval total value: ", ex);
                }

                intervalStart = intervalStart.AddSeconds(INTERV_MIN * 60);
                intervalEnd = intervalStart.AddSeconds(INTERV_MIN * 60 + 2 * PREC);
            }
            return points;
        }

        private void DrawChartToFile(List<DTVPoint> points, string fileName, RegistrationPointElement regPoint)
        {
            ChartHelper chart = new ChartHelper(CHART_WIDTH, CHART_HEIGHT, Environment.ChartFolderFullPath,
                //System.Drawing.Color..FromArgb(13, 78, 140)); 
                System.Drawing.ColorTranslator.FromHtml(regPoint.DisplayLayout.TextColorPrimary));
            chart.SaveChart(points, fileName);
        }
    }
}