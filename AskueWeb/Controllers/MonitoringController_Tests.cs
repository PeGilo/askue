﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AskueCtp.Controllers
{
    public partial class MonitoringController
    {
        public ActionResult Test(int id)
        {
            ViewData["Message"] = this.CurrentUser.Name + " " + this.CurrentUser.Role.ToString();
            ViewData["RegPointId"] = 1;
            ViewData["RegPointName"] = "ЭК ТЕСТ";
            ViewData["BackgroundColorPrimary"] = "White";
            ViewData["BackgroundColorSecondary"] = "Gray";
            ViewData["TextColorPrimary"] = "Black";
            ViewData["TextColorSecondary"] = "Black";
            ViewData["CurrentAction"] = "Test";
            ViewData["CurrentId"] = id.ToString();
            ViewData["Slide"] = 0;

            return View("Total");
        }
    }
}