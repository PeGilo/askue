﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Xml.Linq;
using System.Web;

using NHibernate;
using NHibernate.Cfg;

using AskueCtp.Common;
using AskueCtp.DataAccess;

namespace AskueCtp.Web
{
    /// <summary>
    /// Summary description for ACSessionBuilder
    /// </summary>
    public class ACSessionBuilder : BaseSessionBuilder
    {
        private static ISessionFactory _sessionFactory;
        private static ISession _currentSession;

        private IWebContext m_webContext;

        protected override string ConfigFileName
        {
            get { return String.Empty; }
        }

        //protected override string ContextItemKey
        //{
        //    get { return GetType().FullName; }
        //}

        protected override ISessionFactory SessionFactory
        {
            get { return _sessionFactory; }
            set { _sessionFactory = value; }
        }
        protected override ISession CurrentSession
        {
            get { return _currentSession; }
            set { _currentSession = value; }
        }

        public ACSessionBuilder(IWebContext webContext)
        {
            m_webContext = webContext;
        }

        //protected override ISession getExistingOrNewSession(ISessionFactory factory)
        //{
        //    if (HttpContext.Current != null)
        //    {
        //        ISession session = GetExistingWebSession();
        //        if (session == null)
        //        {
        //            session = openSessionAndAddToContext(factory);
        //            Logger.Info("Creating new AC Session (HttpContext+)" + session.GetHashCode());
        //        }
        //        else if (!session.IsOpen)
        //        {
        //            session = openSessionAndAddToContext(factory);
        //        }
        //        return session;
        //    }
        //    if (_currentSession == null)
        //    {
        //        _currentSession = factory.OpenSession();
        //        Logger.Info("Creating new AC Session (HttpContext-)" + _currentSession.GetHashCode());
        //    }
        //    else if (!_currentSession.IsOpen)
        //    {
        //        _currentSession = factory.OpenSession();
        //    }
        //    return _currentSession;
        //}

        //protected override ISessionFactory getSessionFactory()
        //{
        //    if (_sessionFactory == null)
        //    {
        //        NHibernate.Cfg.Configuration configuration = GetConfiguration();
        //        _sessionFactory = configuration.BuildSessionFactory();

        //        Logger.Info("Creating new AC SessionFactory " + _sessionFactory.GetHashCode());
        //    }
        //    return _sessionFactory;
        //}
    }
}