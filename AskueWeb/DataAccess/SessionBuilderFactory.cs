﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using AskueCtp.DataAccess;

namespace AskueCtp.Web
{
    /// <summary>
    /// Summary description for SessionBuilderFactory
    /// </summary>
    public static class SessionBuilderFactory
    {
        public static ISessionBuilder GetSessionBuilder(DBSessionType sessionType, IWebContext webContext)
        {
            if (sessionType == DBSessionType.RODataStore)
                return new ACSessionBuilder(webContext);
            else if (sessionType == DBSessionType.RWDataBase)
                return new RWSessionBuilder(webContext);

            return null;
        }
    }
}