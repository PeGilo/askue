﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

using NHibernate;
using NHibernate.Cfg;

using AskueCtp.Common;
using AskueCtp.Code.Logging;
using AskueCtp.DataAccess;

namespace AskueCtp.Web
{
    /// <summary>
    /// Base class for session builders. Used when session builders should use different config file.
    /// Therefore different session and hibernate session builder.
    /// </summary>
    public abstract class BaseSessionBuilder : ISessionBuilder
    {
        /// <summary>
        /// Name of the file that is used for configuration of hibernate.
        /// </summary>
        /// <remarks>
        /// Should be overrided in derived classes.
        /// </remarks>
        protected virtual string ConfigFileName
        {
            get { return String.Empty; }
        }

        /// <summary>
        /// Key for Context dictionary where stored sessions. Should be different for different databases.
        /// </summary>
        /// <remarks>
        /// Should be overrided in derived classes.
        /// </remarks>
        protected virtual string ContextItemKey
        {
            get { return GetType().FullName; }
        }

        protected abstract ISessionFactory SessionFactory
        {
            get;
            set;
        }
        protected abstract ISession CurrentSession
        {
            get;
            set;
        }

        public virtual Configuration GetConfiguration()
        {
            var configuration = new Configuration();

            if (String.IsNullOrEmpty(ConfigFileName))
                configuration.Configure();
            else
                configuration.Configure(ConfigFileName);

            return configuration;
        }

        /// <summary>
        /// Does not create new session
        /// </summary>
        /// <returns></returns>
        public virtual ISession GetExistingWebSession()
        {
            return HttpContext.Current.Items[ContextItemKey] as ISession;
        }

        /// <summary>
        /// Returns existing or creates new session
        /// </summary>
        /// <returns></returns>
        public ISession GetSession()
        {
            ISessionFactory factory = getSessionFactory();
            ISession session = getExistingOrNewSession(factory);
            Logger.Debug("Using ISession " + session.GetHashCode());
            return session;
        }

        protected virtual ISessionFactory getSessionFactory()
        {
            if (SessionFactory == null)
            {
                NHibernate.Cfg.Configuration configuration = GetConfiguration();
                SessionFactory = configuration.BuildSessionFactory();

                Logger.Debug("Creating new SessionFactory " + SessionFactory.GetHashCode());
            }
            return SessionFactory;
        }

        protected virtual ISession getExistingOrNewSession(ISessionFactory factory)
        {
            if (HttpContext.Current != null)
            {
                ISession session = GetExistingWebSession();
                if (session == null)
                {
                    session = openSessionAndAddToContext(factory);
                    Logger.Debug("Creating new Session (HttpContext+)" + session.GetHashCode());
                }
                else if (!session.IsOpen)
                {
                    session = openSessionAndAddToContext(factory);
                }
                return session;
            }
            if (CurrentSession == null)
            {
                CurrentSession = factory.OpenSession();
                Logger.Debug("Creating new Session (HttpContext-)" + CurrentSession.GetHashCode());
            }
            else if (!CurrentSession.IsOpen)
            {
                CurrentSession = factory.OpenSession();
            }
            return CurrentSession;
        }

        protected virtual ISession openSessionAndAddToContext(ISessionFactory factory)
        {
            ISession session = factory.OpenSession();
            HttpContext.Current.Items.Remove(ContextItemKey);
            HttpContext.Current.Items.Add(ContextItemKey, session);
            return session;
        }
    }

    ///// <summary>
    ///// Using: 
    ///// new HybridSessionBuilder().GetSession(); // creates new or returns existing session
    ///// new HybridSessionBuilder().GetExistingWebSession(); // returns existing session
    ///// </summary>
    //public class HybridSessionBuilder : ISessionBuilder
    //{
    //    private static ISessionFactory _sessionFactory;
    //    private static ISession _currentSession;

    //    public Configuration GetConfiguration()
    //    {
    //        var configuration = new Configuration();
    //        configuration.Configure();
    //        return configuration;
    //    }

    //    /// <summary>
    //    /// Does not create new session
    //    /// </summary>
    //    /// <returns></returns>
    //    public ISession GetExistingWebSession()
    //    {
    //        return HttpContext.Current.Items[GetType().FullName] as ISession;
    //    }

    //    /// <summary>
    //    /// Returns existing or creates new session
    //    /// </summary>
    //    /// <returns></returns>
    //    public ISession GetSession()
    //    {
    //        ISessionFactory factory = getSessionFactory();
    //        ISession session = getExistingOrNewSession(factory);
    //        Logger.Info("Using ISession " + session.GetHashCode());
    //        return session;
    //    }

    //    private ISession getExistingOrNewSession(ISessionFactory factory)
    //    {
    //        if (HttpContext.Current != null)
    //        {
    //            ISession session = GetExistingWebSession();
    //            if (session == null)
    //            {
    //                session = openSessionAndAddToContext(factory);
    //                Logger.Info("Creating new Session (HttpContext+)" + session.GetHashCode());
    //            }
    //            else if (!session.IsOpen)
    //            {
    //                session = openSessionAndAddToContext(factory);
    //            }
    //            return session;
    //        }
    //        if (_currentSession == null)
    //        {
    //            _currentSession = factory.OpenSession();
    //            Logger.Info("Creating new Session (HttpContext-)" + _currentSession.GetHashCode());
    //        }
    //        else if (!_currentSession.IsOpen)
    //        {
    //            _currentSession = factory.OpenSession();
    //        }
    //        return _currentSession;
    //    }

    //    private ISessionFactory getSessionFactory()
    //    {
    //        if (_sessionFactory == null)
    //        {
    //            Configuration configuration = GetConfiguration();
    //            _sessionFactory = configuration.BuildSessionFactory();

    //            Logger.Info("Creating new SessionFactory " + _sessionFactory.GetHashCode());
    //        }
    //        return _sessionFactory;
    //    }

    //    private ISession openSessionAndAddToContext(ISessionFactory factory)
    //    {
    //        ISession session = factory.OpenSession();
    //        HttpContext.Current.Items.Remove(GetType().FullName);
    //        HttpContext.Current.Items.Add(GetType().FullName, session);
    //        return session;
    //    }
    //}
}
