﻿using System;
using System.Data;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using NHibernate;
using NHibernate.Cfg;

using AskueCtp.Common;
using AskueCtp.DataAccess;

namespace AskueCtp.Web
{
    /// <summary>
    /// Summary description for RWSessionBuilder
    /// </summary>
    public class RWSessionBuilder : BaseSessionBuilder
    {
        private static ISessionFactory _sessionFactory;
        private static ISession _currentSession;

        private IWebContext m_webContext;

        protected override string ConfigFileName
        {
            get {
                string appRootFolder = m_webContext.PhysicalApplicationPath;
                return Path.Combine(appRootFolder, "NHibernate.cfg.xml");
            }
        }

        //protected override string ContextItemKey
        //{
        //    get { return GetType().FullName; }
        //}

        protected override ISessionFactory SessionFactory
        {
            get { return _sessionFactory; }
            set { _sessionFactory = value; }
        }
        protected override ISession CurrentSession
        {
            get { return _currentSession; }
            set { _currentSession = value; }
        }

        public RWSessionBuilder(IWebContext webContext)
        {
            m_webContext = webContext;
        }

        //protected override ISession getExistingOrNewSession(ISessionFactory factory)
        //{
        //    if (HttpContext.Current != null)
        //    {
        //        ISession session = GetExistingWebSession();
        //        if (session == null)
        //        {
        //            session = openSessionAndAddToContext(factory);
        //            Logger.Info("Creating new RW Session (HttpContext+)" + session.GetHashCode());
        //        }
        //        else if (!session.IsOpen)
        //        {
        //            session = openSessionAndAddToContext(factory);
        //        }
        //        return session;
        //    }
        //    if (_currentSession == null)
        //    {
        //        _currentSession = factory.OpenSession();
        //        Logger.Info("Creating new RW Session (HttpContext-)" + _currentSession.GetHashCode());
        //    }
        //    else if (!_currentSession.IsOpen)
        //    {
        //        _currentSession = factory.OpenSession();
        //    }
        //    return _currentSession;
        //}

        //protected override ISessionFactory getSessionFactory()
        //{
        //    if (_sessionFactory == null)
        //    {
        //        NHibernate.Cfg.Configuration configuration = GetConfiguration();
        //        _sessionFactory = configuration.BuildSessionFactory();

        //        Logger.Info("Creating new RW SessionFactory " + _sessionFactory.GetHashCode());
        //    }
        //    return _sessionFactory;
        //}
    }
}