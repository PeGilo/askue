﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AskueCtp.Web
{
    public interface IUserSettings
    {
        WindowSet WindowSet { get; }
        void Save();
    }
}
