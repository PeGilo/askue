﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AskueCtp.Web
{
    [Serializable]
    public class Window
    {
        private string m_id;

        public Window(string id)
        {
            m_id = id;
        }

        public string Id
        {
            get { return m_id; }
        }
    }
}
