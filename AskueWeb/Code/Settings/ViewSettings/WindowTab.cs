﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace AskueCtp.Web
{
    [Serializable]
    public class WindowTab : ISerializable
    {
        [NonSerialized]
        private TabAlignment m_TabAlignment = TabAlignment.Horizontal;

        [NonSerialized]
        private List<Window> m_Windows;

        public TabAlignment TabAlignment
        {
            get { return TabAlignment.Horizontal; }
            set { m_TabAlignment = value; }
        }

        public IList<Window> Windows
        {
            get { return m_Windows; }
        }

        public WindowTab()
        {
            m_Windows = new List<Window>();
        }

        /// <summary>
        /// Deserialization
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public WindowTab(SerializationInfo info, StreamingContext context)
        {
            Window[] windowArray = info.GetValue("window_array", typeof(Window[])) as Window[];
            m_Windows = new List<Window>(windowArray);
        }

        /// <summary>
        /// Serialization
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            // get array
            info.AddValue("window_array", m_Windows.ToArray(), typeof(Window[]));
        }
    }
}
