﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace AskueCtp.Web
{
    [Serializable]
    public class WindowSet : ISerializable
    {
        [NonSerialized]
        private List<WindowTab> m_WindowTabs;

        public IList<WindowTab> WindowTabs
        {
            get { return m_WindowTabs; }
        }

        public WindowSet()
        {
            m_WindowTabs = new List<WindowTab>();
        }

        /// <summary>
        /// Deserialization
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public WindowSet(SerializationInfo info, StreamingContext context)
        {
            WindowTab[] tabArray = info.GetValue("tab_array", typeof(WindowTab[])) as WindowTab[];
            m_WindowTabs = new List<WindowTab>(tabArray);
        }

        /// <summary>
        /// Serialization
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            //Console.WriteLine("serialization");
            // get array
            info.AddValue("tab_array", m_WindowTabs.ToArray(), typeof(WindowTab[]));
        }
    }
}
