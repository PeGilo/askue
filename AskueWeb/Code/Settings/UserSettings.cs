﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using AskueCtp.ServiceLayer.Entities;
using AskueCtp.Web.Base;
using AskueCtp.ServiceLayer;

namespace AskueCtp.Web
{
    public class UserSettings : SettingsBase, IUserSettings
    {
        private const string WINDOWSET_PREF = "WindowSetPreference";

        private IPreferenceService m_preferenceService = null;
        private IUser m_user;
        private WindowSet m_WindowSet = null;

        public UserSettings(IUser user, IPreferenceService preferenceService)
        {
            m_user = user;
            m_preferenceService = preferenceService;
        }

        public WindowSet WindowSet
        {
            get
            {
                // If there is need to read the setting from the base
                if (m_WindowSet == null)
                {
                    // trying to read setting from the base
                    PreferenceCid cid = new PreferenceCid(WINDOWSET_PREF, m_user.Name);
                    Preference p = m_preferenceService.GetById(cid) as Preference;

                    if (p != null)
                    {
                        WindowSet ws = Deserialize(p.Value) as WindowSet;
                        if (ws != null)
                        {
                            m_WindowSet = ws;
                        }
                        else
                        {
                            m_WindowSet = new WindowSet();
                        }
                    }
                    else
                    {
                        m_WindowSet = new WindowSet();
                    }
                }

                return m_WindowSet;
            }
        }

        public void Save()
        {
            PreferenceCid cid = new PreferenceCid(WINDOWSET_PREF, m_user.Name);
            Preference p = new Preference(cid, Serialize(m_WindowSet));

            m_preferenceService.Save(p);
        }
    }
}
