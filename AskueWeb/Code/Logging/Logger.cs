﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using log4net;
using log4net.Config;

namespace AskueCtp.Code.Logging
{
    public static class Logger
    {
        private static bool m_isInitialized = false;
        private static ILog m_log;

        private static ILog getLogger()
        {
            lock (typeof(Logger))
            {
                ensureInitialized();
                if (m_log == null)
                {
                    m_log = LogManager.GetLogger("Default");
                }
                
            }
            return m_log;
        }

        private static void ensureInitialized()
        {
            if (!m_isInitialized)
            {
                initialize();
                m_isInitialized = true;
            }
        }

        private static void initialize()
        {
            System.IO.FileInfo fi = new System.IO.FileInfo(
            System.Web.HttpContext.Current.Server.MapPath("~/web.config"));
            XmlConfigurator.Configure(fi);
        }

        public static void Debug(object message)
        {
            getLogger().Debug(message);
        }

        public static void Info(object message)
        {
            getLogger().Info(message);
        }

        public static void Error(string message, Exception ex)
        {
            ILog logWriter = getLogger();

            StringBuilder fullMessage = new StringBuilder(message);
            if (ex != null)
            {
                fullMessage.Append(". \nMessage: " + ex.Message + ". \nSource: " + ex.Source + ". \nStacktrace: " + ex.StackTrace);
                if (ex.InnerException != null)
                {
                    fullMessage.Append(". \nInnerException: \nMessage: " + ex.InnerException.Message
                        + ". \nSource: " + ex.InnerException.Source
                        + ". \nStackTrace: " + ex.InnerException.StackTrace);
                }
            }
            logWriter.Error(fullMessage.ToString() + Environment.NewLine);
        }
    }
}
