﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AskueWeb.ViewModel
{
    public class Error
    {
        public Error()
        {
        }

        public Error(string text)
        {
            Text = text;
        }

        public String Text { get; set; }
    }
}