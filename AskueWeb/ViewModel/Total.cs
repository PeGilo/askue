﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using System.Web;

namespace AskueWeb.ViewModel
{
    public class Total : ICloneable
    {
        private IList<CounterWarning> _counterWarnings;

        public Total(DateTime start, DateTime end, double totalValue,
            bool hasMissingMeasurements, bool hasMeasurements, bool isValid)
        {
            TimeStart = start;
            TimeEnd = end;
            TotalValue = totalValue;
            HasMissingMeasurements = hasMissingMeasurements;
            HasMeasurements = hasMeasurements;
            IsValid = isValid;

            _counterWarnings = new List<CounterWarning>();
        }

        public Total(DateTime start, DateTime end, double totalValue,
            bool hasMissingMeasurements, bool hasMeasurements, bool isValid,
            IList<CounterWarning> counterWarnings)
        {
            TimeStart = start;
            TimeEnd = end;
            TotalValue = totalValue;
            HasMissingMeasurements = hasMissingMeasurements;
            HasMeasurements = hasMeasurements;
            IsValid = isValid;

            _counterWarnings = new List<CounterWarning>(counterWarnings);
        }

        public DateTime TimeStart
        {
            get; 
            private set;
        }

        public DateTime TimeEnd
        {
            get;
            private set;
        }

        public double TotalValue
        {
            get; 
            private set;
        }

        public bool IsValid
        {
            get;
            private set;
        }

        public bool HasMissingMeasurements
        {
            get;
            private set;
        }

        public bool HasMeasurements
        {
            get;
            private set;
        }

        public CultureInfo CurrentCulture
        {
            get { return CultureInfo.CreateSpecificCulture("ru-RU"); }
        }

        public IList<CounterWarning> CounterWarnings
        {
            get { return _counterWarnings; }
            set { _counterWarnings = value; }
        }

        /// <summary>
        /// Creates a deep copy.
        /// </summary>
        public Object Clone()
        {
            return new Total(TimeStart, TimeEnd, TotalValue, HasMissingMeasurements, HasMeasurements, IsValid, CounterWarnings);
        }
    }
}