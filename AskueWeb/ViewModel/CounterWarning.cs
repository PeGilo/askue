﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AskueWeb.ViewModel
{
    public class CounterWarning
    {
        public string Name { get; set; }
        public string Message { get; set; }
    }
}