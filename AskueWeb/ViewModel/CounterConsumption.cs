﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AskueWeb.ViewModel
{
    public class CounterConsumption
    {
        public int Number { get; set; }
        public string Name { get; set; }
        public double Consumption { get; set; }
        public DateTime Time { get; set; }
        public bool Warning { get; set; }

        public CounterConsumption()
        {
        }

        public CounterConsumption(int number, string name, double consumption, DateTime time)
        {
            Number = number;
            Name = name;
            Consumption = consumption;
            Time = time;
        }

        public CounterConsumption(int number, string name, double consumption, DateTime time, bool warning)
            : this(number, name, consumption, time)
        {
            Warning = warning;
        }
    }
}