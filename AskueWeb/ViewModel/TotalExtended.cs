﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AskueWeb.ViewModel
{
    public class TotalExtended
    {
        private IList<CounterConsumption> _counterConsumptions;
        

        public TotalExtended()
        {
            _counterConsumptions = new List<CounterConsumption>();
        }

        public TotalExtended(Total total, IList<CounterConsumption> counterConsumptions)
        {
            Total = (Total)total.Clone();
            _counterConsumptions = new List<CounterConsumption>(counterConsumptions);
            
        }

        public Total Total { get; set; }

        public IList<CounterConsumption> CounterConsumptions
        {
            get { return _counterConsumptions; }
            set { _counterConsumptions = value; }
        }


    }
}