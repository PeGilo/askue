﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AskueWeb.ViewModel
{
    public class RollerItem
    {
        public RollerItem() { }
        public RollerItem(string name, string url, string id)
        {
            Name = name;
            Url = url;
            Id = id;
        }

        public string Name { get; set; }
        public string Url { get; set; }
        public string Id { get; set; }
    }

    public class Roller
    {
        public Roller()
        {
            Items = new List<RollerItem>();
        }

        public IEnumerable<RollerItem> Items { get; set; }

        public string NextUrl { get; set; }

        public int Slide { get; set; }

        public int FadeInInterval { get; set; }
        public int FadeOutInterval { get; set; }
        public int ShowInterval { get; set; }
    }
}