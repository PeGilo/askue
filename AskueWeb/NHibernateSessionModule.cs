﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using NHibernate;

using AskueCtp.Common;
using AskueCtp.Code.Logging;

namespace AskueCtp.Web
{
    /// <summary>
    /// Summary description for HNibernateSessionModule
    /// </summary>
    public class NHibernateSessionModule : IHttpModule, IWebContext
    {
        HttpApplication httpApplication = null;

        public NHibernateSessionModule()
        {
        }

        private void context_BeginRequest(object sender, EventArgs e)
        {
            Logger.Debug("[BeginRequest]");

            // Initializing inner variable - it will be used later
            httpApplication = sender as HttpApplication;
        }

        private void context_EndRequest(object sender, EventArgs e)
        {
            Logger.Debug("[EndRequest]");

            // Initializing inner variable - it will be used while getting session builder
            // through IWebContext interface
            httpApplication = sender as HttpApplication;

            // Disposing AC session
            ISession existingWebSession = SessionBuilderFactory.GetSessionBuilder(DBSessionType.RODataStore, this).GetExistingWebSession();
            if (existingWebSession != null)
            {
                Logger.Debug("Disposing of AC ISession " + existingWebSession.GetHashCode());
                existingWebSession.Dispose();
            }

            // Disposing RW session
            existingWebSession = SessionBuilderFactory.GetSessionBuilder(DBSessionType.RWDataBase, this).GetExistingWebSession();
            if (existingWebSession != null)
            {
                Logger.Debug("Disposing of RW ISession " + existingWebSession.GetHashCode());
                existingWebSession.Dispose();
            }
        }

        public void Init(HttpApplication context)
        {
            context.BeginRequest += new EventHandler(context_BeginRequest);
            context.EndRequest += new EventHandler(context_EndRequest);
        }

        public void Dispose()
        {
        }

        // Implementatation of IWebContext interface

        public string PhysicalApplicationPath
        {
            get {
                if (httpApplication != null)
                {
                    return httpApplication.Server.MapPath("~");
                }
                return String.Empty;
            }
        }
    }
}