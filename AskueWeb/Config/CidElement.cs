﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace AskueCtp.Config
{
    public class CidElement : ConfigurationElement
    {
        public CidElement()
        {
        }

        public CidElement(int n_ob, int syb_rnk, int n_gr)
        {
            N_OB = n_ob;
            SYB_RNK = syb_rnk;
            N_GR = n_gr;
        }

        [ConfigurationProperty("n_ob", IsKey=true, IsRequired = true)]
        public Int32 N_OB
        {
            get
            { return (Int32)this["n_ob"]; }
            set
            { this["n_ob"] = value; }
        }

        [ConfigurationProperty("syb_rnk", IsKey = true, IsRequired = true)]
        public Int32 SYB_RNK
        {
            get
            { return (Int32)this["syb_rnk"]; }
            set
            { this["syb_rnk"] = value; }
        }

        [ConfigurationProperty("n_gr", IsKey = true, IsRequired = true)]
        public Int32 N_GR
        {
            get
            { return (Int32)this["n_gr"]; }
            set
            { this["n_gr"] = value; }
        }
    }
}
