﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace AskueCtp.Config
{
    public class DisplayLayoutElement : ConfigurationElement
    {
        public DisplayLayoutElement()
        {
        }

        public DisplayLayoutElement(string backgroundColorPrimary, string backgroundColorSecondary,  
            string textColorPrimary, string textColorSecondary)
        {
            BackgroundColorPrimary = backgroundColorPrimary;
            BackgroundColorSecondary = backgroundColorSecondary;
            TextColorPrimary = textColorPrimary;
            TextColorSecondary = textColorSecondary;
        }

        [ConfigurationProperty("BackgroundColorPrimary", IsKey = false, IsRequired = true)]
        public String BackgroundColorPrimary
        {
            get
            { return (String)this["BackgroundColorPrimary"]; }
            set
            { this["BackgroundColorPrimary"] = value; }
        }

        [ConfigurationProperty("BackgroundColorSecondary", IsKey = false, IsRequired = true)]
        public String BackgroundColorSecondary
        {
            get
            { return (String)this["BackgroundColorSecondary"]; }
            set
            { this["BackgroundColorSecondary"] = value; }
        }

        [ConfigurationProperty("TextColorPrimary", IsKey = false, IsRequired = true)]
        public String TextColorPrimary
        {
            get
            { return (String)this["TextColorPrimary"]; }
            set
            { this["TextColorPrimary"] = value; }
        }

        [ConfigurationProperty("TextColorSecondary", IsKey = false, IsRequired = true)]
        public String TextColorSecondary
        {
            get
            { return (String)this["TextColorSecondary"]; }
            set
            { this["TextColorSecondary"] = value; }
        }
    }
}
