﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace AskueCtp.Config
{
    public class RegistrationPointElement : ConfigurationElement
    {
        public RegistrationPointElement()
        {
        }

        public RegistrationPointElement(String name, String id, Int32 readingPeriod)
        {
            Name = name;
            Id = id;
            ReadingPeriod = readingPeriod;
        }

        [ConfigurationProperty("name", IsKey=false, IsRequired = true)]
        public String Name
        {
            get
            { return (String)this["name"]; }
            set
            { this["name"] = value; }
        }

        [ConfigurationProperty("id", IsKey = true, IsRequired = true)]
        public String Id
        {
            get
            { return (String)this["id"]; }
            set
            { this["id"] = value; }
        }

        [ConfigurationProperty("readingPeriod", IsKey = false, IsRequired = true)]
        public Int32 ReadingPeriod
        {
            get
            { return (Int32)this["readingPeriod"]; }
            set
            { this["readingPeriod"] = value; }
        }

        [ConfigurationProperty("cid", IsRequired=true)]
        public CidElement Cid
        {
            get { return (CidElement)base["cid"]; }
        }

        [ConfigurationProperty("DisplayLayout", IsRequired = true)]
        public DisplayLayoutElement DisplayLayout
        {
            get { return (DisplayLayoutElement)base["DisplayLayout"]; }
        }
    }
}
