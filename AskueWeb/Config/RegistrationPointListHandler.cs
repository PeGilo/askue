﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace AskueCtp.Config
{
    public class RegistrationPointListHandler : ConfigurationSection
    {
        public RegistrationPointListHandler()
        {
        }


        [ConfigurationProperty("registrationPoints", IsDefaultCollection = false)]
        public RegistrationPointCollection RegistrationPoints
        {
            get { return (RegistrationPointCollection)base["registrationPoints"]; }
        }
    }
}
