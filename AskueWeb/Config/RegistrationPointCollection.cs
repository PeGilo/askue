﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace AskueCtp.Config
{
    public class RegistrationPointCollection : ConfigurationElementCollection
    {
        public RegistrationPointCollection()
        {
        }

        public override ConfigurationElementCollectionType CollectionType
        {
            get
            {
                return ConfigurationElementCollectionType.AddRemoveClearMap;
            }
        }

        public RegistrationPointElement this[int index]
        {
            get { return (RegistrationPointElement)base.BaseGet(index); }
        }

        public new RegistrationPointElement this[String id]
        {
            get { return (RegistrationPointElement)base.BaseGet(id); }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new RegistrationPointElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((RegistrationPointElement)element).Id;
        }

        public int IndexOf(RegistrationPointElement regPoint)
        {
            return BaseIndexOf(regPoint);
        }

        public void Add(RegistrationPointElement regPoint)
        {
            BaseAdd(regPoint);
        }

        public void Remove(RegistrationPointElement regPoint)
        {
            if (BaseIndexOf(regPoint) > 0)
            {
                BaseRemove(regPoint.Id);
            }
        }

        public void RemoveAt(int index)
        {
            BaseRemoveAt(index);
        }

        public void Remove(String key)
        {
            BaseRemove(key);
        }

        public void Clear()
        {
            BaseClear();
        }
    }
}
