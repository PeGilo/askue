﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;

using AskueCtp.Common;

namespace AskueCtp.Web.Base
{
    public class CacheWrapper : ICache
    {
        System.Web.Mvc.Controller _controller;

        public CacheWrapper(System.Web.Mvc.Controller controller)
        {
            _controller = controller;
        }

        /// <summary>
        /// Inserts object into cach with default priority, with no exparation date and no sliding exparation
        /// </summary>
        /// <param name="key"></param>
        public void Insert(string key, object value)
        {
            _controller.HttpContext.Cache.Insert(key, value);
        }

        public void Insert(string key, object value, DateTime absoluteExpiration)
        {
            _controller.HttpContext.Cache.Insert(key, value, null, absoluteExpiration, Cache.NoSlidingExpiration);
        }

        public Object Get(string key)
        {
            return _controller.HttpContext.Cache.Get(key);
        }
    }
}