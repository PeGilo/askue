﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Soap;
using System.Text;
using System.Web;

namespace AskueCtp.Web.Base
{
    public class SettingsBase
    {
        protected virtual string Serialize(Object obj)
        {
            Stream stream = new MemoryStream();
            SoapFormatter formatter = new SoapFormatter();

            formatter.Serialize(stream, obj);

            // writing to string
            stream.Seek(0, SeekOrigin.Begin);

            byte[] byteArray = new byte[stream.Length];
            int readByte = 0;
            int count = 0;
            for (; count < stream.Length; count++)
            {
                readByte = stream.ReadByte();
                if (readByte == -1)
                    break;

                byteArray[count] = Convert.ToByte(readByte);
            }

            stream.Close();

            return Encoding.UTF8.GetString(byteArray, 0, (int)count);
        }
        
        protected virtual Object Deserialize(string s)
        {
            byte[] byteArray = Encoding.UTF8.GetBytes(s);
            Stream stream = new MemoryStream(byteArray);
            SoapFormatter formatter = new SoapFormatter();

            Object obj = formatter.Deserialize(stream);
            stream.Close();

            return obj;
        }
    }
}
