﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using AskueCtp.Common;

namespace AskueCtp.Web.Base
{
    /// <summary>
    /// Summary description for BaseController
    /// </summary>
    public partial class BaseController : Controller, IWebContext
    {
        /// <summary>
        /// Presents properties of current user: name, access level ...
        /// </summary>
        /// <remarks>It's made private to prohibit other classes to create this object</remarks>
        private class AppUser : IUser
        {
            private string m_name;
            private Role m_role;

            public string Name
            {
                get { return m_name; }
            }

            public Role Role
            {
                get { return m_role; }
            }

            public AppUser(string name, Role role)
            {
                m_name = name;
                m_role = role;
            }
        }

        private IUser m_UserStore
        {
            get 
            {
                return Session["UserStore"] as IUser;
            }
            set 
            {
                Session["UserStore"] = value;
            }
        }

        protected IUser CurrentUser
        {
            get 
            {
                return m_UserStore; 
            }
        }

        public ICache Cache
        {
            get
            {
                return new CacheWrapper(this);
            }
        }

        private IEnvironment m_Environment;

        public IEnvironment Environment
        {
            get
            {
                return (m_Environment != null) ? m_Environment : (m_Environment = new Environment(this));
            }
        }

        public BaseController()
        {
//            AskueCtp.Common.Logger.Info("BaseController [.ctor]");
        }

        protected override void Initialize(RequestContext requestContext) 
        {
//            AskueCtp.Common.Logger.Info("BaseController [Initialize]");
            base.Initialize(requestContext);

            InitSessionData();
        }  

        protected virtual void InitSessionData()
        {
            lock (this)
            {
                if (m_UserStore == null)
                {
                    m_UserStore = InitializeUserData();
                }
            }
        }

        private AppUser InitializeUserData()
        {
            IPrincipal principal = HttpContext.User;

            // getting user's name
            string name = principal.Identity.Name;

            // getting user's role (highest)
            //Configuration config = WebConfigurationManager.OpenWebConfiguration("/");
            //AppCustomSettings custSection = (AppCustomSettings)config.GetSection("appCustomSettings");

            string adminRoleName = WebConfigurationManager.AppSettings["AdminRoleName"];
            string advUserRoleName = WebConfigurationManager.AppSettings["AdvUserRoleName"];
            string userRoleName = WebConfigurationManager.AppSettings["UserRoleName"];

            Role role;
            if (principal.IsInRole(adminRoleName))
            {
                role = Role.Admin;
            }
            else if (principal.IsInRole(advUserRoleName))
            {
                role = Role.AdvancedUser;
            }
            else if (principal.IsInRole(userRoleName))
            {
                role = Role.User;
            }
            else
            {
                role = Role.Guest;
            }

            return new AppUser(name, role);
        }

        public string PhysicalApplicationPath
        {
            get { return Request.PhysicalApplicationPath; }
        }
    }
}