﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.Configuration;

using AskueCtp.Config;

namespace AskueCtp.Web.Base
{
    public class Environment : IEnvironment
    {
        System.Web.Mvc.Controller _controller;

        public Environment(System.Web.Mvc.Controller controller)
        {
            _controller = controller;
        }

        public string ChartFolderFullPath
        {
            get
            {
                return Path.Combine(_controller.Server.MapPath("~"),
                WebConfigurationManager.AppSettings["ChartFolder"]);
            }
        }

        public string ChartFolderWebPath
        {
            get {
                return _controller.Url.Content("/" + 
                WebConfigurationManager.AppSettings["ChartFolder"]);
            }
        }

        public IList<Config.RegistrationPointElement> GetRegPoints()
        {
            RegistrationPointListHandler registrationPointSettings =
                (RegistrationPointListHandler)WebConfigurationManager.GetSection("registrationPointList");

            return registrationPointSettings.RegistrationPoints.Cast<Config.RegistrationPointElement>().ToList();
        }

        public Config.RegistrationPointElement GetRegPointConfig(int id)
        {
            RegistrationPointListHandler registrationPointSettings =
                (RegistrationPointListHandler)WebConfigurationManager.GetSection("registrationPointList");

            return registrationPointSettings.RegistrationPoints[id.ToString()];
        }

        public int FadeInInterval
        {
            get
            {
                return Convert.ToInt32(WebConfigurationManager.AppSettings["FadeInInterval"]);
            }
        }

        public int FadeOutInterval
        {
            get
            {
                return Convert.ToInt32(WebConfigurationManager.AppSettings["FadeOutInterval"]);
            }
        }

        public int ShowInterval
        {
            get
            {
                return Convert.ToInt32(WebConfigurationManager.AppSettings["ShowInterval"]);
            }
        }
    }
}