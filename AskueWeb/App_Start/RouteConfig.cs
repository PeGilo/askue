﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace AskueWeb
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "monitoring", action = "index", id = UrlParameter.Optional }
            );

            //routes.MapRoute(
            //    "Default",                                              // Route name
            //    "{controller}/{action}/{id}",                                // URL with parameters
            //    new { controller = "monitoring", action = "index", id = "" }     // Parameter defaults
            //);

            //routes.MapRoute(
            //    "DefaultNoPrefix",                                              // Route name
            //    "{controller}/{action}",                                // URL with parameters
            //    new { controller = "monitoring", action = "index" }     // Parameter defaults

            //);

            //routes.MapRoute(
            //    "Root",
            //    "", new { controller = "monitoring", action = "index", id = "" });
        }
    }
}