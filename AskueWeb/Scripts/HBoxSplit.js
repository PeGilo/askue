﻿/**
 * @class Ext.layout.HBoxSplitLayout
 * @extends Ext.layout.BoxLayout
 * @version 0.3
 * @license LGPLv3 - http://www.gnu.org/licenses/lgpl.html
 * @author Ry Racherbaumer
 * 
 * A layout that arranges items horizontally with a split bar between each item
 */
Ext.layout.HBoxFitSplitLayout = Ext.extend(Ext.layout.BoxLayout, {
    /**
     * @cfg {Number} flex
     * This configuation option is to be applied to <b>child <tt>items</tt></b> of the container managed
     * by this layout. Each child item with a <tt>flex</tt> property will be flexed <b>horizontally</b>
     * according to each item's <b>relative</b> <tt>flex</tt> value compared to the sum of all items with
     * a <tt>flex</tt> value specified.  Any child items that have either a <tt>flex = 0</tt> or
     * <tt>flex = undefined</tt>, or no width value, will be assigned a flex value based on the remaining
     * width of the container.
     */
    /**
     * @cfg {Number} minWidth
     * This configuation option is to be applied to <b>child <tt>items</tt></b> of the container managed
     * by this layout. This is the minimum width that the child element can be. This setting will override the
     * itemMinWidth option for this layout.
     */
    /**
     * @cfg {Number} maxWidth
     * This configuation option is to be applied to <b>child <tt>items</tt></b> of the container managed
     * by this layout. This is the maximum width that the child element can be.
     */

    /**
     * @cfg {Number} itemMinWidth
     * This is the minimum width in pixels that child items can be. Defaults to 100. This setting is overridden
     * by the minWidth option on component items.
     */
    itemMinWidth: 100,
    /**
     * @cfg {Number} splitBarWidth
     * This is the width in pixels of each SplitBar. Defaults to 5.
     */
    splitBarWidth: 5,
    // private
    splitsRendered: false,
    rendered: false,
    splits: [],
    // private
    onLayout: function(ct, target) {
        // setup variables
        var i, len, last, num;
        var cs, c, i, cm, cw, ch, ct;
        // add/render splitbar box components
        if (!this.splitsRendered) {
            len = ct.items.length,
            last = len - 1,
            num = 1,
            index = 1;
            for (i = 0; i < len; ++i) {
                if (i < last) {
                    ct.insert(index, {
                        xtype: 'box',
                        id: ct.id + '-splitbar' + num,
                        width: this.splitBarWidth,
                        isSplitBar: true
                    }); ++num;
                    index += 2;
                }
            }
            // render the new items
            Ext.layout.HBoxFitSplitLayout.superclass.onLayout.call(this, ct, target);
            // add splitbars
            ct.items.eachKey(function(key, item, index, count) {
                if (key.indexOf('split') !== -1) {
                    this.splits[this.splits.length] = new Ext.SplitBar(Ext.get(item.id), Ext.getCmp(ct.items.items[index - 1].id).getEl(), Ext.SplitBar.HORIZONTAL, Ext.SplitBar.LEFT);
                    this.splits[this.splits.length - 1].minSize = Ext.getCmp(ct.items.items[index - 1].id).minWidth || this.itemMinWidth;
                    this.splits[this.splits.length - 1].on('beforeapply',
                    function(c, newSize) {
                        this.fireEvent('moved', this, newSize);
                        // don't resize elements automatically
                        return false;
                    });
                    this.splits[this.splits.length - 1].on('moved', this.onSplitMove, this);
                    this.splits[this.splits.length - 1].on('beforeresize', this.onBeforeResize, this);
                }
            },
            this);
            // splits have been rendered
            this.splitsRendered = true;
            // if splits are rendered, just call parent onLayout
        } else Ext.layout.HBoxFitSplitLayout.superclass.onLayout.call(this, ct, target);
        // get layout components
        cs = ct.items.items,
        len = ct.items.items.length,
        last = len - 1;
        // get the size of the layout
        var layoutSize = this.getTargetSize(target);
        // get width, height, and left pos of layout
        var layoutWidth = layoutSize.width - target.getPadding('lr') - this.scrollOffset,
        layoutHeight = layoutSize.height - target.getPadding('tb'),
        layoutLeft = this.padding.left,
        layoutTop = this.padding.top;
        // make sure width/height is greater than 0
        if ((Ext.isIE && !Ext.isStrict) && (layoutWidth < 1 || layoutHeight < 1)) {
            return;
        } else if (layoutWidth < 1 && layoutHeight < 1) {
            return;
        }
        // get width of all splitbars
        var splitBarsWidth = this.splitBarWidth * this.splits.length;
        // get minimum allowable layout width, based on component minWidth, or itemMinWidth
        var minLayoutWidth = splitBarsWidth;
        for (i = 0; i < len; i++) {
            c = cs[i];
            if (!c.isSplitBar) {
                if (c.minWidth && c.minWidth > 0) minLayoutWidth += c.minWidth;
                else minLayoutWidth += this.itemMinWidth;
            }
        }
        // if layout is less than min allowable width, set to min allowable width
        if (layoutWidth < minLayoutWidth) {
            // set layout container to minimum allowed width
            // factor in extra container width (i.e. borders)
            ct.setWidth(minLayoutWidth + (ct.getWidth() - layoutWidth));
            // setting the container width will call onLayout again
            return;
        }
        // set width/height of inner container to match layout
        this.innerCt.setSize(layoutWidth, layoutHeight);
        // get stretch height
        var stretchHeight = layoutHeight - (this.padding.top + this.padding.bottom);
        // get available width for layout items
        var availableWidth = layoutWidth - splitBarsWidth;
        // if layout has never been rendered, make sure flex values are setup properly
        if (!this.rendered) {
            // total flex value
            var totalFlex = 0;
            // make sure total flex value is equal to 1
            for (i = 0; i < len; i++) {
                c = cs[i];
                if (!c.isSplitBar && c.flex && c.flex > 0) totalFlex += c.flex;
            }
            // total flex cannot be greater than 1
            if (totalFlex > 1) return;
            // if total flex value is less than 1, set flex value for components without flex and width
            else if (totalFlex < 1) {
                var availFlex = 1 - totalFlex;
                num = 0;
                // count number of items without width and flex values
                for (i = 0; i < len; i++) {
                    c = cs[i];
                    if (!c.isSplitBar && !c.flex && !c.width)++num;
                }
                // set flex values for items without width and flex values
                for (i = 0; i < len; i++) {
                    c = cs[i];
                    if (!c.isSplitBar && !c.flex && !c.width) c.flex = availFlex / num;
                }
            }
            var availableFlexWidth = availableWidth,
            allocatedFlexWidth = 0;
            // if component width is specified, remove flex value if specified
            // subtract component width from available flex width
            for (i = 0; i < len; i++) {
                c = cs[i];
                if (!c.isSplitBar && c.width && c.width > 0) {
                    // subtract component width from available width (for flex calculations)
                    availableFlexWidth -= c.width;
                    // remove flex value if it's specified
                    if (c.flex || c.flex > 0) delete c.flex;
                    // remove width value from future checks
                    delete c.width;
                }
            }
            // calculate component widths for components with flex value
            for (i = 0; i < len; i++) {
                // get component
                c = cs[i];
                // get component margins
                cm = c.margins;
                // if component is not a splitbar
                if (!c.isSplitBar) {
                    // calculate width of flex value
                    if (c.flex && c.flex > 0) {
                        cw = Math.round(availableFlexWidth * c.flex);
                        // remove flex value
                        delete c.flex;
                        // increase allocated width
                        allocatedFlexWidth += cw;
                        // if allocated width is not equal to available width for last item, subtract diff
                        if (i === last && (allocatedFlexWidth - availableFlexWidth !== 0)) cw -= (allocatedFlexWidth - availableFlexWidth);
                        // account for component margins
                        cw -= (cm.left + cm.right);
                        // set component width
                        c.setWidth(cw);
                    }
                }
            }
            this.rendered = true;
        } else {
            // see if layout width has changed
            var totalComponentWidth = 0;
            for (i = 0; i < len; i++) {
                c = cs[i];
                cm = c.margins;
                cw = c.getWidth();
                cw += cm.left + cm.right;
                if (!c.isSplitBar) totalComponentWidth += cw;
            }
            if (availableWidth !== totalComponentWidth) {
                var allocatedWidth = 0,
                nw;
                // re-calculate component widths based on new layout width
                for (i = 0; i < len; i++) {
                    c = cs[i];
                    cw = c.getWidth();
                    cm = c.margins;
                    cw += cm.left + cm.right;
                    if (!c.isSplitBar) {
                        nw = Math.round(cw / totalComponentWidth * availableWidth);
                        allocatedWidth += nw;
                        // if allocated width is not equal to available width for last item, subtract diff
                        if (i === last && (allocatedWidth - availableWidth !== 0)) nw -= allocatedWidth - availableWidth;
                        c.setWidth(nw - cm.left - cm.right);
                    }
                }
            }
        }
        // make sure component widths don't fall below min/max widths
        // if they do, set their widths to min/max values
        var adjustedComponents = [];
        var newAvailableWidth = availableWidth;
        for (i = 0; i < len; i++) {
            c = cs[i];
            if (!c.isSplitBar) {
                cm = c.margins;
                cw = c.getWidth();
                // add left/right margins to component width
                cw += cm.left + cm.right;
                // set min/max width, accounting for margins
                if ((c.minWidth && c.minWidth > 0 && cw <= c.minWidth) || cw <= this.itemMinWidth) {
                    if (c.minWidth && c.minWidth > 0) {
                        c.setWidth(c.minWidth - (cm.left + cm.right));
                        newAvailableWidth -= c.minWidth;
                    } else {
                        c.setWidth(this.itemMinWidth - (cm.left + cm.right))
                        newAvailableWidth -= this.itemMinWidth;
                    }
                    adjustedComponents[adjustedComponents.length] = i;
                }
                if (c.maxWidth && c.maxWidth > 0 && cw >= c.maxWidth) {
                    c.setWidth(c.maxWidth - (cm.left + cm.right));
                    newAvailableWidth -= c.maxWidth;
                    adjustedComponents[adjustedComponents.length] = i;
                }
            }
        }
        // if component widths were adjusted, re-calculate other item widths
        if (adjustedComponents.length > 0) {
            num = 0;
            var allocatedWidth = 0,
            oldAvailableWidth = 0,
            minWidth = 0;
            // get current width of all items that didn't fall below min/max widths
            for (i = 0; i < len; i++) {
                c = cs[i];
                cw = c.getWidth();
                cm = c.margins;
                // add left/right margins to component width
                cw += cm.left + cm.right;
                if (!c.isSplitBar && adjustedComponents.indexOf(i) === -1) {
                    oldAvailableWidth += cw;
                    minWidth += (c.minWidth && c.minWidth > 0) ? c.minWidth: this.itemMinWidth;
                    // increase item count
                    ++num;
                }
            }
            var nw;
            // set new widths for all items that didn't fall below min/max widths
            for (i = 0; i < len; i++) {
                c = cs[i];
                // make sure component didn't fall below item min/max widths
                if (!c.isSplitBar && adjustedComponents.indexOf(i) === -1) {
                    cm = c.margins;
                    nw = Math.round((newAvailableWidth * c.getWidth()) / oldAvailableWidth);
                    allocatedWidth += nw;
                    // decrease item count
                    --num;
                    // if last item, make sure there's no extra width
                    if (num === 0 && (allocatedWidth - newAvailableWidth !== 0)) nw -= allocatedWidth - newAvailableWidth;
                    // set component width, accounting for margins
                    c.setWidth(nw - (cm.left + cm.right));
                }
            }
            // check if new available width falls below the minimum possible width
            // if so, subtract needed width equally from other components that can spare it
            if (newAvailableWidth < minWidth) {
                var extraWidthComponents = [],
                extraWidths = [],
                neededWidth = minWidth - newAvailableWidth;
                var currentMinWidth = 0;
                num = 0;
                for (i = 0; i < len; i++) {
                    c = cs[i];
                    cw = c.getWidth();
                    cm = c.margins;
                    // add left/right margins to component width
                    cw += cm.left + cm.right;
                    currentMinWidth = (c.minWidth && c.minWidth > 0) ? c.minWidth: this.itemMinWidth;
                    if (!c.isSplitBar && cw > currentMinWidth) {
                        var extraWidth = cw - currentMinWidth;
                        extraWidthComponents[extraWidthComponents.length] = i;
                        extraWidths[extraWidths.length] = extraWidth; ++num;
                    }
                }
                // get the width to subtract from each component with available width
                var subtractWidth = Math.round(neededWidth / extraWidths.length),
                subtractedWidth = 0;
                // make sure each component has enough width to subtract
                for (i = 0; i < len; i++) {
                    c = cs[i];
                    cw = c.getWidth();
                    cm = c.margins;
                    // add left/right margins to component width
                    cw += cm.left + cm.right;
                    if (extraWidthComponents.indexOf(i) !== -1 && extraWidths[extraWidthComponents.indexOf(i)] < subtractWidth) {
                        // set width of component
                        c.setWidth(cw - extraWidths[extraWidthComponents.indexOf(i)]);
                        // subtract width from needed width
                        neededWidth -= extraWidths[extraWidthComponents.indexOf(i)];
                        // remove component from width arrays
                        extraWidthComponents.splice(extraWidthComponents.indexOf(i), 1);
                        extraWidths.splice(extraWidthComponents.indexOf(i), 1);
                        // re-calculate subtract width
                        subtractWidth = Math.round(neededWidth / extraWidths.length); --num;
                    }
                }
                // subtract width from components with enough width
                var sw;
                for (i = 0; i < len; i++) {
                    c = cs[i];
                    cw = c.getWidth();
                    cm = c.margins;
                    // add left/right margins to component width
                    cw += cm.left + cm.right;
                    if (extraWidthComponents.indexOf(i) !== -1) {
                        // keep track of subtracted width
                        subtractedWidth += subtractWidth;
                        sw = subtractWidth; --num;
                        // if last item to subtract, make sure there's no extra width
                        if (num === 0 && (subtractedWidth - neededWidth !== 0)) sw -= subtractedWidth - neededWidth;
                        // set width of component
                        c.setWidth(cw - sw);
                    }
                }
                // loop through un-adjusted components and set their minimum width, if necessary
                for (i = 0; i < len; i++) {
                    c = cs[i];
                    cw = c.getWidth();
                    cm = c.margins;
                    // add left/right margins to component width
                    cw += cm.left + cm.right;
                    currentMinWidth = (c.minWidth && c.minWidth > 0) ? c.minWidth: this.itemMinWidth;
                    if (!c.isSplitBar && adjustedComponents.indexOf(i) === -1 && cw < currentMinWidth) {
                        // set minimum width of component, factor in margins
                        c.setWidth(currentMinWidth - cm.left - cm.right);
                    }
                }
            }
        }
        // set component positions within the layout
        for (i = 0; i < len; i++) {
            // get component
            c = cs[i];
            // get component margins
            cm = c.margins;
            // get component width
            cw = c.getWidth();
            // get component height
            ch = c.getHeight();
            // current left position within layout
            layoutLeft += cm.left;
            // component top position
            ct = layoutTop + cm.top;
            // set position of component
            c.setPosition(layoutLeft, ct);
            // set component height to layout height
            c.setHeight((stretchHeight - (cm.top + cm.bottom)).constrain(c.minHeight || 0, c.maxHeight || 1000000));
            // increase layout left position
            layoutLeft += cw + cm.right;
        }
    },
    // execute when a splitbar has moved
    onSplitMove: function(split, newSize) {
        // get split resize component
        var c = Ext.getCmp(split.resizingEl.id);
        var cm = c.margins;
        var target = this.container.getLayoutTarget();
        var size = this.getTargetSize(target);
        // layout width
        var lw = size.width - target.getPadding('lr') - this.scrollOffset;
        // get total splitbar width
        var sw = this.splitBarWidth * this.splits.length;
        // get total width
        var w = lw - sw;
        // get split adjacent component
        var ac = Ext.getCmp(split.el.id).nextSibling();
        var acm = ac.margins;
        // get sum of resize and adjacent component widths + margins
        var tcw = c.getWidth() + cm.left + cm.right + ac.getWidth() + acm.left + acm.right;
        // new adjacent component width
        var nacw = tcw - newSize;
        // set item width
        c.setWidth(newSize);
        ac.setWidth(nacw);
        this.layout();
    },
    // execute before split bar is moved
    onBeforeResize: function(split) {
        var c = Ext.getCmp(split.resizingEl.id);
        var cm = c.margins;
        var sc = Ext.getCmp(split.el.id);
        var ac = Ext.getCmp(split.el.id).nextSibling();
        var acm = ac.margins;
        // get total component width
        var tcw = c.getWidth() + ac.getWidth();
        // get adjacent component min width
        var acMinWidth = ac.minWidth || this.itemMinWidth;
        // calculate max size of splitbar
        if (c.maxWidth && c.maxWidth > 0 && c.maxWidth <= (tcw - acMinWidth)) {
            split.maxSize = c.maxWidth;
        } else split.maxSize = tcw - acMinWidth;
    }
});
// register layout
Ext.Container.LAYOUTS['hboxfitsplit'] = Ext.layout.HBoxFitSplitLayout;