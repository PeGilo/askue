﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AskueCtp.Web
{
    public interface IEnvironment
    {
        string ChartFolderFullPath { get; }
        string ChartFolderWebPath { get; }
        Config.RegistrationPointElement GetRegPointConfig(int id);
        IList<Config.RegistrationPointElement> GetRegPoints();
        int FadeInInterval { get; }
        int FadeOutInterval { get; }
        int ShowInterval { get; }
    }
}
