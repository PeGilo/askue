# Monitoring app #
This web-application is used for monitoring current technical measurements. The data is extracted from Oracle database.

This is an ASP.NET MVC app.

Business logic and data access logic are separated to different layers and linked with interfaces. The BL layer is thoroughly tested with Unit tests. Repository pattern is used along with dependency injection. 

Developer environment: **C# (.NET Framework 4.5), Visual Studio**.

Used:

 * **ASP.NET MVC 3**;
 * **NHibernate 2.0** - for object-relation mapping;
 * **log4net** - for logging;
 * **NPlot** library - for drawing charts;
 * Visual Studio **Unit Testing Framework**.
 * Oracle datbase