﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AskueCtp.Common;

namespace AskueCtp.Entities
{
    public class PlanValueCid
    {
        private string m_ParameterId;
        private DateTime m_Date;
        private int m_Interval;
        private int m_IntervalNumber;

        public string ParameterId
        {
            get { return m_ParameterId; }
            set { m_ParameterId = value; }
        }

        public DateTime Date
        {
            get { return m_Date; }
            set { m_Date = value; }
        }

        /// <summary>
        /// Значение интервала (1, 3, 5, 10, 15, 30, 60, ...)
        /// </summary>
        public int Interval
        {
            get { return m_Interval; }
            set { m_Interval = value; }
        }

        /// <summary>
        /// Номер интервала (1,2,3,4,5,6...)
        /// </summary>
        public int IntervalNumber
        {
            get { return m_IntervalNumber; }
            set { m_IntervalNumber = value; }
        }

        public PlanValueCid()
        {
            m_ParameterId = String.Empty;
        }

        public PlanValueCid(string paramId, DateTime date, int interval, int intervalNumber)
        {
            m_ParameterId = paramId;
            m_Date = date;
            m_Interval = interval;
            m_IntervalNumber = intervalNumber;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is PlanValueCid)) return false;

            return this.Equals((PlanValueCid)obj);
        }

        public virtual bool Equals(PlanValueCid obj)
        {
            return (m_ParameterId.Equals(obj.m_ParameterId)
                && m_Date.Equals(obj.m_Date)
                && m_Interval.Equals(obj.m_Interval)
                && m_IntervalNumber.Equals(obj.m_IntervalNumber));
        }

        public override int GetHashCode()
        {
            return Hash.Generate(m_ParameterId.GetHashCode(),
                m_Date.GetHashCode(), m_Interval, m_IntervalNumber);
        }
    }
}
