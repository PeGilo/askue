﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AskueCtp.Common;

namespace AskueCtp.Entities
{
    public class PreferenceCid
    {
        private string m_PreferenceId;
        private string m_UserId;
        private int m_PartNo;

        public string PreferenceId
        {
            get { return m_PreferenceId; }
            set { m_PreferenceId = value; }
        }

        public string UserId
        {
            get { return m_UserId; }
            set { m_UserId = value; }
        }

        public int PartNo
        {
            get { return m_PartNo; }
            set { m_PartNo = value; }
        }

        public PreferenceCid()
        {
            m_PreferenceId = String.Empty;
            m_UserId = String.Empty;
        }

        public PreferenceCid(string preferenceId, string userId, int partNo)
        {
            m_PreferenceId = preferenceId;
            m_UserId = userId;
            m_PartNo = partNo;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is PreferenceCid)) return false;

            return this.Equals((PreferenceCid)obj);
        }

        public virtual bool Equals(PreferenceCid obj)
        {
            return (m_PreferenceId.Equals(obj.m_PreferenceId)
                && m_UserId == obj.m_UserId
                && m_PartNo == obj.m_PartNo);
        }

        public override int GetHashCode()
        {
            return Hash.Generate(m_PreferenceId.GetHashCode(), m_PartNo,
                ((m_UserId==null) ? 0 : m_UserId.GetHashCode()));
        }

        public override string ToString()
        {
            return "PrefId=" + m_PreferenceId.ToString() 
                + ((m_UserId==null) ? String.Empty : " UserId=" + m_UserId.ToString()
                + " PartNo=" + m_PartNo.ToString());
        }
    }
}
