﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AskueCtp.Entities
{
    public enum Unit : int
    {
        Percent = 0,
        Absolute = 1
    }
}
