﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AskueCtp.Common;

namespace AskueCtp.Entities
{
    public class Preference
    {
        private PreferenceCid m_Cid = new PreferenceCid();
        private string m_Value;

        public virtual PreferenceCid Cid
        {
            get { return m_Cid; }
            set { m_Cid = value; }
        }

        public virtual string Value
        {
            get { return ((m_Value != null) ? m_Value : string.Empty); }
            set { m_Value = value; }
        }

        public Preference()
        {
            m_Value = String.Empty;
        }

        public Preference(PreferenceCid cid, string value)
        {
            m_Cid = cid;
            m_Value = value;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Preference)) return false;

            return this.Equals((Preference)obj);
        }

        public virtual bool Equals(Preference obj)
        {
            return (m_Cid.Equals(obj.m_Cid)
                && m_Value.Equals(obj.m_Value));
        }

        public override int GetHashCode()
        {
            return Hash.Generate(m_Cid.GetHashCode(), m_Value.GetHashCode());
        }

        public override string ToString()
        {
            return m_Cid.ToString() + " Value=" + m_Value.ToString();
        }
    }
}
