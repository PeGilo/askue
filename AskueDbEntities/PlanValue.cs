﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AskueCtp.Common;

namespace AskueCtp.Entities
{
    public class PlanValue
    {
        private PlanValueCid m_Cid = new PlanValueCid();
        private float? m_Value;

        public virtual PlanValueCid Cid
        {
            get { return m_Cid; }
            set { m_Cid = value; }
        }

        public virtual float? Value
        {
            get { return m_Value; }
            set { m_Value = value; }
        }

        public PlanValue()
        {
        }

        public PlanValue(PlanValueCid cid, float? value)
        {
            m_Cid = cid;
            m_Value = value;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is PlanValue)) return false;

            return this.Equals((PlanValue)obj);
        }

        public virtual bool Equals(PlanValue obj)
        {
            return (m_Cid.Equals(obj.m_Cid)
                && m_Value.Equals(obj.m_Value));
        }

        public override int GetHashCode()
        {
            return Hash.Generate(m_Cid.GetHashCode(), m_Value.GetHashCode());
        }
    }
}
