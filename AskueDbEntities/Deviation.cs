﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AskueCtp.Common;

namespace AskueCtp.Entities
{
    public class Deviation
    {
        private string m_ParameterId;
        private float? m_WarningDiff;
        private float? m_LimitDiff;
        private Unit m_Unit;

        public virtual string ParameterId
        {
            get { return m_ParameterId; }
            set { m_ParameterId = value; }
        }

        public virtual float? WarningDiff
        {
            get { return m_WarningDiff; }
            set { m_WarningDiff = value; }
        }

        public virtual float? LimitDiff
        {
            get { return m_LimitDiff; }
            set { m_LimitDiff = value; }
        }

        public virtual Unit Unit
        {
            get { return m_Unit; }
            set { m_Unit = value; }
        }

        public Deviation()
        {
        }

        public Deviation(string paramId, float? warningDiff, float? limitDiff, Unit unit)
        {
            m_ParameterId = paramId;
            m_WarningDiff = warningDiff;
            m_LimitDiff = limitDiff;
            m_Unit = unit;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Deviation)) return false;

            return this.Equals((Deviation)obj);
        }

        public virtual bool Equals(Deviation obj)
        {
            return (m_ParameterId.Equals(obj.m_ParameterId)
                && m_WarningDiff.Equals(obj.m_WarningDiff)
                && m_LimitDiff.Equals(obj.m_LimitDiff)
                && m_Unit.Equals(obj.m_Unit)
                );
        }

        public override int GetHashCode()
        {
            return Hash.Generate(m_ParameterId.GetHashCode(), m_WarningDiff.GetHashCode(),
                m_LimitDiff.GetHashCode(), m_Unit.GetHashCode());
        }
    }
}
