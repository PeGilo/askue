﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using AskueCtp.ServiceLayer.Impl;
using AskueCtp.Entities;

namespace AskueCtp.ServiceLayer.Tests
{
    [TestClass]
    public class MonitoringServiceTests
    {
        MonitoringService service = null;

        [TestInitialize]
        public void TestInit()
        {
            service = new MonitoringService();
        }

        [TestMethod]
        public void TestMethod1()
        {
            Check(0, 60);
        }

        [TestMethod]
        public void TestMethod2()
        {
            Check(1, 60);
        }

        [TestMethod]
        public void TestMethod3()
        {
            Check(0.00001f, 60);
        }

        [TestMethod]
        public void TestMethod4()
        {
            Check(0.00001f, 60.0001f);
        }

        [TestMethod]
        public void TestMethod5()
        {
            Check(-0.00001f, -60.0001f);
        }

        private void Check(float x, float y)
        {
            Assert.IsFalse(service.CheckPhaseFailure(new SetiMeasurement() { Ua = y, Ub = y, Uc = y }));

            Assert.IsFalse(service.CheckPhaseFailure(new SetiMeasurement() { Ua = x, Ub = x, Uc = x }));

            Assert.IsTrue(service.CheckPhaseFailure(new SetiMeasurement() { Ua = y, Ub = x, Uc = x }));

            Assert.IsTrue(service.CheckPhaseFailure(new SetiMeasurement() { Ua = y, Ub = y, Uc = x }));

            Assert.IsTrue(service.CheckPhaseFailure(new SetiMeasurement() { Ua = x, Ub = y, Uc = x }));

            Assert.IsTrue(service.CheckPhaseFailure(new SetiMeasurement() { Ua = x, Ub = y, Uc = y }));

            Assert.IsTrue(service.CheckPhaseFailure(new SetiMeasurement() { Ua = x, Ub = x, Uc = y }));

            Assert.IsTrue(service.CheckPhaseFailure(new SetiMeasurement() { Ua = y, Ub = x, Uc = y }));
        }

        [TestMethod]
        public void TestMethod6()
        {
            CheckNoFailure(0.05f, 0.5f, 0.06f);
            CheckNoFailure(0.0588f, 0.5570f, 0.0491f);
            CheckNoFailure(0.0483f, 0.5648f, 0.0489f);
        }

        private void CheckNoFailure(float a, float b, float c)
        {
            Assert.IsFalse(service.CheckPhaseFailure(new SetiMeasurement() { Ua = a, Ub = b, Uc = c }));
        }
    }
}
