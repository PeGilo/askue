﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;

using log4net;
using log4net.Config;

using AskueCtp.Common;

namespace AskueCtp.ConsoleTests
{
    class Program
    {
        static void Main(string[] args)
        {
            //TestLogger();
            TestWebSite();
        }

        static void TestWebSite()
        {
            int threadCount = 10;

            for (int i = 0; i < threadCount; i++)
            {
                WebClient webClient = new WebClient();
                Uri uri = new Uri("http://localhost:1576/AskueCtp/");
                webClient.DownloadStringAsync(uri);
            }
        }

        static void TestLogger()
        {
            int threadCount = 10;
            Thread[] threads = new Thread[threadCount];

            for (int i = 0; i < threadCount; i++)
            {
                threads[i] = new Thread(new ThreadStart(LoggerThreadProc));
            }

            for (int i = 0; i < threadCount; i++)
            {
                threads[i].Start();
            }

            for (int i = 0; i < threadCount; i++)
            {
                threads[i].Join();
            }
        }

        static void LoggerThreadProc()
        {
            //ILog log = param as ILog;
            //Logger log = new Logger();
            //if(log != null)
            //{
                string threadNumber = Thread.CurrentThread.ManagedThreadId.ToString();
                for (int i = 0; i < 10; i++)
                {
                    //Logger.Info("#" + threadNumber + " string# " + i.ToString());
                }
            //}
        }
    }
}
